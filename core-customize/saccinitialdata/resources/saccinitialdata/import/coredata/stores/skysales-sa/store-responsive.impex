# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
# 
# Create the Responsive Skysales BaseStore
#
$productCatalog=skysalesProductCatalog
$classificationCatalog=SkysalesClassification
$classificationCatalogSA=SkysalesClassificationSA
$pdpIconsClassificationCatalog=PDPIconsSkysalesClassification
$currencies=SAR,USD
$net=false
$storeUid=skysales-sa
$defaultCurrency=SAR
$languages=en,ar
$defaultLanguage=en
$deliveryCountries=SA
$billingCountries=SA
$orderProcessCode=order-process
$pickupInStoreMode=BUY_AND_COLLECT
$customerAllowedToIgnoreSuggestions=true
$paymentProvider=HyperpayPaymentProvider
$expressCheckoutEnabled=false
$returnProcessCode=return-process
$checkoutFlowGroup=responsiveCheckoutGroup

INSERT_UPDATE PromotionGroup;Identifier[unique=true];
;skysales-saPromoGrp;

# Base Store
INSERT_UPDATE BaseStore;uid[unique=true];catalogs(id);currencies(isocode);net;taxGroup(code);storelocatorDistanceUnit(code);defaultCurrency(isocode);languages(isocode);defaultLanguage(isocode);deliveryCountries(isocode);billingCountries(isocode);submitOrderProcessCode;pickupInStoreMode(code);customerAllowedToIgnoreSuggestions;paymentProvider;expressCheckoutEnabled;maxRadiusForPoSSearch;checkoutFlowGroup;createReturnProcessCode;
;$storeUid;$productCatalog,$classificationCatalog,$classificationCatalogSA,$pdpIconsClassificationCatalog;$currencies;$net;sk-sa-taxes;km;$defaultCurrency;$languages;$defaultLanguage;$deliveryCountries;$billingCountries;$orderProcessCode;$pickupInStoreMode;$customerAllowedToIgnoreSuggestions;$paymentProvider;$expressCheckoutEnabled;300;$checkoutFlowGroup;$returnProcessCode;

INSERT_UPDATE BaseStore2DeliveryModeRel;source(uid)[unique=true];target(code)[unique=true]
;$storeUid;sk-sa	

# Hyperpay Test Credentials
$TestHyperpayAccessToken=OGFjN2E0Yzc3MmI3N2RkZjAxNzJiZDI2NGI5ODFlNWF8N2tnSzNCcG5Xbg==
$TestHyperpayEntityId=8ac7a4c772b77ddf0172bd26cdb31e61
$TestHyperpaySrcForm=https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=
$TestHyperpayBrands=VISA MASTER AMEX
$TestHyperpayTestMode=EXTERNAL
$TestHyperpayRound=true
$TestHyperpayCurrency=SAR
$TestHyperpayPaymentType=DB
$TestHyperpayCheckoutsURL=https://test.oppwa.com/v1/checkouts
$TestHyperpayQueryURL=https://test.oppwa.com/v1/query/
$TestHyperpayPaymentsURL=https://test.oppwa.com/v1/payments/

# MADA test creds
$TestHyperpayMadaAccessToken=OGE4Mjk0MTc0ZDcwYmIxYTAxNGQ3MWFlNzdkNDA5NWN8bVFBS242Y2c=
$TestHyperpayMadaEntityId=8ac7a4c876d64b480176d83aea7d077b
$TestHyperpayMadaBrands=MADA

INSERT_UPDATE HyperpayPaymentProvider;code[unique=true];name;active;accessToken;entityId;srcForm;brands;testMode;round;currency(isocode);paymentType;checkoutsURL;queryURL;paymentsURL;
;skysales-saHyperpayPaymentProvider_TEST_visa_master_amex;Skysales SA Hyperpay Payment Provider;false;$TestHyperpayAccessToken;$TestHyperpayEntityId;$TestHyperpaySrcForm;$TestHyperpayBrands;$TestHyperpayTestMode;$TestHyperpayRound;$TestHyperpayCurrency;$TestHyperpayPaymentType;$TestHyperpayCheckoutsURL;$TestHyperpayQueryURL;$TestHyperpayPaymentsURL;
;skysales-saHyperpayPaymentProvider_TEST_mada;Skysales SA Hyperpay Payment Provider;false;$TestHyperpayMadaAccessToken;$TestHyperpayMadaEntityId;$TestHyperpaySrcForm;$TestHyperpayMadaBrands;$TestHyperpayTestMode;$TestHyperpayRound;$TestHyperpayCurrency;$TestHyperpayPaymentType;$TestHyperpayCheckoutsURL;$TestHyperpayQueryURL;$TestHyperpayPaymentsURL;

# Hyperpay Live Credentials
$HyperpayAccessToken=OGFjOWE0Y2U3MzlhYjNkNjAxNzNkMmNlY2UzMTRmY2V8d3hqR1AzUmZ4Qw==
$HyperpayEntityId=8ac9a4c973be75180173d2e85b4813df
$HyperpaySrcForm=https://oppwa.com/v1/paymentWidgets.js?checkoutId=
$HyperpayBrands=VISA MASTER AMEX
$HyperpayTestMode=EXTERNAL
$HyperpayRound=false
$HyperpayCurrency=SAR
$HyperpayPaymentType=DB
$HyperpayCheckoutsURL=https://oppwa.com/v1/checkouts
$HyperpayQueryURL=https://oppwa.com/v1/query/
$HyperpayPaymentsURL=https://oppwa.com/v1/payments/

# Hyperpay Live MADA Credentials
$HyperpayMadaAccessToken=OGFjOWE0Y2U3MzlhYjNkNjAxNzNkMmNlY2UzMTRmY2V8d3hqR1AzUmZ4Qw==
$HyperpayMadaEntityId=8ac9a4c973be75180173d2e85b4813df
$HyperpayMadaBrands=MADA

### Hyperpay LIVE
INSERT_UPDATE HyperpayPaymentProvider;code[unique=true];name;active;accessToken;entityId;srcForm;brands;testMode;round;currency(isocode);paymentType;checkoutsURL;queryURL;paymentsURL;
;skysales-saHyperpayPaymentProvider_OtherBrands;Skysales SA Hyperpay Payment Provider;true;$HyperpayAccessToken;$HyperpayEntityId;$HyperpaySrcForm;$HyperpayBrands;$HyperpayTestMode;$HyperpayRound;$HyperpayCurrency;$HyperpayPaymentType;$HyperpayCheckoutsURL;$HyperpayQueryURL;$HyperpayPaymentsURL;
;skysales-saHyperpayPaymentProvider_MADA;Skysales SA Hyperpay Payment Provider;true;$HyperpayMadaAccessToken;$HyperpayMadaEntityId;$HyperpaySrcForm;$HyperpayMadaBrands;$HyperpayTestMode;$HyperpayRound;$HyperpayCurrency;$HyperpayPaymentType;$HyperpayCheckoutsURL;$HyperpayQueryURL;$HyperpayPaymentsURL;


### Link Hyperpay Test Providers with the Payment Mode

UPDATE StandardPaymentMode;code[unique=true]	;paymentProvider(code)
								 ;card			;skysales-saHyperpayPaymentProvider_TEST_visa_master_amex
								 ;mada			;skysales-saHyperpayPaymentProvider_TEST_mada

INSERT_UPDATE StoreCreditMode;storeCreditModeType(code)[unique=true];name;store(uid);active[default=true]
;REDEEM_NONE;REDEEM NONE;$storeUid
;REDEEM_FULL_AMOUNT;REDEEM FULL AMOUNT;$storeUid
;REDEEM_SPECIFIC_AMOUNT;REDEEM SPECIFIC AMOUNT;$storeUid

UPDATE BaseStore;uid[unique=true];storeCreditEnabled;calcStoreCreditAmountIncludeDeliveryCost;calcStoreCreditAmountIncludePaymentCost
;$storeUid;true;true;true

UPDATE BaseStore;uid[unique=true];paymentProviders(code)
;$storeUid;skysales-saHyperpayPaymentProvider_OtherBrands,skysales-saHyperpayPaymentProvider_MADA,skysales-saHyperpayPaymentProvider_TEST_visa_master_amex,skysales-saHyperpayPaymentProvider_TEST_mada

$storeUidSMSA=$storeUid
$code=Smsa_Fulfillment
$name=Smsa Fulfillment Provider
$active=true
$baseUrl=/
$passKey=s@ud1c5r
$smsaDeliveryType=DLV
$description=Skysales
$shipperName=SKYSALES ONLINE
$shipperContactName=Skysales Online
$shipperAddress1=SMSA FULFILLMENT CENTER
$shipperCity=Riyadh
$shipperPhone=920001758 
$shipperCountry=Saudi Arabia

INSERT_UPDATE SmsaFulfillmentProvider ;code[unique=true];trackingUrl;stores(uid);name;active[default=false];baseUrl;passKey;smsaDeliveryType(code);description;shipperName;shipperContactName;shipperAddress1;shipperCity;shipperPhone;shipperCountry;
;$code;"http://www.smsaexpress.com/Track.aspx?tracknumbers=";$storeUidSMSA;$name;$active;$baseUrl;$passKey;$smsaDeliveryType;$description;$shipperName;$shipperContactName;$shipperAddress1;$shipperCity;$shipperPhone;$shipperCountry

UPDATE BaseStore;uid[unique=true];fulfillmentProvider
;$storeUid;SmsaFulfillmentProvider

$saee_code=Saee_Fulfillment
$saee_name=Saee Fulfillment Provider
$saee_secret=$2y$10$m9dolGpdQ5eCtiA0E/xuyus.wNhj75WRH3gib0pp3wBAfVzO/.AR.
$saee_sendername=SKYSALES ONLINE
$saee_sendermail=Cs@SkySales.sa
$saee_senderaddress=Jeddah
$saee_sendercity=Jeddah
$saee_senderphone=920034331
$saee_sendercountry=Saudi Arabia
$saee_quantity=1
$saee_baseUrl=https://www.k-w-h.com/
$saee_active=true


INSERT_UPDATE SaeeFulfillmentProvider;code[unique=true];trackingUrl;active;baseUrl;stores(uid);name;secret;sendername;senderphone;sendermail;sendercity;sendercountry;quantity
;$saee_code;"https://saee.sa/trackingpage?trackingnum=";$saee_active;$saee_baseUrl;$storeUid;$saee_name;$saee_secret;$saee_sendername;$saee_senderphone;$saee_sendermail;$saee_sendercity;$saee_sendercountry;$saee_quantity

UPDATE BaseStore;uid[unique=true];fulfillmentProvidorTypes(code)
;$storeUid;SMSA,SAEE


INSERT_UPDATE OrderProcessingCronJob;code[unique=true];job(code)[default=updateFulfillmentStatusJob];store(uid)[default=$storeUid];sessionLanguage(isoCode)[default=en];intervalLength[default=7200];timezone[default=Asia/Dubai];active[default=true];logToFile[default=true]
;skysales-sa-OrderProcessingCronJob-updateConsignmentStatus;;;;;;;

INSERT_UPDATE SourcingBanConfig;sourcingBanAllowed;sourcingBanDeclineReasonOtherAllowed;sourcingBanDeclineReasonStoreCloseAllowed;sourcingBanDeclineReasonTooBusyAllowed;
;true;true;true;true;


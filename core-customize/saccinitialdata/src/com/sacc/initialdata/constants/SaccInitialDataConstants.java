/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.initialdata.constants;

/**
 * Global class for all SaccInitialData constants.
 */
public final class SaccInitialDataConstants extends GeneratedSaccInitialDataConstants
{
	public static final String EXTENSIONNAME = "saccinitialdata";

	private SaccInitialDataConstants()
	{
		//empty
	}
}

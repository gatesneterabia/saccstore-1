package com.sacc.sacccustomersupportbackoffice.widgets.returns.createreturnrequest;

import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.commerceservices.event.CreateReturnEvent;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.refund.RefundService;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;

import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.hybris.cockpitng.util.notifications.NotificationService;
import com.sacc.sacccustomersupportbackoffice.widgets.returns.dtos.ReturnEntryToCreateDto;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomCreateReturnRequestController extends DefaultWidgetController
{
	private static final Logger LOG = Logger.getLogger(CustomCreateReturnRequestController.class);
	private static final long serialVersionUID = 1L;
	protected static final String IN_SOCKET = "inputObject";
	protected static final String OUT_CONFIRM = "confirm";
	protected static final Object COMPLETED = "completed";
	protected static final int COLUMN_INDEX_RETURNABLE_QUANTITY = 5;
	protected static final int COLUMN_INDEX_RETURN_QUANTITY = 6;
	protected static final int COLUMN_INDEX_RETURN_AMOUNT = 7;
	protected static final int COLUMN_INDEX_RETURN_REASON = 8;
	protected static final int COLUMN_INDEX_RETURN_COMMENT = 9;
	private final List<String> refundReasons = new ArrayList<String>();
	private transient Set<ReturnEntryToCreateDto> returnEntriesToCreate;
	private OrderModel order;
	@Wire
	private Textbox orderCode;
	@Wire
	private Textbox customer;
	@Wire
	private Doublebox totalDiscounts;
	@Wire
	private Doublebox orderTotal;
	@Wire
	private Combobox globalReason;
	@Wire
	private Textbox globalComment;
	@Wire
	private Grid returnEntries;
	@Wire
	private Checkbox isReturnInstore;
	@Wire
	private Checkbox refundDeliveryCost;
	@Wire
	private Checkbox globalReturnEntriesSelection;
	@Wire
	private Doublebox totalRefundAmount;
	@Wire
	private Doublebox estimatedTax;
	@Wire
	private Doublebox deliveryCost;
	@WireVariable
	private transient ReturnService returnService;
	@WireVariable
	private transient RefundService refundService;
	@WireVariable
	private transient EventService eventService;
	@WireVariable
	private transient EnumerationService enumerationService;
	@WireVariable
	private transient ModelService modelService;
	@WireVariable
	private transient BackofficeLocaleService cockpitLocaleService;
	@WireVariable
	private transient CockpitEventQueue cockpitEventQueue;
	@WireVariable
	private transient NotificationService notificationService;

	@SocketEvent(socketId = "inputObject")
	public void initCreateReturnRequestForm(final OrderModel inputOrder)
	{
		setOrder(inputOrder);
		refundReasons.clear();
		isReturnInstore.setChecked(false);
		refundDeliveryCost.setChecked(false);
		globalReturnEntriesSelection.setChecked(false);
		deliveryCost.setValue(getOrder().getDeliveryCost());
		refundDeliveryCost.setDisabled(getReturnService().getReturnRequests(getOrder().getCode()).stream()
				.anyMatch(returnRequest -> returnRequest.getRefundDeliveryCost() != false
						&& returnRequest.getStatus() != ReturnStatus.CANCELED));
		getWidgetInstanceManager().setTitle(
				String.valueOf(getWidgetInstanceManager().getLabel("customersupportbackoffice.createreturnrequest.confirm.title"))
						+ " " + getOrder().getCode());
		orderCode.setValue(getOrder().getCode());
		customer.setValue(getOrder().getUser().getDisplayName());
		orderTotal.setValue(getOrder().getTotalPrice());
		setTotalDiscounts();
		final Locale locale = getCockpitLocaleService().getCurrentLocale();
		getEnumerationService().getEnumerationValues(RefundReason.class).forEach(reason -> {
			final boolean bl = refundReasons.add(getEnumerationService().getEnumerationName(reason, locale));
		});
		globalReason.setModel(new ListModelArray(refundReasons));
		final Map<AbstractOrderEntryModel, Long> returnableOrderEntries = getReturnService().getAllReturnableEntries(inputOrder);
		returnEntriesToCreate = new HashSet<ReturnEntryToCreateDto>();
		returnableOrderEntries.forEach((orderEntry, returnableQty) -> {
			final boolean bl = returnEntriesToCreate
					.add(new ReturnEntryToCreateDto(orderEntry, returnableQty.intValue(), refundReasons));
		});
		getReturnEntries().setModel(new ListModelList(returnEntriesToCreate));
		getReturnEntries().renderAll();
		addListeners();
	}

	protected void addListeners()
	{
		final List<Component> rows = this.returnEntries.getRows().getChildren();
		for (final Component row : rows)
		{
			for (final Component myComponent : row.getChildren())
			{
				if (myComponent instanceof Combobox)
				{
					myComponent.addEventListener("onCustomChange",
							event -> Events.echoEvent("onLaterCustomChange", myComponent, event.getData()));
					myComponent.addEventListener("onLaterCustomChange", event -> {
						Clients.clearWrongValue(myComponent);
						myComponent.invalidate();
						this.handleIndividualRefundReason(event);
					});
					continue;
				}
				if (myComponent instanceof Checkbox)
				{
					myComponent.addEventListener("onCheck", event -> {
						this.handleRow((Row) event.getTarget().getParent());
						this.calculateTotalRefundAmount();
					});
					continue;
				}
				if (myComponent instanceof Intbox)
				{
					myComponent.addEventListener("onChanging", this::handleIndividualQuantityToReturn);
					continue;
				}
				if (myComponent instanceof Doublebox)
				{
					myComponent.addEventListener("onChanging", this::handleIndividualAmountToReturn);
					continue;
				}
				if (!(myComponent instanceof Textbox))
				{
					continue;
				}
				myComponent.addEventListener("onChanging", event -> {
					this.autoSelect(event);
					((ReturnEntryToCreateDto) ((Row) event.getTarget().getParent()).getValue())
							.setRefundEntryComment(((InputEvent) event).getValue());
				});
			}
		}
		this.globalReason.addEventListener("onSelect", this::handleGlobalReason);
		this.globalComment.addEventListener("onChanging", this::handleGlobalComment);
		this.globalReturnEntriesSelection.addEventListener("onCheck", event -> this.selectAllEntries());
		this.refundDeliveryCost.addEventListener("onClick", event -> this.calculateTotalRefundAmount());
	}

	protected void handleIndividualAmountToReturn(final Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
		final Row myRow = (Row) event.getTarget().getParent();
		final ReturnEntryToCreateDto returnEntryDto = (ReturnEntryToCreateDto) myRow.getValue();
		final String refundAmountStr = ((InputEvent) event).getValue();
		final BigDecimal newAmount = refundAmountStr != null && !refundAmountStr.isEmpty()
				? BigDecimal.valueOf(Double.parseDouble(refundAmountStr))
				: BigDecimal.ZERO;
		returnEntryDto.getRefundEntry().setAmount(newAmount);
		this.applyToRow(newAmount.setScale(2, RoundingMode.CEILING).doubleValue(), 7, myRow);
		this.calculateIndividualTaxEstimate(returnEntryDto);
		this.calculateTotalRefundAmount();
	}

	protected void calculateIndividualTaxEstimate(final ReturnEntryToCreateDto returnEntryDto)
	{
		if (returnEntryDto.getQuantityToReturn() <= returnEntryDto.getReturnableQuantity())
		{
			final RefundEntryModel refundEntry = returnEntryDto.getRefundEntry();
			final Optional orderEntryOptional = refundEntry.getOrderEntry().getTaxValues().stream().findFirst();
			BigDecimal orderEntryTax = BigDecimal.ZERO;
			if (orderEntryOptional.isPresent())
			{
				orderEntryTax = BigDecimal.valueOf(((TaxValue) orderEntryOptional.get()).getValue());
			}
			if (refundEntry.getAmount().compareTo(BigDecimal.valueOf(refundEntry.getOrderEntry().getTotalPrice())) >= 0)
			{
				returnEntryDto.setTax(orderEntryTax);
			}
			else
			{
				final BigDecimal returnEntryTax = orderEntryTax.multiply(refundEntry.getAmount())
						.divide(BigDecimal.valueOf(refundEntry.getOrderEntry().getTotalPrice()), RoundingMode.HALF_UP)
						.setScale(refundEntry.getOrderEntry().getOrder().getCurrency().getDigits(), RoundingMode.HALF_UP);
				returnEntryDto.setTax(returnEntryTax);
			}
		}
	}

	protected void handleIndividualQuantityToReturn(final Event event)
	{
		this.autoSelect(event);
		final Row myRow = (Row) event.getTarget().getParent();
		final ReturnEntryToCreateDto myReturnEntry = (ReturnEntryToCreateDto) myRow.getValue();
		final String returnQuantityStr = ((InputEvent) event).getValue();
		final int amountEntered = returnQuantityStr != null && !returnQuantityStr.isEmpty() ? Integer.parseInt(returnQuantityStr)
				: 0;
		this.calculateRowAmount(myRow, myReturnEntry, amountEntered);
	}

	protected void handleIndividualRefundReason(final Event event)
	{
		final Optional<RefundReason> refundReason = this.getCustomSelectedRefundReason(event);
		if (refundReason.isPresent())
		{
			this.autoSelect(event);
			((ReturnEntryToCreateDto) ((Row) event.getTarget().getParent()).getValue()).getRefundEntry()
					.setReason(refundReason.get());
		}
	}

	protected void handleGlobalComment(final Event event)
	{
		this.applyToGrid(((InputEvent) event).getValue(), 9);
		this.returnEntries.getRows().getChildren().stream()
				.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach(entry -> ((ReturnEntryToCreateDto) ((Row) entry).getValue())
						.setRefundEntryComment(((InputEvent) event).getValue()));
	}

	protected void handleGlobalReason(final Event event)
	{
		final Optional<RefundReason> refundReason = this.getSelectedRefundReason(event);
		if (refundReason.isPresent())
		{
			this.applyToGrid(this.getReasonIndex(refundReason.get()), 8);
			this.returnEntries.getRows().getChildren().stream()
					.filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked()).forEach(
							entry -> ((ReturnEntryToCreateDto) ((Row) entry).getValue()).getRefundEntry().setReason(refundReason.get()));
		}
	}

	protected void calculateRowAmount(final Row myRow, final ReturnEntryToCreateDto myReturnEntry, final int qtyEntered)
	{
		final BigDecimal newAmount = myReturnEntry.isDiscountApplied() ? BigDecimal.ZERO
				: BigDecimal.valueOf(myReturnEntry.getRefundEntry().getOrderEntry().getBasePrice() * qtyEntered);
		myReturnEntry.setQuantityToReturn(qtyEntered);
		myReturnEntry.getRefundEntry().setAmount(newAmount);
		this.applyToRow(newAmount.setScale(2, RoundingMode.HALF_EVEN).doubleValue(), 7, myRow);
		this.calculateIndividualTaxEstimate(myReturnEntry);
		this.calculateTotalRefundAmount();
	}

	protected void calculateTotalRefundAmount()
	{
		this.calculateEstimatedTax();
		Double calculatedRefundAmount = this.refundDeliveryCost.isChecked() ? this.getOrder().getDeliveryCost() : 0.0;
		calculatedRefundAmount = calculatedRefundAmount + this.returnEntriesToCreate.stream()
				.map(entry -> entry.getRefundEntry().getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();
		this.totalRefundAmount.setValue(
				BigDecimal.valueOf(calculatedRefundAmount).add(BigDecimal.valueOf(this.estimatedTax.doubleValue())).doubleValue());
	}

	protected void calculateEstimatedTax()
	{
		BigDecimal totalTax = this.returnEntriesToCreate.stream()
				.filter(returnEntryToCreate -> returnEntryToCreate.getQuantityToReturn() > 0 && returnEntryToCreate.getTax() != null)
				.map(ReturnEntryToCreateDto::getTax).reduce(BigDecimal.ZERO, BigDecimal::add);
		if (this.refundDeliveryCost.isChecked())
		{
			final BigDecimal deliveryCostTax = BigDecimal.valueOf(this.getOrder().getTotalTax()
					- this.getOrder().getEntries().stream().filter(entry -> !entry.getTaxValues().isEmpty())
							.mapToDouble(entry -> entry.getTaxValues().stream().findFirst().get().getValue()).sum());
			totalTax = totalTax.add(deliveryCostTax);
		}
		this.estimatedTax.setValue(totalTax.doubleValue());
	}

	protected void autoSelect(final Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
	}

	protected void handleRow(final Row row)
	{
		final ReturnEntryToCreateDto myEntry = (ReturnEntryToCreateDto) row.getValue();
		if (row.getChildren().iterator().next() instanceof Checkbox)
		{
			if (!((Checkbox) row.getChildren().iterator().next()).isChecked())
			{
				this.applyToRow(0, 6, row);
				this.applyToRow(null, 8, row);
				this.applyToRow(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN).doubleValue(), 7, row);
				this.applyToRow(null, 9, row);
				myEntry.setQuantityToReturn(0);
				myEntry.getRefundEntry().setAmount(BigDecimal.ZERO);
				myEntry.getRefundEntry().setReason(null);
				myEntry.setRefundEntryComment(null);
			}
			else
			{
				this.applyToRow(this.globalReason.getSelectedIndex(), 8, row);
				this.applyToRow(this.globalComment.getValue(), 9, row);
				final Optional<RefundReason> reason = this.matchingComboboxReturnReason(
						this.globalReason.getSelectedItem() != null ? this.globalReason.getSelectedItem().getLabel() : null);
				myEntry.getRefundEntry().setReason(reason.isPresent() ? reason.get() : null);
				myEntry.setRefundEntryComment(this.globalComment.getValue());
			}
		}
		this.calculateTotalRefundAmount();
	}

	protected void selectAllEntries()
	{
		this.applyToGrid(Boolean.TRUE, 0);
		for (final Component row : this.returnEntries.getRows().getChildren())
		{
			final Component firstComponent = row.getChildren().iterator().next();
			if (firstComponent instanceof Checkbox)
			{
				((Checkbox) firstComponent).setChecked(this.globalReturnEntriesSelection.isChecked());
			}
			this.handleRow((Row) row);
			if (!this.globalReturnEntriesSelection.isChecked())
			{
				continue;
			}
			final int returnableQty = Integer.parseInt(((Label) row.getChildren().get(5)).getValue());
			this.applyToRow(returnableQty, 6, row);
			this.calculateRowAmount((Row) row, (ReturnEntryToCreateDto) ((Row) row).getValue(), returnableQty);
		}
		if (this.globalReturnEntriesSelection.isChecked())
		{
			this.returnEntriesToCreate.forEach(entry -> entry.setQuantityToReturn(entry.getReturnableQuantity()));
			this.calculateTotalRefundAmount();
		}
	}

	protected int getReasonIndex(final RefundReason refundReason)
	{
		int index = 0;
		final String myReason = this.getEnumerationService().getEnumerationName(refundReason,
				this.getCockpitLocaleService().getCurrentLocale());
		for (final String reason : this.refundReasons)
		{
			if (myReason.equals(reason))
			{
				break;
			}
			++index;
		}
		return index;
	}

	protected Optional<RefundReason> getSelectedRefundReason(final Event event)
	{
		Optional<RefundReason> result = Optional.empty();
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			final Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			result = this.matchingComboboxReturnReason(selectedValue.toString());
		}
		return result;
	}

	protected Optional<RefundReason> getCustomSelectedRefundReason(final Event event)
	{
		Optional<RefundReason> reason = Optional.empty();
		if (event.getTarget() instanceof Combobox)
		{
			final Object selectedValue = event.getData();
			reason = this.matchingComboboxReturnReason(selectedValue.toString());
		}
		return reason;
	}

	protected void applyToGrid(final Object data, final int childrenIndex)
	{
		for (final Component row : this.returnEntries.getRows().getChildren())
		{
			final Component firstComponent = row.getChildren().iterator().next();
			if (!(firstComponent instanceof Checkbox) || !((Checkbox) firstComponent).isChecked())
			{
				continue;
			}
			this.applyToRow(data, childrenIndex, row);
		}
	}

	protected void applyToRow(final Object data, final int childrenIndex, final Component row)
	{
		int index = 0;
		for (final Component myComponent : row.getChildren())
		{
			if (index != childrenIndex)
			{
				++index;
				continue;
			}
			if (myComponent instanceof Checkbox && data != null)
			{
				((Checkbox) myComponent).setChecked(((Boolean) data).booleanValue());
			}
			if (myComponent instanceof Combobox)
			{
				if (!(data instanceof Integer))
				{
					((Combobox) myComponent).setSelectedItem(null);
				}
				else
				{
					((Combobox) myComponent).setSelectedIndex(((Integer) data).intValue());
				}
			}
			else if (myComponent instanceof Intbox)
			{
				((Intbox) myComponent).setValue((Integer) data);
			}
			else if (myComponent instanceof Doublebox)
			{
				((Doublebox) myComponent).setValue((Double) data);
			}
			else if (myComponent instanceof Textbox)
			{
				((Textbox) myComponent).setValue((String) data);
			}
			++index;
		}
	}

	@ViewEvent(componentID = "resetcreatereturnrequest", eventName = "onClick")
	public void reset()
	{
		this.globalReason.setSelectedItem(null);
		this.globalComment.setValue("");
		this.initCreateReturnRequestForm(this.getOrder());
		this.calculateTotalRefundAmount();
	}

	@ViewEvent(componentID = "confirmcreatereturnrequest", eventName = "onClick")
	public void confirmCreation()
	{
		this.validateRequest();
		try
		{
			final ReturnRequestModel returnRequest = this.getReturnService().createReturnRequest(this.getOrder());
			returnRequest.setRefundDeliveryCost(Boolean.valueOf(this.refundDeliveryCost.isChecked()));
			final ReturnStatus status = this.isReturnInstore.isChecked() ? ReturnStatus.RECEIVED : ReturnStatus.APPROVAL_PENDING;
			returnRequest.setStatus(status);
			this.getModelService().save(returnRequest);
			this.returnEntriesToCreate.stream().filter(entry -> entry.getQuantityToReturn() != 0).forEach(entry -> {
				final RefundEntryModel refundEntryModel = this.createRefundWithCustomAmount(returnRequest, entry);
			});
			this.applyReturnRequest(returnRequest);
			final CreateReturnEvent createReturnEvent = new CreateReturnEvent();
			createReturnEvent.setReturnRequest(returnRequest);
			this.getEventService().publishEvent(createReturnEvent);
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.SUCCESS, new Object[]
			{ String
					.valueOf(this.getWidgetInstanceManager().getLabel("customersupportbackoffice.createreturnrequest.confirm.success"))
					+ " - " + returnRequest.getRMA() });
		}
		catch (final Exception e)
		{
			LOG.info(e.getMessage(), e);
			this.getNotificationService().notifyUser("", "JustMessage", NotificationEvent.Level.FAILURE, new Object[]
			{ this.getWidgetInstanceManager().getLabel("customersupportbackoffice.createreturnrequest.confirm.error") });
		}
		this.sendOutput(OUT_CONFIRM, COMPLETED);
	}

	private void applyReturnRequest(final ReturnRequestModel returnRequest) throws OrderReturnRecordsHandlerException
	{
		try
		{
			this.getRefundService().apply(returnRequest.getOrder(), returnRequest);
		}
		catch (final IllegalStateException illegalStateException)
		{
			LOG.info("Order " + this.getOrder().getCode() + " Return record already in progress");
		}
	}

	protected RefundEntryModel createRefundWithCustomAmount(final ReturnRequestModel returnRequest,
			final ReturnEntryToCreateDto entry)
	{
		final ReturnAction actionToExecute = this.isReturnInstore.isChecked() ? ReturnAction.IMMEDIATE : ReturnAction.HOLD;
		final RefundEntryModel refundEntryToBeCreated = this.getReturnService().createRefund(returnRequest,
				entry.getRefundEntry().getOrderEntry(), entry.getRefundEntryComment(), Long.valueOf(entry.getQuantityToReturn()),
				actionToExecute, entry.getRefundEntry().getReason());
		refundEntryToBeCreated.setAmount(entry.getRefundEntry().getAmount());
		returnRequest.setSubtotal(returnRequest.getSubtotal().add(entry.getRefundEntry().getAmount()));
		this.getModelService().save(refundEntryToBeCreated);
		return refundEntryToBeCreated;
	}

	protected void validateReturnEntry(final ReturnEntryToCreateDto entry)
	{
		if (entry.getQuantityToReturn() > entry.getReturnableQuantity())
		{
			final InputElement quantity = (InputElement) this
					.targetFieldToApplyValidation(entry.getRefundEntry().getOrderEntry().getProduct().getCode(), 1, 6);
			throw new WrongValueException(quantity,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.invalid.quantity"));
		}
		if (entry.getRefundEntry().getReason() != null && entry.getQuantityToReturn() == 0)
		{
			final InputElement quantity = (InputElement) this
					.targetFieldToApplyValidation(entry.getRefundEntry().getOrderEntry().getProduct().getCode(), 1, 6);
			throw new WrongValueException(quantity,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.missing.quantity"));
		}
		if (entry.getRefundEntry().getReason() == null && entry.getQuantityToReturn() > 0)
		{
			final Combobox combobox = (Combobox) this
					.targetFieldToApplyValidation(entry.getRefundEntry().getOrderEntry().getProduct().getCode(), 1, 8);
			throw new WrongValueException(combobox,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.missing.reason"));
		}
		if (entry.getQuantityToReturn() > 0 && entry.getRefundEntry().getAmount().compareTo(BigDecimal.ZERO) <= 0)
		{
			final InputElement amountInput = (InputElement) this
					.targetFieldToApplyValidation(entry.getRefundEntry().getOrderEntry().getProduct().getCode(), 1, 7);
			throw new WrongValueException(amountInput,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.invalid.amount"));
		}
	}

	protected void validateRequest()
	{
		for (final Component row : this.getReturnEntries().getRows().getChildren())
		{
			InputElement returnQty;
			final Component firstComponent = row.getChildren().iterator().next();
			if (!(firstComponent instanceof Checkbox) || !((Checkbox) firstComponent).isChecked()
					|| !(returnQty = (InputElement) row.getChildren().get(6)).getRawValue().equals(0))
			{
				continue;
			}
			throw new WrongValueException(returnQty,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.missing.quantity"));
		}
		final ListModelList<ReturnEntryToCreateDto> modelList = (ListModelList) this.getReturnEntries().getModel();
		if (modelList.stream().allMatch(entry -> entry.getQuantityToReturn() == 0))
		{
			throw new WrongValueException(this.globalReturnEntriesSelection,
					this.getLabel("customersupportbackoffice.createreturnrequest.validation.missing.selectedLine"));
		}
		modelList.forEach(this::validateReturnEntry);
	}

	protected Component targetFieldToApplyValidation(final String stringToValidate, final int indexLabelToCheck,
			final int indexTargetComponent)
	{
		for (final Component component : this.returnEntries.getRows().getChildren())
		{
			final Label label = (Label) component.getChildren().get(indexLabelToCheck);
			if (!label.getValue().equals(stringToValidate))
			{
				continue;
			}
			return component.getChildren().get(indexTargetComponent);
		}
		return null;
	}

	protected Optional<RefundReason> matchingComboboxReturnReason(final String refundReasonLabel)
	{
		return this.getEnumerationService().getEnumerationValues(RefundReason.class).stream()
				.filter(reason -> this.getEnumerationService()
						.getEnumerationName(reason, this.getCockpitLocaleService().getCurrentLocale()).equals(refundReasonLabel))
				.findFirst();
	}

	protected void setTotalDiscounts()
	{
		Double totalDiscount = this.getOrder().getTotalDiscounts() != null ? this.getOrder().getTotalDiscounts() : 0.0;
		totalDiscount = totalDiscount + this.getOrder().getEntries().stream()
				.mapToDouble(entry -> entry.getDiscountValues().stream().mapToDouble(DiscountValue::getAppliedValue).sum()).sum();
		this.totalDiscounts.setValue(totalDiscount);
	}

	protected OrderModel getOrder()
	{
		return this.order;
	}

	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}

	public Grid getReturnEntries()
	{
		return this.returnEntries;
	}

	public void setReturnEntries(final Grid returnEntries)
	{
		this.returnEntries = returnEntries;
	}

	public ReturnService getReturnService()
	{
		return this.returnService;
	}

	public void setReturnService(final ReturnService returnService)
	{
		this.returnService = returnService;
	}

	public EventService getEventService()
	{
		return this.eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected EnumerationService getEnumerationService()
	{
		return this.enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected BackofficeLocaleService getCockpitLocaleService()
	{
		return this.cockpitLocaleService;
	}

	public void setCockpitLocaleService(final BackofficeLocaleService cockpitLocaleService)
	{
		this.cockpitLocaleService = cockpitLocaleService;
	}

	protected CockpitEventQueue getCockpitEventQueue()
	{
		return this.cockpitEventQueue;
	}

	public void setCockpitEventQueue(final CockpitEventQueue cockpitEventQueue)
	{
		this.cockpitEventQueue = cockpitEventQueue;
	}

	protected RefundService getRefundService()
	{
		return this.refundService;
	}

	public void setRefundService(final RefundService refundService)
	{
		this.refundService = refundService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}

	public void setNotificationService(final NotificationService notificationService)
	{
		this.notificationService = notificationService;
	}
}

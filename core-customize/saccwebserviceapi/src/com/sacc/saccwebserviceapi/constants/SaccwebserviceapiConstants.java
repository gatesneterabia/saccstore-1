/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwebserviceapi.constants;

/**
 * Global class for all Saccwebserviceapi constants. You can add global constants for your extension into this class.
 */
public final class SaccwebserviceapiConstants extends GeneratedSaccwebserviceapiConstants
{
	public static final String EXTENSIONNAME = "saccwebserviceapi";

	private SaccwebserviceapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "saccwebserviceapiPlatformLogo";
}

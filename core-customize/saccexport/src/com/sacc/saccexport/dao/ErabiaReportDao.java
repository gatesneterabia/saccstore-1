/**
 *
 */
package com.sacc.saccexport.dao;

import java.util.ArrayList;
import java.util.List;


/**
 * @author jaafarnaddaf
 *
 */
public interface ErabiaReportDao
{
	List<String> getReports(final String catalog, final String catalogVersion);

	/**
	 * Finds result for the provided query
	 *
	 * @param columns
	 *
	 * @param query
	 * @return List<String>
	 */
	List<ArrayList<String>> getResultForQuery(int columns, String query);

	/**
	 * Finds result for the provided query
	 *
	 * @param query
	 * @return List<String>
	 */
	List<String> getResultForQuery(String query);
}

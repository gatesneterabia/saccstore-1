package com.sacc.saccerpwebservice.controllers.fulfillment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sacc.saccfulfillment.saee.exceptions.SaeeException;
import com.sacc.saccwebhooksnotificationservice.saee.entry.NotificationMessage;
import com.sacc.saccwebhooksnotificationservice.saee.entry.NotificationRequest;
import com.sacc.saccwebhooksnotificationservice.saee.service.SaeeNotificationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/saee-fulfillment")
@Api(value = "/saee-fulfillment", description = "Saee fulfillment notification")
public class SaeeFulfillmentNotificationController
{

	private static final Logger LOG = LoggerFactory.getLogger(SaeeFulfillmentNotificationController.class);

	@Resource(name="saeeNotificationService")
	private SaeeNotificationService saeeNotificationService;

	@RequestMapping(value = "/{baseStoreId}/notify", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map> notifyConsignment(final HttpServletRequest request, @RequestBody
	final LinkedHashMap<String, Object> notificationRequest, @ApiParam(value = "Base site identifier.", required = true)
	@PathVariable
	final String baseStoreId) throws IOException, SaeeException
	{
		LOG.info("SaeeFulfillmentNotificationController : Recieved a request of : {}", notificationRequest.toString());

		final String secretKey = request.getHeader("authorization");

		final NotificationRequest notiRequest = buildNotificationRequest(notificationRequest);

		saeeNotificationService.notifyConsignment(baseStoreId, secretKey, notiRequest);

		final Map <String, Object> responseMap = new HashMap();
      responseMap.put("response_code", "200");
      responseMap.put("message", "Notification Received successfully");
		return ResponseEntity.ok(responseMap);
	}

	private NotificationRequest buildNotificationRequest(final LinkedHashMap<String, Object> requestBody)
	{
		final NotificationRequest request = new NotificationRequest();
		request.setEvent(getRequestEvent(requestBody));
		request.setStatus(getRequestStatus(requestBody));
		final NotificationMessage message = new NotificationMessage();
		message.setWaybills(getAllWayBills(requestBody));
		message.setWaybill(getRequestMessageWaybill(requestBody));
		request.setMessage(message);
		return request;
	}

	/**
	 * @param requestBody
	 * @return
	 */
	private String getRequestEvent(final LinkedHashMap<String, Object> requestBody)
	{
		return String.valueOf((requestBody.get("event")));
	}

	private String getRequestStatus(final LinkedHashMap<String, Object> requestBody)
	{
		return ((String) (requestBody.get("status")));
	}

	private String getRequestMessageWaybill(final LinkedHashMap<String, Object> requestBody) {
		return (String) ((LinkedHashMap<String, Object>) (requestBody.get("message"))).get("waybill");
	}

	private List<String> getAllWayBills(final LinkedHashMap<String, Object> requestBody)
	{
		final List<String> waybills = new ArrayList();
		if (requestBody.get("message") != null
				&& ((LinkedHashMap<String, Object>) (requestBody.get("message"))).get("waybills") != null)
		{
			waybills.addAll((List<String>) ((LinkedHashMap<String, Object>) (requestBody.get("message"))).get("waybills"));
		}
		if (requestBody.get("message") != null
				&& ((LinkedHashMap<String, Object>) (requestBody.get("message"))).get("waybill") != null)
		{
			waybills.add((String) ((LinkedHashMap<String, Object>) (requestBody.get("message"))).get("waybill"));
		}

		return waybills;
	}

}

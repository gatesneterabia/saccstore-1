/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.omsfulfillment;

import de.hybris.bootstrap.annotations.UnitTest;
import com.sacc.omsfulfillment.cancellation.SapCpiOmsEnterCancellingStrategyTest;
import com.sacc.omsfulfillment.inbound.helper.SapCipOmsInboundHelperTest;
import com.sacc.omsfulfillment.inbound.helper.SapCipOmsInboundOrderHelperTest;
import com.sacc.omsfulfillment.strategy.SapConsignmentPreFulfillmentStrategyTest;
import com.sacc.omsfulfillment.strategy.SapSendConsignmentToExternalFulfillmentSystemStrategyTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@UnitTest
@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                SapCipOmsInboundHelperTest.class,
                SapCipOmsInboundOrderHelperTest.class,
                SapConsignmentPreFulfillmentStrategyTest.class,
                SapSendConsignmentToExternalFulfillmentSystemStrategyTest.class,
                SapCpiOmsEnterCancellingStrategyTest.class
        })
public class SapCpiOmsFulfillmentTestSuite {
  // To run the test suite
}
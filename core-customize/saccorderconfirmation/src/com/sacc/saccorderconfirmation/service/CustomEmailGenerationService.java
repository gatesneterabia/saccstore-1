/**
 *
 */
package com.sacc.saccorderconfirmation.service;

import de.hybris.platform.acceleratorservices.email.EmailGenerationService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomEmailGenerationService extends EmailGenerationService
{

}

/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccthirdpartyauthenticationbackoffice.services;

/**
 * Hello World SaccthirdpartyauthenticationbackofficeService
 */
public class SaccthirdpartyauthenticationbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}

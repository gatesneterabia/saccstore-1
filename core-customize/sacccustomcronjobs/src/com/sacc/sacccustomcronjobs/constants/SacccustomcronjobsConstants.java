/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomcronjobs.constants;

/**
 * Global class for all Sacccustomcronjobs constants. You can add global constants for your extension into this class.
 */
public final class SacccustomcronjobsConstants extends GeneratedSacccustomcronjobsConstants
{
	public static final String EXTENSIONNAME = "sacccustomcronjobs";

	private SacccustomcronjobsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "sacccustomcronjobsPlatformLogo";
}

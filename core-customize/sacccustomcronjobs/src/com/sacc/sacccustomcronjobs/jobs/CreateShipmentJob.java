/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomcronjobs.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.sacccustomcronjobs.model.CreateShipmentCronJobModel;
import com.sacc.saccfulfillment.context.FulfillmentContext;
import com.sacc.saccfulfillment.exception.FulfillmentException;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CreateShipmentJob extends AbstractJobPerformable<CreateShipmentCronJobModel>
{
	protected static final Logger LOG = Logger.getLogger(CreateShipmentJob.class);
	private static final String CRONJOB_FINISHED = "CreateShipmentJob is Finished ...";
	private static final String CREATING_SHIPMENT_FOR_CONSIGNMENT = "Creating shipment for consignment: ";
	private static final String SHIPMENT_CREATED_FOR_CONSIGNMENT = "Shipment created for consignment: ";
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";
	private static final String TRACKING_ID = "Tracking ID: ";
	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public PerformResult perform(final CreateShipmentCronJobModel cronjob)
	{
		LOG.info("CreateShipmentJob is Starting ...");
		if (CollectionUtils.isEmpty(cronjob.getConsignments()))
		{
			LOG.info("No consignments found to create shipment for");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		cronjob.getConsignments().stream().filter(Objects::nonNull).forEach(c -> {
			LOG.info(CREATING_SHIPMENT_FOR_CONSIGNMENT + c.getCode());
			try
			{
				final Optional<String> trackingID = fulfillmentContext.createShipment(c);
				if (trackingID.isPresent() && !StringUtils.isEmpty(trackingID.get()))
				{
					LOG.info(SHIPMENT_CREATED_FOR_CONSIGNMENT + c.getCode());
					LOG.info(TRACKING_ID + trackingID.get());
				}
				else
				{
					LOG.error("Error getting Tracking ID for consignment: " + c.getCode());
				}
			}
			catch (final FulfillmentException e)
			{
				LOG.error("An exception occurred while creating the shipment for consignment: " + c.getCode());
				LOG.error("Fulfillment Exception type: " + e.getType());
				LOG.error(e.getMessage(), e);
				getEventService().publishEvent(new SendErrorEmailEvent(getIntegrationProviderType(c.getOrder().getStore()),
						c.getOrder(), e.getCause() != null ? e.getCause().getMessage() : e.getMessage()));
			}
		});
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private IntegrationProvider getIntegrationProviderType(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		if (StringUtils.isEmpty(baseStoreModel.getFulfillmentProvider()))
		{
			return null;
		}
		switch (baseStoreModel.getFulfillmentProvider().toUpperCase())
		{
			case "SMSAFULFILLMENTPROVIDER":
				return IntegrationProvider.SMSA;
			case "SAEEFULFILLMENTPROVIDER":
				return IntegrationProvider.SAEE;
			default:
				return null;
		}
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
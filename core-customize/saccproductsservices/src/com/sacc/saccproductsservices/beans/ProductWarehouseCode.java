/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.beans;

/**
 *
 */
public class ProductWarehouseCode
{
	private final String ProductCode;
	private final String WarehouseCode;

	/**
	 *
	 */
	public ProductWarehouseCode(final String productString, final String wareString)
	{
		this.ProductCode = productString;
		this.WarehouseCode = wareString;
	}

	@Override
	public boolean equals(final Object obj)
	{
		return this.toString().equals(obj);
	}

	@Override
	public String toString()
	{
		return ProductCode + WarehouseCode;
	}
}

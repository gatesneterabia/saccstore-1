/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.beans;

import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class SuccessfulResult
{
	private final List<Object> updatedRecords;
	private final List<Object> createdRecords;


	/**
	 *
	 */
	public SuccessfulResult()
	{
		super();
		this.updatedRecords = new ArrayList<>();
		this.createdRecords = new ArrayList<>();
	}


	/**
	 * @return the updatedRecords
	 */
	public List<Object> getUpdatedRecords()
	{
		return updatedRecords;
	}


	/**
	 * @return the createdRecords
	 */
	public List<Object> getCreatedRecords()
	{
		return createdRecords;
	}



}

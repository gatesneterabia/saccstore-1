/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductsservices.beans;

import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class Result
{
	private final SuccessfulResult successful;
	private final List<FailedRecord> failed;

	/**
	 *
	 */
	public Result()
	{
		super();
		this.failed = new ArrayList<>();
		this.successful = new SuccessfulResult();
	}

	/**
	 * @return the successful
	 */
	public SuccessfulResult getSuccessful()
	{
		return successful;
	}

	/**
	 * @return the failed
	 */
	public List<FailedRecord> getFailed()
	{
		return failed;
	}




}

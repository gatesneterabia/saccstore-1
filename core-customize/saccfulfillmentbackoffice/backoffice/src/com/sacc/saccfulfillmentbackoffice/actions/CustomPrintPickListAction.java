package com.sacc.saccfulfillmentbackoffice.actions;

import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionContext;
import org.slf4j.LoggerFactory;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.warehousingbackoffice.labels.strategy.ConsignmentPrintDocumentStrategy;
import de.hybris.platform.core.model.order.OrderModel;
import javax.annotation.Resource;
import org.slf4j.Logger;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import com.hybris.cockpitng.actions.CockpitAction;

import java.util.Objects;

public class CustomPrintPickListAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
    private static final Logger LOG;
    protected static final String PICKING_TEMPLATE_CODE = "NPR_Picking";
    @Resource
    private ConsignmentPrintDocumentStrategy consignmentPrintPickSlipStrategy;
    @Resource
    private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;
    
    static {
        LOG = LoggerFactory.getLogger(CustomPrintPickListAction.class);
    }
    
    public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext) {
        final ActionResult<ConsignmentModel> result = (ActionResult<ConsignmentModel>)new ActionResult("success");
        final ConsignmentModel consignment = (ConsignmentModel)consignmentModelActionContext.getData();
        final WorkflowActionModel pickWorkflowAction = getWarehousingConsignmentWorkflowService().getWorkflowActionForTemplateCode(PICKING_TEMPLATE_CODE, consignment);
        if (pickWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals(pickWorkflowAction.getStatus())) {
            getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignment, PICKING_TEMPLATE_CODE, "pickConsignment");
        }
        CustomPrintPickListAction.LOG.info("Generating Pick Label for consignment {}", consignment.getCode());
        getConsignmentPrintPickSlipStrategy().printDocument(consignment);
        return result;
    }
    
    public boolean canPerform(final ActionContext<ConsignmentModel> consignmentModelActionContext) {
        final ConsignmentModel consignment = consignmentModelActionContext.getData();
        return !Objects.isNull(consignment) && !Objects.isNull(consignment.getOrder()) && consignment.getFulfillmentSystemConfig() == null && !((OrderModel) consignment.getOrder()).isSyncPartialCancelFailed();
    }
    
    public boolean needsConfirmation(final ActionContext<ConsignmentModel> consignmentModelActionContext) {
        return false;
    }
    
    public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext) {
        return null;
    }
    
    protected ConsignmentPrintDocumentStrategy getConsignmentPrintPickSlipStrategy() {
        return consignmentPrintPickSlipStrategy;
    }
    
    protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService() {
        return warehousingConsignmentWorkflowService;
    }
}
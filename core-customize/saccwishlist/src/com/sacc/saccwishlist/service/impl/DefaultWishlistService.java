/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccwishlist.service.impl;

import de.hybris.platform.cms2.data.PageableData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.sacc.saccwishlist.dao.WishlistDao;
import com.sacc.saccwishlist.exception.WishlistException;
import com.sacc.saccwishlist.exception.enums.WishlistExceptionType;
import com.sacc.saccwishlist.service.WishlistService;
import com.google.common.base.Preconditions;


/**
 *
 *
 * @author mohammad-abu-muhasien
 */

public class DefaultWishlistService extends DefaultWishlist2Service implements WishlistService
{

	private static final Logger LOG = Logger.getLogger(DefaultWishlistService.class);
	private static final String DESIRED_CAN_NOT_BE_NULL = "desired can not be null";

	private static final String PAGEABLE_DATA_CAN_NOT_BE_NULL = "pageableData can not be null";

	private static final String NEW_NAME_CAN_T_BE_NULL_OR_EMPTY = "newName can't be null or empty";

	/** The Constant PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY. */
	private static final String PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY = "productCode can't be null or empty";

	/** The Constant WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY. */
	private static final String WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY = "wishlistPK can't be null or empty";

	private static final String CMSSITE_CANT_BE_NULL_OR_EMPTY = "CMSSite can't be null or empty";
	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The user service. */
	@Resource(name = "userService")
	private UserService userService;

	/** The wishlist dao. */
	@Resource(name = "customWishlistDao")
	private WishlistDao customWishlistDao;

	/** The product service. */
	@Resource(name = "productService")
	private ProductService productService;

	/** The enumeration service. */
	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;



	/**
	 * Removes the all wishlist entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws WishlistException
	 */
	@Override
	public void removeAllWishlistEntries(final String wishlistPK) throws WishlistException
	{
		validateRegistration();
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPK);
		wishlist2Model.ifPresent(e -> {
			final Wishlist2Model wishlist2Model2 = wishlist2Model.get();
			if (!CollectionUtils.isEmpty(wishlist2Model2.getEntries()))
			{
				modelService.removeAll(wishlist2Model2.getEntries());
				wishlist2Model2.setEntries(Collections.emptyList());
				modelService.save(wishlist2Model.get());
			}
		});




	}



	/**
	 * Edits the wishlist name.
	 *
	 * @param newName
	 *           the new name
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist 2 model
	 * @throws WishlistException
	 */
	@Override
	public Optional<Wishlist2Model> editWishlistName(final String newName, final String wishlistPK) throws WishlistException
	{
		validateRegistration();
		Preconditions.checkArgument(StringUtils.isNotEmpty(newName), NEW_NAME_CAN_T_BE_NULL_OR_EMPTY);
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPK);
		if (!wishlist2Model.isPresent())
		{
			return Optional.empty();
		}
		final Wishlist2Model wishlistModel = wishlist2Model.get();

		wishlistModel.setName(newName);

		modelService.save(wishlistModel);
		modelService.refresh(wishlistModel);

		return wishlist2Model;
	}

	/**
	 * Gets the wishlist entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param pageableData
	 *           the pageable data
	 * @return the wishlist entries
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(final String wishlistPK, final PageableData pageableData)
			throws WishlistException
	{
		validateRegistration();
		Preconditions.checkArgument(pageableData != null, PAGEABLE_DATA_CAN_NOT_BE_NULL);
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPK);
		if (!wishlist2Model.isPresent())
		{
			throw new WishlistException(WishlistExceptionType.WISHLIST_NOT_FOUND,
					String.format("There's no wishlist with pk [%s]", wishlistPK), null);
		}
		final Optional<List<Wishlist2EntryModel>> wishlistEntries = customWishlistDao.getWishlistEntries(wishlist2Model.get(),
				pageableData);
		if (!wishlistEntries.isPresent())
		{
			return Optional.<List<Wishlist2EntryModel>> empty();
		}
		return Optional.<List<Wishlist2EntryModel>> of(wishlistEntries.get());
	}


	/**
	 * Removes the wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @throws WishlistException
	 */
	@Override
	public void removeWishList(final String wishlistPK) throws WishlistException
	{
		validateRegistration();
		Preconditions.checkArgument(StringUtils.isNotEmpty(wishlistPK), WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY);
		final Optional<Wishlist2Model> wishlistByPK = getWishlistByPK(wishlistPK);
		if (!wishlistByPK.isPresent())
		{
			throw new WishlistException(WishlistExceptionType.WISHLIST_NOT_FOUND,
					String.format("There's no wishlist with pk [%s]", wishlistPK), null);
		}
		modelService.remove(wishlistByPK.get());
	}

	/**
	 * Checks if is product in wish list.
	 *
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 * @throws WishlistException
	 */
	@Override
	public boolean isProductInWishList(final String productCode) throws WishlistException
	{
		validateRegistration();
		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);
		final UserModel currentUser = userService.getCurrentUser();
		final List<Wishlist2Model> wishlist = currentUser.getWishlist();

		return wishlist.stream().filter(w -> w != null && !CollectionUtils.isEmpty(w.getEntries()))
				.flatMap(w -> w.getEntries().stream())
				.anyMatch(w -> productCode != null && productCode.equals(w.getProduct().getCode()));
	}


	/**
	 * Gets the wish lists by current user.
	 *
	 * @return the wish lists by current user
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<Wishlist2Model>> getWishListsByCurrentUser() throws WishlistException
	{
		final UserModel currentUser = userService.getCurrentUser();
		return Optional.<List<Wishlist2Model>> ofNullable(currentUser.getWishlist());
	}


	/**
	 * Gets the wishlist entries.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist entries
	 * @throws WishlistException
	 */
	@Override
	public Optional<List<Wishlist2EntryModel>> getWishlistEntries(final String wishlistPK) throws WishlistException
	{
		validateRegistration();
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPK);
		if (!wishlist2Model.isPresent())
		{
			throw new WishlistException(WishlistExceptionType.WISHLIST_NOT_FOUND,
					String.format("There's no wishlist with pk [%s]", wishlistPK), null);
		}
		final Optional<List<Wishlist2EntryModel>> wishlistEntries = customWishlistDao.getWishlistEntries(wishlist2Model.get());
		if (!wishlistEntries.isPresent())
		{
			return Optional.<List<Wishlist2EntryModel>> empty();
		}
		return Optional.<List<Wishlist2EntryModel>> of(wishlistEntries.get());
	}



	/**
	 * Gets the wishlist entry by product code.
	 *
	 * @param productCode
	 *           the product code
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wishlist entry by product code
	 * @throws WishlistException
	 */
	@Override
	public Optional<Wishlist2EntryModel> getWishlistEntryByProductCode(final String productCode, final String wishlistPK)
			throws WishlistException
	{
		validateRegistration();

		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNotEmpty(wishlistPK), WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY);

		final Optional<Wishlist2Model> wishListByPK = getWishlistByPK(wishlistPK);
		if (!wishListByPK.isPresent() || wishListByPK.get().getEntries().isEmpty())
		{
			return Optional.empty();
		}

		return wishListByPK.get().getEntries().stream()
				.filter(w -> w != null && w.getProduct() != null && productCode.equals(w.getProduct().getCode())).findFirst();

	}

	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 */
	@Override
	public void removeWishlistEntryForProduct(final String productCode) throws WishlistException
	{
		validateRegistration();

		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);

		final List<Wishlist2Model> wishlists = getWishlists();

		wishlists.stream().forEach(w -> {
			w.getEntries().stream().forEach(entry -> {
				if (entry != null && entry.getProduct() != null && productCode.equals(entry.getProduct().getCode()))
				{
					removeWishlistEntry(w, entry);
				}
			});
		});



	}


	/**
	 * Removes the wishlist entry for product.
	 *
	 * @param wishlistPk
	 *           the wishlist pk
	 * @param productCode
	 *           the product code
	 * @throws WishlistException
	 */
	@Override
	public void removeWishlistEntryForProduct(final String wishlistPk, final String productCode) throws WishlistException
	{
		validateRegistration();

		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNotEmpty(wishlistPk), WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY);
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPk);

		wishlist2Model.ifPresent(ex -> {
			wishlist2Model.get()
					.setEntries(wishlist2Model.get().getEntries().stream()
							.filter(e -> e != null && e.getProduct() != null && !productCode.equals(e.getProduct().getCode()))
							.collect(Collectors.toList()));

			modelService.save(wishlist2Model.get());

		});
	}



	/**
	 * Checks if is product in wish list.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @param productCode
	 *           the product code
	 * @return true, if is product in wish list
	 * @throws WishlistException
	 */
	@Override
	public boolean isProductInWishList(final String wishlistPK, final String productCode) throws WishlistException
	{
		validateRegistration();

		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);
		Preconditions.checkArgument(StringUtils.isNotEmpty(wishlistPK), WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY);

		final Optional<Wishlist2Model> wishListByPK = getWishlistByPK(wishlistPK);
		if (!wishListByPK.isPresent())
		{
			return false;

		}
		return wishListByPK.get().getEntries().stream().filter(Objects::nonNull)
				.anyMatch(e -> productCode.equals(e.getProduct().getCode()));


	}


	/**
	 * Required login.
	 *
	 * @throws WishlistException
	 */
	protected void validateRegistration() throws WishlistException
	{
		if (userService.isAnonymousUser(userService.getCurrentUser()))
		{
			throw new WishlistException(WishlistExceptionType.LOGIN_REQUIRED, "login is required", null);
		}
	}


	/**
	 * Gets the wish list by pk.
	 *
	 * @param wishlistPK
	 *           the wishlist PK
	 * @return the wish list by pk
	 * @throws WishlistException
	 */
	@Override
	public Optional<Wishlist2Model> getWishlistByPK(final String wishlistPK) throws WishlistException

	{
		Preconditions.checkArgument(StringUtils.isNotEmpty(wishlistPK), WISHLIST_PK_CAN_T_BE_NULL_OR_EMPTY);
		validateRegistration();
		Wishlist2Model wishlist2Model = null;
		try
		{
			wishlist2Model = modelService.get(PK.parse(wishlistPK));
			return Optional.of(wishlist2Model);
		}
		catch (final Exception e)
		{
			throw new WishlistException(WishlistExceptionType.WISHLIST_NOT_FOUND,
					String.format("There's no wishlist with pk [%s]", wishlistPK), null);

		}


	}



	@Override
	public int getWishlistEntriesCount(final Wishlist2Model wishlist, final PageableData pageableData) throws WishlistException
	{
		validateRegistration();
		return (wishlist == null || pageableData == null) ? 0 : customWishlistDao.getWishlistEntriesCount(wishlist, pageableData);
	}

	@Override
	public void addWishlistEntryWithCapacitySite(final String wishlistPK, final String productCode, final Integer desired,
			Wishlist2EntryPriority entryPriority, final String comment, final CMSSiteModel cmsSiteModel) throws WishlistException
	{
		Preconditions.checkArgument(StringUtils.isNotEmpty(productCode), PRODUCT_CODE_CAN_T_BE_NULL_OR_EMPTY);

		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_CANT_BE_NULL_OR_EMPTY);
		validateRegistration();
		final Optional<Wishlist2Model> wishlist2Model = getWishlistByPK(wishlistPK);
		if (!wishlist2Model.isPresent())
		{
			throw new WishlistException(WishlistExceptionType.WISHLIST_NOT_FOUND,
					String.format("There's no wishlist with pk [%s]", wishlistPK), null);
		}

		final int wishlistCapacity = cmsSiteModel.getNumberOfWishlistItems() != null
				? cmsSiteModel.getNumberOfWishlistItems().intValue()
				: 50;
		if (wishlist2Model.get().getEntries().size() >= wishlistCapacity)
		{
			throw new WishlistException(WishlistExceptionType.MAX_CAPACITY,
					String.format("wishlist pk [%s] wishlist is max capacity", wishlistPK), "" + wishlistCapacity);
		}
		ProductModel productModel = null;
		try
		{
			productModel = productService.getProductForCode(productCode);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error(e.getMessage());
			throw new WishlistException(WishlistExceptionType.PRODUCT_NOT_FOUND,
					String.format("product code [%s] is not found", productCode), productCode);
		}
		if (productModel == null)
		{
			LOG.error(String.format("product code [%s] is not found", productCode));
			throw new WishlistException(WishlistExceptionType.PRODUCT_NOT_FOUND,
					String.format("product code [%s] is not found", productCode), productCode);
		}
		if (isProductInWishList(wishlistPK, productModel.getCode()))
		{
			return;
		}

		entryPriority = (entryPriority == null) ? Wishlist2EntryPriority.MEDIUM : entryPriority;
		addWishlistEntry(wishlist2Model.get(), productModel, desired, entryPriority, comment);

	}

}
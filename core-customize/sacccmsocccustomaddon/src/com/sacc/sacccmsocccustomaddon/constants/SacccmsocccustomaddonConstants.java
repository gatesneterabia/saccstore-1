/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccmsocccustomaddon.constants;

/**
 * Global class for all sacccmsocccustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class SacccmsocccustomaddonConstants extends GeneratedSacccmsocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "sacccmsocccustomaddon";

	public static final String PAGE_LABEL_ID = "pageLabelOrId";
	public static final String PAGE_ID = "pageId";
	public static final String PAGE_TYPE = "pageType";
	public static final String CODE = "code";

	private SacccmsocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}

<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xmlns:context="http://www.springframework.org/schema/context"
	   xmlns:util="http://www.springframework.org/schema/util"
	   xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd


           http://www.springframework.org/schema/util
           http://www.springframework.org/schema/util/spring-util.xsd
           http://www.springframework.org/schema/context
           http://www.springframework.org/schema/context/spring-context.xsd">
           
    <context:annotation-config/>
	<context:component-scan base-package="com.sacc.sacccmsocccustomaddon.controllers" />

	<!-- Web filter -->
	<bean id="cmsPreviewTicketFilter" class="com.sacc.sacccmsocccustomaddon.filter.CMSPreviewTicketFilter">
		<property name="cmsPreviewService" ref="cmsPreviewService" />
		<property name="commerceCommonI18NService" ref="commerceCommonI18NService" />
		<property name="baseSiteService" ref="baseSiteService" />
		<property name="catalogVersionService" ref="catalogVersionService" />
		<property name="sessionService" ref="sessionService" />
		<property name="timeService" ref="timeService" />
		<property name="pageRedirectStrategy" ref="cmsPageRedirectStrategy" />
	</bean>

	<bean depends-on="commerceWebServicesFilterChainListV2" parent="listMergeDirective">
		<property name="add" ref="cmsPreviewTicketFilter" />
		<property name="afterBeanNames">
			<list value-type="java.lang.String">
				<value>commerceWebServicesSessionLanguageFilterV2</value>
			</list>
		</property>
		<property name="beforeBeanNames">
			<list value-type="java.lang.String">
				<value>cxOccPersonalizationFilter</value>
				<value>guestRoleFilterV2</value>
			</list>
		</property>
	</bean>

	 <!-- Data mapper -->
    <alias alias="cmsDataMapper" name="defaultCMSDataMapper" />
	<bean id="defaultCMSDataMapper" class="com.sacc.sacccmsocccustomaddon.mapping.DefaultCMSDataMapper" parent="defaultDataMapper">
		<property name="converters" ref="cmsDataToWsConverterList" />
	</bean>

	<alias name="cmsCustomJaxbContextFactory" alias="cmsCustomJaxbContextFactory"/>
	<bean id="cmsCustomJaxbContextFactory" parent="customJaxbContextFactory">
		<property name="metadataSourceFactory" ref="customMetadataSourceFactory" />
		<property name="typeAdapters">
			<list merge="true">
				<value>com.sacc.sacccmsocccustomaddon.jaxb.adapters.components.ComponentListWsDTOAdapter</value>
				<value>com.sacc.sacccmsocccustomaddon.jaxb.adapters.components.ComponentWsDTOAdapter</value>
				<value>com.sacc.sacccmsocccustomaddon.jaxb.adapters.pages.PageWsDTOAdapter</value>
				<value>com.sacc.sacccmsocccustomaddon.jaxb.adapters.slots.SlotListWsDTOAdapter</value>
				<value>com.sacc.sacccmsocccustomaddon.jaxb.adapters.slots.SlotWsDTOAdapter</value>
			</list>
		</property>
	</bean>
	
	<alias name="cmsCustomJsonHttpMessageConverter" alias="customJsonHttpMessageConverter"/>
	<bean id="cmsCustomJsonHttpMessageConverter" class="com.sacc.sacccmsocccustomaddon.jaxb.CmsJaxb2HttpMessageConverter"
		  parent="defaultJsonHttpMessageConverter">
		<property name="jaxbContextFactory" ref="cmsCustomJaxbContextFactory" />
	</bean>
	
	<alias name="cmsCustomXmlHttpMessageConverter" alias="customXmlHttpMessageConverter"/>
	<bean id="cmsCustomXmlHttpMessageConverter" class="com.sacc.sacccmsocccustomaddon.jaxb.CmsJaxb2HttpMessageConverter"
		  parent="defaultXmlHttpMessageConverter">
		<property name="jaxbContextFactory" ref="cmsCustomJaxbContextFactory" />
	</bean>
	
	<alias name="defaultComponentNodeSuffixesToRemove" alias="nodeSuffixesToRemove"/>
	<util:list id="defaultComponentNodeSuffixesToRemove">
		<value>AdaptedData</value>
		<value>WsDTO</value>
		<value>Data</value>
	</util:list>
	
	<bean id="contentNegotiationManager" class="org.springframework.web.accept.ContentNegotiationManagerFactoryBean">
		<property name="favorPathExtension" value="false" />
	</bean>

	<bean id="abstractDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.AbstractDataToWsConverter" abstract="true">
		<property name="mapper" ref="cmsDataMapper" />
	</bean>

	<alias name="defaultPageDataToWsConverter" alias="cmsPageDataToWsConverter" />
	<bean id="defaultPageDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.PageDataToWsConverter" parent="abstractDataToWsConverter" />

	<alias name="defaultSlotDataToWsConverter" alias="cmsSotDataToWsConverter" />
	<bean id="defaultSlotDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.SlotDataToWsConverter" parent="abstractDataToWsConverter" />

	<alias name="defaultMediaDataToWsConverter" alias="cmsMediaDataToWsConverter" />
	<bean id="defaultMediaDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.MediaDataToWsConverter" parent="abstractDataToWsConverter" />

	<alias name="defaultNavigationNodeDataToWsConverter" alias="cmsNavigationNodeDataToWsConverter" />
	<bean id="defaultNavigationNodeDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.NavigationNodeDataToWsConverter" parent="abstractDataToWsConverter" />

	<alias name="defaultComponentDataToWsConverter" alias="cmsComponentDataToWsConverter" />
	<bean id="defaultComponentDataToWsConverter" class="com.sacc.sacccmsocccustomaddon.mapping.converters.ComponentDataToWsConverter" parent="abstractDataToWsConverter" />

	<alias name="defaultDataToWsConverterList" alias="cmsDataToWsConverterList" />
	<util:list id="defaultDataToWsConverterList">
		<ref bean="cmsPageDataToWsConverter"/>
		<ref bean="cmsSotDataToWsConverter"/>
		<ref bean="cmsMediaDataToWsConverter"/>
		<ref bean="cmsNavigationNodeDataToWsConverter"/>
		<ref bean="cmsComponentDataToWsConverter"/>
	</util:list>


	<!-- Cache manager for addon -->
	<alias name="defaultSacccmsocccustomaddonCacheManager" alias="sacccmsocccustomaddonCacheManager"/>
	<bean id="defaultSacccmsocccustomaddonCacheManager" class="org.springframework.cache.ehcache.EhCacheCacheManager">
		<property name="cacheManager" ref="sacccmsocccustomaddonEhcache"/>
	</bean>

	<alias name="defaultSacccmsocccustomaddonEhcache" alias="sacccmsocccustomaddonEhcache"/>
	<bean id="defaultSacccmsocccustomaddonEhcache" class="de.hybris.platform.webservicescommons.cache.TenantAwareEhCacheManagerFactoryBean">
		<property name="cacheNamePrefix" value="sacccmsocccustomaddonCache_"/>
		<property name="configLocation" value="/WEB-INF/cache/ehcache.xml"/>
	</bean>

	<bean depends-on="wsCacheManagerList" parent="listMergeDirective">
		<property name="add" ref="sacccmsocccustomaddonCacheManager"/>
	</bean>

	<!-- field set level -->
	<!-- It extends baseSiteWsDTOFieldSetLevelMapping bean from ycommercewebservices -->
	<bean parent="fieldSetLevelMapping" id="cmsSiteWsDTOFieldSetLevelMapping">
		<property name="dtoClass" value="de.hybris.platform.commercewebservicescommons.dto.basesite.BaseSiteWsDTO"/>
		<property name="levelMapping">
			<map>
				<entry key="FULL"
					   value="urlPatterns,defaultPreviewCatalogId,defaultPreviewCategoryCode,defaultPreviewProductCode"/>
			</map>
		</property>
	</bean>

	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.ContentSlotWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="slotId,slotUuid,position" />
				<entry key="DEFAULT" value="BASIC,name,slotShared,otherProperties" />
				<entry key="FULL" value="DEFAULT,slotStatus" />
			</map>
		</property>
	</bean>
	
	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.CMSPageWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="uid,uuid,title,template,typeCode" />
				<entry key="DEFAULT" value="BASIC,name,otherProperties" />
				<entry key="FULL" value="DEFAULT,defaultPage" />
			</map>
		</property>
	</bean>
	
	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.ComponentWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="uid,uuid,typeCode,modifiedtime" />
				<entry key="DEFAULT" value="BASIC,name,otherProperties" />
				<entry key="FULL" value="DEFAULT" />
			</map>
		</property>
	</bean>
	
	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.ComponentListWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="component" />
				<entry key="DEFAULT" value="component(DEFAULT)" />
				<entry key="FULL" value="component(FULL)" />
			</map>
		</property>
	</bean>
	
	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.MediaWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="code,url,altText" />
				<entry key="DEFAULT" value="BASIC,mime" />
				<entry key="FULL" value="DEFAULT,downloadUrl,description,catalogId" />
			</map>
		</property>
	</bean>
	
	<bean parent="fieldSetLevelMapping">
		<property name="dtoClass"
			value="com.sacc.sacccmsocccustomaddon.data.NavigationNodeWsDTO" />
		<property name="levelMapping">
			<map>
				<entry key="BASIC" value="uid,uuid,title,children,entries" />
				<entry key="DEFAULT" value="BASIC" />
				<entry key="FULL" value="DEFAULT,name,position" />
			</map>
		</property>
	</bean>
</beans>

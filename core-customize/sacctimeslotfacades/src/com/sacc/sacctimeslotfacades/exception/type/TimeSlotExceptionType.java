/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacctimeslotfacades.exception.type;

/**
 *
 */
public enum TimeSlotExceptionType
{
	NO_TIMESLOT_CONFIGURATIONS_AVAILABLE, NO_TIMESLOT_WEEKDAYS, NO_TIMEZONE_FOUND, INVALID_NUMBER_OF_DAYS;
}

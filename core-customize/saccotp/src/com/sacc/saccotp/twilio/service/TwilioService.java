/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccotp.twilio.service;

import com.sacc.saccotp.exception.OTPException;


/**
 * @author mnasro
 *
 *         The Interface TwilioService.
 */
public interface TwilioService
{

	/**
	 * Send OTP code.
	 *
	 * @param authToken
	 *           the auth token
	 * @param apiKey
	 *           the api key
	 * @param accountSid
	 *           the account sid
	 * @param countryCode
	 *           the country code
	 * @param messageTo
	 *           the message to
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean sendOTPCode(final String authToken, final String apiKey, final String accountSid, final String countryCode,
			final String messageTo) throws OTPException;

	/**
	 * Verify code.
	 *
	 * @param authToken
	 *           the auth token
	 * @param apiKey
	 *           the api key
	 * @param accountSid
	 *           the account sid
	 * @param countryCode
	 *           the country code
	 * @param messageTo
	 *           the message to
	 * @param code
	 *           the code
	 * @return true, if successful
	 * @throws OTPException
	 *            the OTP exception
	 */
	public boolean verifyCode(final String authToken, final String apiKey, final String accountSid, final String countryCode,
			final String messageTo,
			final String code) throws OTPException;

}

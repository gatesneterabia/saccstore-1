/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccotp.strategy.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccotp.model.OTPProviderModel;
import com.sacc.saccotp.model.TwilioOTPProviderModel;
import com.sacc.saccotp.strategy.OTPStrategy;
import com.sacc.saccotp.twilio.service.TwilioService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultTwilioOTPStrategy implements OTPStrategy
{

	@Resource(name = "twilioService")
	private TwilioService twilioService;

	@Override
	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof TwilioOTPProviderModel,
				"otpProviderModel cannot cast to TwilioOTPProviderModel");
		final TwilioOTPProviderModel providerModel = (TwilioOTPProviderModel) otpProviderModel;

		return twilioService.sendOTPCode(providerModel.getAuthToken(), providerModel.getApiKey(), providerModel.getAccountSid(),
				countryisoCode, mobileNumber);
	}

	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "code cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof TwilioOTPProviderModel,
				"otpProviderModel cannot cast to TwilioOTPProviderModel");
		final TwilioOTPProviderModel providerModel = (TwilioOTPProviderModel) otpProviderModel;

		return twilioService.verifyCode(providerModel.getAuthToken(), providerModel.getApiKey(), providerModel.getAccountSid(),
				countryisoCode, mobileNumber, code);
	}

}

package com.sacc.saccotp.strategy;

import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccotp.model.OTPProviderModel;

/**
 * @author mnasro
 *
 *         The Interface PaymentStrategy.
 */
public interface OTPStrategy
{

	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException;


	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException;

}

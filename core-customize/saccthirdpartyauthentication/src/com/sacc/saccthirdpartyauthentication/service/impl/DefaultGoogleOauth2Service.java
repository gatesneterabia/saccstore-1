package com.sacc.saccthirdpartyauthentication.service.impl;

import java.io.IOException;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.service.GoogleOauth2Service;


public class DefaultGoogleOauth2Service implements GoogleOauth2Service
{

	private static final Logger LOG = Logger.getLogger(DefaultGoogleOauth2Service.class);
	private static final String GOOGLE_LINK = "https://oauth2.googleapis.com/token";

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String code, final String clientId, final String clientSecret)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("GoogleBasicUserServiceImpl getData()");
		if (StringUtils.isEmpty(code) || StringUtils.isEmpty(clientSecret) || StringUtils.isEmpty(clientId))
		{
			LOG.error("code or clientSecret or clientId is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		GoogleTokenResponse tokenResponse = null;

		try
		{
			tokenResponse = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(), JacksonFactory.getDefaultInstance(),
					GOOGLE_LINK, clientId, clientSecret, code, "postmessage").execute();
		}
		catch (final IOException e)
		{
			LOG.error("Not Authorized");

			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		GoogleIdToken idToken = null;
		try
		{
			idToken = tokenResponse.parseIdToken();
		}
		catch (final IOException e)
		{
			LOG.error("Not Authorized");

			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final GoogleIdToken.Payload payload = idToken.getPayload();
		final String userId = payload.getSubject();
		final String email = payload.getEmail();

		final String name = (String) payload.get("name");

		final String familyName = (String) payload.get("family_name");
		final String givenName = (String) payload.get("given_name");

		final ThirdPartyAuthenticationUserData user = new ThirdPartyAuthenticationUserData();
		user.setId(userId);
		user.setEmail(email);
		user.setName(name);
		user.setLastName(familyName);
		user.setFirstName(givenName);

		return Optional.ofNullable(user);
	}

	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String appId, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(appId) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(id, appId, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}

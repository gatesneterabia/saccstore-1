/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.service;

import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 *
 */
public interface ThirdPartyUserService
{

	Boolean isUserExist(final String cutomerId);

	void saveUser(CustomerData user);

}

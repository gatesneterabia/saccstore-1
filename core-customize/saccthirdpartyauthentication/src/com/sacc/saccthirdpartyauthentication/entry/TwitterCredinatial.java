/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.entry;

/**
 *
 */
public class TwitterCredinatial


{
	private String consumerKey;
	private String consumerSecret;

	/**
	 *
	 */
	public TwitterCredinatial(final String consumerKey, final String consumerSecret)
	{
		super();
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
	}

	/**
	 * @return the consumerKey
	 */
	public String getConsumerKey()
	{
		return consumerKey;
	}

	/**
	 * @param consumerKey
	 *           the consumerKey to set
	 */
	public void setConsumerKey(final String consumerKey)
	{
		this.consumerKey = consumerKey;
	}

	/**
	 * @return the consumerSecret
	 */
	public String getConsumerSecret()
	{
		return consumerSecret;
	}

	/**
	 * @param consumerSecret
	 *           the consumerSecret to set
	 */
	public void setConsumerSecret(final String consumerSecret)
	{
		this.consumerSecret = consumerSecret;
	}



	/**
	 *
	 */

}

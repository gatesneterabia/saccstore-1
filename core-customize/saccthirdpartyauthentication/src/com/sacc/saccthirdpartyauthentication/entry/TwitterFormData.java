/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.entry;

/**
 *
 */
public class TwitterFormData
{
	private String src;
	private String ouathToken;
	private String ouathTokenSecret;



	/**
	 *
	 */
	public TwitterFormData(final String src, final String ouathToken, final String ouathTokenSecret)
	{
		super();
		this.src = src;
		this.ouathToken = ouathToken;
		this.ouathTokenSecret = ouathTokenSecret;
	}

	/**
	 * @return the src
	 */
	public String getSrc()
	{
		return src;
	}

	/**
	 * @param src
	 *           the src to set
	 */
	public void setSrc(final String src)
	{
		this.src = src;
	}

	/**
	 * @return the ouathToken
	 */
	public String getOuathToken()
	{
		return ouathToken;
	}

	/**
	 * @param ouathToken
	 *           the ouathToken to set
	 */
	public void setOuathToken(final String ouathToken)
	{
		this.ouathToken = ouathToken;
	}

	/**
	 * @return the ouathTokenSecret
	 */
	public String getOuathTokenSecret()
	{
		return ouathTokenSecret;
	}

	/**
	 * @param ouathTokenSecret
	 *           the ouathTokenSecret to set
	 */
	public void setOuathTokenSecret(final String ouathTokenSecret)
	{
		this.ouathTokenSecret = ouathTokenSecret;
	}

}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccthirdpartyauthentication.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccthirdpartyauthentication.context.ThirdPartyAuthenticationContext;
import com.sacc.saccthirdpartyauthentication.context.ThirdPartyAuthenticationProviderContext;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.sacc.saccthirdpartyauthentication.entry.TwitterFormData;
import com.sacc.saccthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.sacc.saccthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.sacc.saccthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.sacc.saccthirdpartyauthentication.model.FacebookAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.model.GoogleAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.model.TwitterAuthenticationProviderModel;
import com.sacc.saccthirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;


/**
 *
 */
public class DefaultThirdPartyAuthenticationContext implements ThirdPartyAuthenticationContext
{

	private static final String THIRDPARTY_AUTHENTICATION_STRATEGY_NOT_FOUND = "strategy not found";
	private static final String CMSSITE_NOT_FOUND = "cmsSite is null";
	private static final String CALLBACK_URL_NOT_FOUND = "callbackUrl is null";

	private static final String DATA_IS_NULL = "data is null";

	private static final String THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL = "thirdPartyAuthenticationType is null";


	@Resource(name = "thirdPartyAuthenticationStrategyMap")
	private Map<Class<?>, ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStrategyMap;

	@Resource(name = "thirdPartyAuthenticationProviderContext")
	private ThirdPartyAuthenticationProviderContext thirdPartyAuthenticationProviderContext;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	@Override
	public List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderData(
			final CMSSiteModel cmsSiteModel) throws ThirdPartyAuthenticationException
	{

		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		final List<ThirdPartyAuthenticationType> supportedProvider = cmsSiteModel.getSupportedProviders();
		if (CollectionUtils.isEmpty(supportedProvider))
		{
			return null;
		}
		final List<ThirdPartyAuthenticationProviderData> list = new ArrayList<ThirdPartyAuthenticationProviderData>();
		for (final ThirdPartyAuthenticationType type : supportedProvider)
		{
			Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
			switch (type)
			{
				case FACEBOOK:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
					break;
				case GOOGLE:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
					break;
				case TWITTER:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
					break;
				default:
					continue;

			}
			if (!thirdPartyAuthenticationProvider.isPresent())
			{
				return null;
			}
			final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
					thirdPartyAuthenticationProvider.get().getClass());
			final Optional<ThirdPartyAuthenticationProviderData> data = thirdPartyAuthenticationStratergy.get()
					.getThirdPartyAuthenticationProviderData(thirdPartyAuthenticationProvider.get());
			if (data.isPresent())
			{
				list.add(data.get());
			}
		}
		return list;
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationType type, final CMSSiteModel cmsSiteModel) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;

			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);

				break;

			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (!thirdPartyAuthenticationProvider.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND,
					ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND.getMsg());
		}

		final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
				thirdPartyAuthenticationProvider.get().getClass());

		return thirdPartyAuthenticationStratergy.get().getThirdPartyUserData(data, thirdPartyAuthenticationProvider.get());
	}

	protected Optional<ThirdPartyAuthenticationStrategy> getStrategy(final Class<?> providerClass)
	{
		final ThirdPartyAuthenticationStrategy strategy = getThirdPartyAuthenticationStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, THIRDPARTY_AUTHENTICATION_STRATEGY_NOT_FOUND);
		return Optional.ofNullable(strategy);
	}

	/**
	 * @return the thirdPartyAuthenticationStrategyMap
	 */
	public Map<Class<?>, ThirdPartyAuthenticationStrategy> getThirdPartyAuthenticationStrategyMap()
	{
		return thirdPartyAuthenticationStrategyMap;
	}

	/**
	 * @return the thirdPartyAuthenticationProviderContext
	 */
	public ThirdPartyAuthenticationProviderContext getThirdPartyAuthenticationProviderContext()
	{
		return thirdPartyAuthenticationProviderContext;
	}

	@Override
	public List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderDataByCurrentStore()
			throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getSupportedThirdPartyAuthenticationProviderData(cmsSiteModel);
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserDataByCurrentStore(final Object data,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyUserData(data, type, cmsSiteModel);
	}

	@Override
	public Optional<TwitterFormData> getThirdPartyFormData(final Object data, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSiteModel, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;
			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				Preconditions.checkArgument(callbackUrl != null, CALLBACK_URL_NOT_FOUND);
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
				break;
			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (!thirdPartyAuthenticationProvider.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND,
					ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND.getMsg());
		}

		final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
				thirdPartyAuthenticationProvider.get().getClass());

		return thirdPartyAuthenticationStratergy.get().getFormData(data, thirdPartyAuthenticationProvider.get(), callbackUrl);
	}

	@Override
	public Optional<TwitterFormData> getThirdPartyFormDataByCurrentStore(final Object data,
			final ThirdPartyAuthenticationType type, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyFormData(data, type, cmsSiteModel, callbackUrl);
	}

	@Override
	public boolean verifyAccessTokenWithThirdPartyByCurrentStore(final Object data, final Object token,
			final ThirdPartyAuthenticationType type, final String callbackUrl)
			throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return verifyAccessTokenWithThirdParty(data, token, type, cmsSiteModel, callbackUrl);
	}

	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object data, final Object token, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSiteModel, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;

			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
				break;

			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (!thirdPartyAuthenticationProvider.isPresent())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND,
					ThirdPartyAuthenticationExceptionType.PROVIDER_NOT_FOUND.getMsg());
		}

		final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
				thirdPartyAuthenticationProvider.get().getClass());

		return thirdPartyAuthenticationStratergy.get().verifyAccessTokenWithThirdParty(data, token,
				thirdPartyAuthenticationProvider.get());
	}

}

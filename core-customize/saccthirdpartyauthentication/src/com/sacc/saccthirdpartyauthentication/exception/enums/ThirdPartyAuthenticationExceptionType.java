package com.sacc.saccthirdpartyauthentication.exception.enums;

public enum ThirdPartyAuthenticationExceptionType
{

	BAD_REQUEST("bad request"), NON_AUTHORIZED("user not authorized"), INVALID_PROVIDER(
			"provider is invalid"), CALLBACK_NOT_AUTHPRIZED("callback url is not authorized"), INVALID_PROVIDER_TYPE(
					"provider type is invalid"), METHOD_NOT_SUPPORTED(
							"method is not supported"), PROVIDER_NOT_FOUND("no such provider"), REDIRECT_ERROR("error in redirect");



	private String msg;


	private ThirdPartyAuthenticationExceptionType(final String msg)
	{
		this.msg = msg;
	}


	public String getMsg()
	{
		return msg;
	}






}

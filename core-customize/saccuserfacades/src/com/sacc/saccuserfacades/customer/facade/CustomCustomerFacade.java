/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccuserfacades.customer.facade;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;


/**
 *
 */
public interface CustomCustomerFacade extends CustomerFacade
{

	CustomerData getCustomerByCustomerId(String id);

	boolean isCustomerExists(String customerId);

}

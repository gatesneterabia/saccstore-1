package com.sacc.saccuserfacades.customer.populator;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;


/**
 * The Class CustomerReversePopulator.
 *
 * @author mnasro
 */
public class CustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CustomerData source, final CustomerModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getMobileCountry() != null && !StringUtils.isEmpty(source.getMobileCountry().getIsocode()))
		{
			target.setMobileCountry(commonI18NService.getCountry(source.getMobileCountry().getIsocode()));
		}
		target.setMobileNumber(source.getMobileNumber());
	}
}

/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccuserfacades.address.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.sacc.saccuser.area.service.AreaService;
import com.sacc.saccuser.city.service.CityService;
import com.sacc.saccuser.model.AreaModel;
import com.sacc.saccuser.model.CityModel;


/**
 * The Class AddressReversePopulator.
 *
 * @author mnasro
 */
public class CustomAddressReversePopulator extends de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator
{

	/** The city service. */
	@Resource(name = "cityService")
	private CityService cityService;


	/** The area service. */
	@Resource(name = "areaService")
	private AreaService areaService;


	/**
	 * Fill the source to target.
	 *
	 * @param addressData
	 *           the address data
	 * @param addressModel
	 *           the address model
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel)
	{
		if (addressData == null)
		{
			return;
		}
		super.populate(addressData, addressModel);
		addressModel.setMobile(addressData.getMobileNumber());
		if (addressData.getMobileCountry() != null && !StringUtils.isEmpty(addressData.getMobileCountry().getIsocode()))
		{
			addressModel.setMobileCountry(getCommonI18NService().getCountry(addressData.getMobileCountry().getIsocode()));
		}

		if (addressData.getCity() != null && !StringUtils.isEmpty(addressData.getCity().getCode()))
		{
			final Optional<CityModel> city = cityService.get(addressData.getCity().getCode());
			if (city.isPresent())
			{
				addressModel.setCity(city.get());
			}
		}

		if (addressData.getArea() != null && !StringUtils.isEmpty(addressData.getArea().getCode()))
		{
			final Optional<AreaModel> area = getAreaService().get(addressData.getArea().getCode());
			if (area.isPresent())
			{
				addressModel.setArea(area.get());
			}

		}

		addressModel.setTown(addressModel.getTown());

		addressModel.setPostalcode(addressData.getPostalCode());

		addressModel.setLatitude(addressData.getLatitude());
		addressModel.setLongitude(addressData.getLongitude());
		addressModel.setNearestLandmark(addressData.getNearestLandmark());
		addressModel.setAddressName(addressData.getAddressName());
		addressModel.setDistrictName(addressData.getDistrictName());
	}


	/**
	 * @return the cityService
	 */
	protected CityService getCityService()
	{
		return cityService;
	}


	/**
	 * @return the areaService
	 */
	protected AreaService getAreaService()
	{
		return areaService;
	}


}

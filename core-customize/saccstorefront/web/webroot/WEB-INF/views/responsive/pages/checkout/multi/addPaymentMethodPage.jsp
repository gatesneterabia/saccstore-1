<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

    <div class="row">
        <div class="col-sm-6">
            <div class="checkout-headline">
                <span class="glyphicon glyphicon-lock"></span>
                <spring:theme code="checkout.multi.secure.checkout"/>
            </div>
            <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
                <jsp:body>
                    <ycommerce:testId code="checkoutStepFour">
                        <div class="checkout-paymentmethod">
                            <div class="checkout-indent">

                                <div class="headline"><spring:theme code="checkout.multi.paymentMethod"/></div>
							<spring:url var="selectPaymentMethodUrl" value="{contextPath}/checkout/multi/payment-method/add" htmlEscape="false" >
								<spring:param name="contextPath" value="${request.contextPath}" />
							</spring:url>
							
								<form:form method="post" modelAttribute="paymentDetailsForm" action="${selectPaymentMethodUrl}" id="selectPaymentMethodForm">
								
								<formElement:formSelectBoxDefaultEnabled idKey="payment.method."
		labelKey="payment.method.title" selectCSSClass="form-control hidden"
		path="paymentModeCode" mandatory="true" skipBlank="false"
		skipBlankMessageKey="form.select.none" items="${supportedPaymentModes}" />
								</form:form>
                                <c:forEach items="${supportedPaymentModes}" var="pm">
								<label class="container-radio" for="${pm.code}">
								<input id="${pm.code}" type="radio" name="paymentmothed"  value="${pm.code}"/>
								<c:if test="${pm.paymentModeType.code eq 'NOCARD'}"><i class="fal fa-sack-dollar"></i> ${pm.name}</c:if>
								<c:if test="${pm.paymentModeType.code eq 'CARD'}"><i class="fal fa-credit-card"></i> ${pm.name}</c:if>
								<c:if test="${pm.paymentModeType.code eq 'CONTINUE'}">${pm.name}</c:if>
								<span class="checkmark"></span>
								</label>
								
								
								</c:forEach>

<%-- 								<form id="selectPaymentMethodForm" action="${fn:escapeXml(selectPaymentMethodUrl)}" method="post"> --%>
<!-- 									<div class="form-group"> -->
<%-- 										<multi-checkout:paymentMethodSelector paymentMethods="${supportedPaymentModes}" selectedPaymentMethodId="${cartData.paymentMode.code}"/> --%>
<!-- 									</div> -->
<%-- 								</form> --%>

                            </div>
                        </div>
					<button id="paymentMethodSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.deliveryMethod.continue"/></button>

                    </ycommerce:testId>
                    
               </jsp:body>

            </multiCheckout:checkoutSteps>
		</div>

        <div class="col-sm-6 hidden-xs">
            <multiCheckout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
        </div>

		<div class="col-sm-12 col-lg-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
	</div>

</template:page>

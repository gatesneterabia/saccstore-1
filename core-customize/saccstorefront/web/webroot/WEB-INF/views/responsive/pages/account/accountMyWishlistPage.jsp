<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="wishlist" tagdir="/WEB-INF/tags/responsive/wishlist"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:choose>
<c:when test="${not empty searchPageData.results && not empty wishlistSelected}">
<div class="account-section-header wishlist_header">
    		<c:url value="/my-account/my-wishlist/addAllEntriesToCart" var="addAllToCartURL"/>
    		<div class="row">
			<div class="col-md-6 col-sm-4 col-xs-12 header_wishlist">
	<spring:theme code="wishlist.title.header.WISHLIST" />
	
	<c:choose>
		<c:when test="${searchPageData.pagination.totalNumberOfResults > 1}">
			(<spring:theme code="wishlist.page.totals.total.items" arguments="${searchPageData.pagination.totalNumberOfResults}" />)
		</c:when>
		<c:otherwise>
			(<spring:theme code="wishlist.page.totals.total.items.one" arguments="${searchPageData.pagination.totalNumberOfResults}" />)
		</c:otherwise>
	</c:choose>
	</div>
  <div class=" pull-right col-md-6 col-sm-8 col-xs-12">
    <form:form action="${addAllToCartURL}" class="addAllCart">
                       <button class="btn btn-default ">
				<spring:theme code="wishlist.page.add.all.to.cart.btn"    />
			</button>
    </form:form>
                        		<c:url value="/my-account/my-wishlist/removeall" var="removeallWishlistURL"/>
    <form:form action="${removeallWishlistURL}" class="removeAll" >
                       <button class="btn btn-primary">
				<spring:theme code="wishlist.page.aremove.all.btn"/>
			</button>
     </form:form>
     </div>
     
	</div>
</div>
<div class="row wishlist"><div class="col-md-12">
<ul class="item__list item__list__cart">
    <li class="hidden-xs hidden-sm">
        <ul class="item__list--header">
            <li class="item__image"></li>
            <li class="item__info"><spring:theme code='wishlist.page.items.header.info'/></li>
            <li class="item__price"><spring:theme code="wishlist.page.items.header.price"/></li>
			<li class="item__cart"></li>
            <li class="item__remove"></li>
        </ul>
    </li>
<c:forEach items="${searchPageData.results}" var="entry" varStatus="loop">
	<spring:url value="${entry.product.url}" var="productUrl" htmlEscape="false" />
	<li class="item__list--item">
		<%-- chevron for multi-d products --%>
		<div class="item__image">
			<ycommerce:testId code="test_searchPage_wholeProduct">
				<a href="${productUrl}"> <product:productPrimaryImage product="${entry.product}" format="thumbnail" />
				</a>
			</ycommerce:testId>
		</div> <%-- product name, code, availability --%>
		<div class="item__info">
			<ycommerce:testId code="searchPage_productName_link_${fn:escapeXml(entry.product.code)}">
				<a href="${productUrl}"><span class="item__name">${fn:escapeXml(entry.product.name)}</span></a>
			</ycommerce:testId>
			<div class="item__code">
				<c:set value="${entry.product.code}" var="productCode" />
				<c:set value="${entry.wishlistPK}" var="wishlistPK" />
				<c:out value="${fn:escapeXml(productCode)}" />
				<c:out value="${fn:escapeXml(wishlistPK)}" />
			</div>
			<%-- availability --%>
			<div class="item__stock">
				<ycommerce:testId code="searchPage_productName_link_${entry.product.code}">
					<c:set var="entryStock" value="${entry.product.stock.stockLevelStatus.code}" />
					<c:forEach items="${entry.product.baseOptions}" var="option">
						<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
							<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
							 <div>
                                    <strong>${fn:escapeXml(selectedOption.name)}:</strong>
                                    <span>${fn:escapeXml(selectedOption.value)}</span>
                                </div>
								<c:set var="entryStock" value="${option.selected.stock.stockLevelStatus.code}" />
							</c:forEach>
						</c:if>
					</c:forEach>
					<div>
						<c:choose>
							<c:when test="${not empty entryStock and entryStock eq 'inStock'}">
								<span class="stock instock"><i class="fas fa-check-circle"></i><spring:theme code="product.variants.in.stock" /></span>
							</c:when>
							<c:when test="${not empty entryStock and entryStock eq 'lowStock'}">
								<span class="stock lowstock"><spring:theme code="product.variants.low.stock" /></span>
							</c:when>
							<c:otherwise>
								<span class="out-of-stock"> <i class="fas fa-times-circle red"></i><spring:theme code="product.variants.out.of.stock" />
								</span>
							</c:otherwise>
						</c:choose>
					</div>
					<c:if test="${empty entryStock or entryStock eq 'outOfStock' and not empty entry.product.futureStocks}">
						<div>
							<span class="stockSpan"> <spring:theme code="text.account.wishlist.estimatedAvailability" /> <fmt:formatDate
									value="${entry.product.futureStocks[0].date}" pattern="yyyy/MM/dd" />
							</span>
						</div>
					</c:if>
				</ycommerce:testId>
			</div>
			<div class="item__price hidden-sm hidden-md hidden-lg">
			<c:out value="${entry.product.price.formattedValue}" />
		</div>
			
		</div> <%-- price --%>
		<div class="item__price hidden-xs">
			<c:out value="${entry.product.price.formattedValue}" />
		</div> <%-- notification --%> 
		
		
		<div class="item__cart">
			<product:addtocartcarousel product="${entry.product}" />
		</div>
	
		<div class="item__remove item_wishlist">
		
		<c:url value="/my-account/my-wishlist/remove/${fn:escapeXml(productCode)}" var="removeWishlistEntryURL"/>
		<form:form action="${removeWishlistEntryURL}" >
                       <button class="btn remove-intrersts-for-product remove-wishlist-entry-for-product" id="removeEntry_${loop.index}">
				<span class="fas fa-trash removeWishlistEntry2" data-productCode="${fn:escapeXml(productCode)}"
					data-pk="${fn:escapeXml(wishlistPK)}"></span>
			</button>
                    </form:form>
			
		</div>
	</li>
</c:forEach>
</ul>
<nav:wishlistPagination
							 top="false"
							 msgKey="text.account.wishlist.page"
							 showCurrentPageInfo="true"
							 hideRefineButton="true"
							 supportShowPaged="true"
							 supportShowAll="true"
							 searchPageData="${searchPageData}"
							 searchUrl="/my-account/my-wishlist"  numberPagesShown="5"/>
</div>
</div>
</c:when>
<c:otherwise>
<div class="search-empty text-center">
		<div class="headline">
			<img src="${fn:escapeXml(themeResourcePath)}/images/wishlist.png" class="noSearch">
			<h3><spring:theme code="wishlist.page.empty"/></h3>
			<p><spring:theme code="wishlist.page.empty.add"/></p>
		</div>
	</div>
</c:otherwise>
</c:choose>

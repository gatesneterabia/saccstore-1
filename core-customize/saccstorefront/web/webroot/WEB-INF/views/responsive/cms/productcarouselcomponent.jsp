<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:choose>
	<c:when test="${not empty productData}">
		<div class="carousel__component ${fn:toLowerCase(component.theme)}">
			<div class="carousel__component--headline">${fn:escapeXml(title)}</div>

			<c:choose>
				<c:when test="${component.popup}">
					<div class="carousel__component--carousel owl-carousel js-owl-carousel js-owl-${fn:toLowerCase(component.theme)} js-owl-carousel-reference">
						
						<c:forEach items="${productData}" var="product">

							<c:url value="${product.url}/quickView" var="productQuickViewUrl"/>
							<div class="carousel__item itemtheme">
								<a href="${productQuickViewUrl}" class="js-reference-item">
									<div class="carousel__item--thumb thumb">
										<product:productPrimaryReferenceImage product="${product}" format="zoom"/>
									</div>
									<div class="carousel__item--name name">
									<c:choose>
						<c:when test="${fn:length(product.name) > 45}">
							<c:out value="${fn:substring(product.name, 0, 45)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									
									</div>
									<div class="carousel__item--price">

<c:choose>
						<c:when test="${not empty product.discount}">
							
							<p class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></p>
						<span class="scratch"><format:fromPrice priceData="${product.discount.price}"/><span class="line_dis"></span></span>
						</c:when>
						<c:otherwise>
						
						<p class="price"><format:fromPrice priceData="${product.price}"/></p>
							<span class="scratch"></span>	
						</c:otherwise>
					</c:choose>

									
									</div>
								</a>
							</div>
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
					<div class="carousel__component--carousel js-owl-carousel owl-carousel owl-theme js-owl-${fn:toLowerCase(component.theme)}">
						<c:forEach items="${productData}" var="product">

							<c:url value="${product.url}" var="productUrl"/>

							<div class="${product.code} carousel__item itemtheme">
								<c:url value="javascript:;" var="link"></c:url>
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:url value="/login" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${product.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!product.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
        <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${isin}" data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${isout}" data-productcode="${product.code}" data-pk="8796093055677"><i class="far fa-heart"></i></a>
				</span>
									<div class="thumbimage">
									<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}"><i class="outstock"><spring:theme code='product.variants.out.of.stock'/></i></c:if>
									<a href="${productUrl}">
									<c:if test="${not empty product.productLabel}"><div class="carousel__item--label">${product.productLabel}</div></c:if>
										<product:productPrimaryImage product="${product}" format="zoom"/>
										
									</a>
									</div>
									<div class="content_box">
									<div class="carousel__item--price-top">
									
									<c:choose>
						<c:when test="${not empty product.discount}">
							
							<p class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></p>
						<span class="scratch"><format:fromPrice priceData="${product.discount.price}"/><span class="line_dis"></span></span>
						</c:when>
						<c:otherwise>
						
						<p class="price"><format:fromPrice priceData="${product.price}"/></p>
							<span class="scratch"></span>	
						</c:otherwise>
					</c:choose>
									</div>
								
									<div class="carousel__item--name name">
									<a href="${productUrl}">
									<c:choose>
						<c:when test="${fn:length(product.name) > 40}">
							<c:out value="${fn:substring(product.name, 0, 40)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									</a>
									</div>
									<div class="carousel__item--reviews reviews">
										<product:productReviewSummary product="${product}" showLinks="false"/>
									</div>
									
									
									
							
									<div class="carousel__item--price-bottom">
									
									<c:choose>
						<c:when test="${not empty product.discount}">
							
							<p class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></p>
						<span class="scratch"><format:fromPrice priceData="${product.discount.price}"/><span class="line_dis"></span></span>
						</c:when>
						<c:otherwise>
						
						<p class="price"><format:fromPrice priceData="${product.price}"/></p>
							<span class="scratch"></span>	
						</c:otherwise>
					</c:choose>
									</div>
									
									
									<c:if test="${not empty product.stock.stockLevel}">
									<div class="stock hidden">
										<spring:theme code="product.stocklevel" arguments="${product.stock.stockLevel}"/>
									</div>
									</c:if>
									
									<div class="addcart">
										<product:addProductToCart showQuantityBox="true" product="${product}" />
										</div>
									
									</div>
							
						
							</div>
							
						</c:forEach>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>


<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>



			<c:forEach items="${banners}" var="banner" varStatus="status">
			<div class=" box-${fn:toLowerCase(component.theme)} septheme ${fn:toLowerCase(component.theme) eq 'theme2' && cmsPage.uid eq 'homepage' ? 'no-space col-md-2 col-sm-4 col-xs-4' : ''}">
			
			
			
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
				
						<a class="${fn:toLowerCase(banner.theme)}" tabindex="-1" href="${fn:escapeXml(encodedUrl)}"<c:if test="${banner.external}"> target="_blank"</c:if>>
							<img class="btn-block" src="${fn:escapeXml(banner.media.url)}" 
								alt="" 
								title=""/>
						<c:if test="${not empty banner.title}">
						<label class="devcontent">
						<c:if test="${not empty banner.title}"> <h2>${banner.title}</h2></c:if>
						<c:if test="${not empty banner.content}"> <span>${banner.content}</span></c:if>
						</label>
						</c:if>
					
			  
			  
						</a>
					
				</c:if>
			</div>
			</c:forEach>


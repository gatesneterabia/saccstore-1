<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
    <div class="row no-margin margin_top padd30 hidden-sm hidden-xs">
    	
    	<div class="col-md-12 col-lg-12 col-xs-12">
    		<cms:pageSlot position="FooterNavLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
    </div>
    <div class="row no-margin margin_top padd30 borderbottom">
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 contactstyle margint40 hidden-sm hidden-xs">
    	  <cms:pageSlot position="FooterContactUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
        
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 appstyle margint40 hidden">
    	 <cms:pageSlot position="FooterAppStoreLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 hidden margint40 socialicons text-center hidden-lg hidden-md">
        <cms:pageSlot position="FooterFollowUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
     </div>
    
    	<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 margint40">
    		<cms:pageSlot position="FooterNavLinks2" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
    </div>
    <div class="row no-margin padd30" >
   
    <div class="col-sm-6 col-md-3 pull-right hidden-sm hidden-xs logo text-center margint20">
    		 <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    	</div>
       <div class="col-sm-6 col-md-3 socialicons pull-right text-center margint20 hidden-sm hidden-xs">
        <cms:pageSlot position="FooterFollowUsLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
     </div>
      <div class="col-sm-12 col-md-3 pull-right paymenttext margint20">
    <cms:pageSlot position="FooterPaymentLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    <cms:pageSlot position="Footer" var="feature" element="div" class="logo hidden-md hidden-lg">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
  
    </div>
    <div class="col-sm-12 col-md-3 pull-left copyright margint20">
     <cms:pageSlot position="CopyrightFooter" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    </div>
    
    
    
   
</footer>


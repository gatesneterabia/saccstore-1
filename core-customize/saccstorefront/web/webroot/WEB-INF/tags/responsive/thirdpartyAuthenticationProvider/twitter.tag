<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ attribute name="provider" required="false"
	type="com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="/thirdpartyauthentication/twitter"
	var="getUserDataURL" />



<form action="${getUserDataURL }" method="get"  class="hidden">
<input type="submit" value="TwitterLogin" id="TwitterLogin"/>
</form>
<script>
	$( "#twitter_action" ).click(function() {
 $("#TwitterLogin").trigger('click');
});
	
	</script>
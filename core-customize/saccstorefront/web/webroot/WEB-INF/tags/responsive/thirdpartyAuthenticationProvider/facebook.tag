<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ attribute name="provider" required="false"
	type="com.sacc.saccthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="/thirdpartyauthentication/getuserdata"
	var="getUserDataURL" />
<script>
		window.fbAsyncInit = function() {
			FB.init({
			
				appId : 	${provider.id},
				cookie : false,
				xfbml : true,
				status: true,
				version : 'v8.0',

			});
			FB.AppEvents.logPageView();
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

		function checkLoginState() {
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}

		function statusChangeCallback(response) {

			console.log(response.authResponse.accessToken);
			if (response.status === 'connected') {				
				var redirectUrl = ACC.config.encodedContextPath + '/thirdpartyauthentication/getuserdata';
				
				 var form = $('<form action="' + redirectUrl + '" method="get">' +
		                 '<input type="text" name="data" value="' +
		                 response.authResponse.accessToken + '" />' +
		                  '<input type="text" name="type" value="FACEBOOK" />' +
		                                                       '</form>');
				$('body').append(form);
				form.submit();

				console.log("SUBMITED");
				
			} else {
				// The person is not logged into your app or we are unable to tell.
				document.getElementById('status').innerHTML = 'Please log '
						+ 'into this app.';
			}
			
		}
			
	</script>

<!-- <button onclick="checkLoginState()">
	Facebook login
</button> 
<fb:login-button scope="public_profile,email"
	onlogin="checkLoginState();">
</fb:login-button>

-->

<div scope="public_profile,email"  onlogin="checkLoginState();" class="fb-login-button" data-size="large" data-button-type="continue_with"  data-use-continue-as="false" data-width="50px"></div>


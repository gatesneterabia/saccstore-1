<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="supportedPaymentData" required="true" type="com.sacc.saccpayment.entry.PaymentRequestData" %>
 <script> 
     var wpwlOptions = {
//              paymentTarget: '_top',
         locale: "${currentLanguage.isocode}",
         onReady: function(){
             
             $(".wpwl-control-cardHolder").attr("placeholder", ACC.cardHolderName);
             $(".wpwl-label-cardHolder").text(ACC.cardHolderName);
             }
     }
 </script> 
        
<br/>
	<script src="${supportedPaymentData.scriptSrc}"> </script>
	<c:url value="/checkout/multi/summary/callback" var="hyperpayFormURL" />
	<form action="${hyperpayFormURL}" class="paymentWidgets" data-brands="${supportedPaymentData.paymentProviderData.brands}"></form>
	
	
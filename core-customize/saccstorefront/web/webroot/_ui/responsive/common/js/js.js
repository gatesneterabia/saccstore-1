$('body').on('click', function (e) {

 $('.bootstrap-select.form-control').removeClass('open');
 
});
$("#change-country option, #mcountry option, #addresscountry option").each(function(){
    $(this).addClass("option-with-flag");
    $(this).attr("data-content","<span class='inline-flag flag "+ $(this).val().toLowerCase()+"'></span><span class='text'>"+$(this).html()+"</span>");    
});
$('#change-country').selectpicker('refresh');
$('#mcountry').selectpicker('refresh');
$('#addresscountry').selectpicker('refresh');
$('select').selectpicker();

$('#date').datepicker({
    language: 'en',
    dateFormat: 'dd/mm/yyyy'
});


var ias = $.ias({
	  container:  ".product__listing.product__grid",
	  item:       ".product-item",
	  delay: 500,
	  pagination: ".pagination-bar.top .pagination",
	  next:       ".pagination-bar.top .pagination-next a"
	});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
	text: 'Load More',
	offset: '50',
	html: '<div class="ias-trigger ias-trigger-next" ><a class="btn btn-primary btn-block">{text}</a></div>'// override text when no pages left
})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
text: '',
}));

ias.on('rendered', function() {

	
	ACC.product.bindToAddToCartForm();
	ACC.product.bindToAddToCartStorePickUpForm();
	ACC.product.enableStorePickupButton();
	ACC.product.enableAddToCartButton();
	
//	if($('.stock-0.selected').length){
//		$('.stock-0.selected').each(function() {
//				var list = $(this).parents('.list');
//			$(list).find('.size_change').each(function(index) {
//			
//				if($(this).data('stock') > 0){
//					$(this).trigger('click');
//					return false;
//					
//					
//				}
//				
//			});
//
//		
//		});
//		}

});
$('.link_ar').click(function(){ 
    $('.lang-selector').val('ar');
    $(".lang-form").submit();
    
})
$('.link_en').click(function(){ 
	$('.lang-selector').val('en');
    $(".lang-form").submit();
 
    
})

//popup form_login in home_page
$(".registration .login_action").click(function(){
	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
	$(".login_box").fadeIn("300", function () {
		$('.login_box').removeClass("hidden");
		
	});
	
	$('.registration').addClass("click_border");
	
	});

$('.darkback').click(function(){
	$(".login_box").fadeOut("slow").fadeOut("slow", function () {
		$('.login_box').addClass("hidden");
		$('.darkback').addClass("hidden");
		
	});
	$('.registration').removeClass("click_border");
});
//popup personal_info in home_page in sm and xs 
$(".navigation--bottom .personal_info").click(function(){
	 $(".navigation--bottom .NAVcompONENT").toggle();
	
	
	});
	

// popup personal_info in home_pageNAVcompONENT
$(".navigation--top .personal_info").click(function(){
	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
	$(".navigation--top .NAVcompONENT").fadeIn("300", function () {
		$('.navigation--top .NAVcompONENT').removeClass("hidden");
		
	});
	
	
	
	});

$('.darkback').click(function(){
	$(".NAVcompONENT").fadeOut("slow").fadeOut("slow", function () {
		$('.NAVcompONENT').addClass("hidden");
		$('.darkback').addClass("hidden");
		
	});
	
});

$(".mobile-action .title").click(function(){
	$(".mobile-action .footer__nav--links").removeClass('activefooter');
	$(this).parent('.footer__nav--container').find('.footer__nav--links').toggleClass('activefooter');
	$(this).toggleClass('activefooterlink');
	});


//set palceholder  in register page


/*let label_fisrt_name= $('.first-name label').text().trim();
$('[id*="register.firstName"]').attr("placeholder", label_fisrt_name);

let label_last_name= $('.last-name label').text().trim();
$('[id*="register.lastName"]').attr("placeholder", label_last_name);

let label_mobile_name= $('.mobile_number label').text().trim();
$('[id*="register.mobileNumber"]').attr("placeholder", label_mobile_name);

let label_email_name= $('.email label').text().trim();
$('[id*="register.email"]').attr("placeholder", label_email_name);
*/
let label_pass= $('.pass label').text().trim();
$('[id*="password"]').attr("placeholder", label_pass);

let label_pass_con= $('.pass_con label').text().trim();
$('[id*="register.checkPwd"]').attr("placeholder", label_pass_con);



// set placeholder in login page
/*let label_email_login=$('.email_login label').text().trim();

$('[id*="j_username').attr("placeholder", label_email_login);*/



//set palceholder password in login_form in home_page
let label=$('.password-holder .control-label').text().trim();

$('[id*="j_password"]').attr("placeholder", label);


/*let label_email_loginpop=$('.user_name_popup label').text().trim();


$('[id*="j_username').attr("placeholder", label_email_loginpop);

*/




$(".js-offcanvas-links").sticky({topSpacing:0});

$("#fade-example .js-tabs-link").aniTabs();


//add checked in radio in slot time
$("input[name='selector_selector']").click(function(){
	 var data_code = $("input[name='selector_selector']:checked").data('code');
	    var data_start = $("input[name='selector_selector']:checked").data('start');
	    var data_end = $("input[name='selector_selector']:checked").data('end');
	    var data_day = $("input[name='selector_selector']:checked").data('day');
	    var data_date = $("input[name='selector_selector']:checked").data('date');
	    $("#periodCode").attr("value",data_code)
	     $("#start").attr("value",data_start)
	     $("#end").attr("value",data_end);
	    $("#day").attr("value",data_day);
	    $("#date").attr("value",data_date);
	   console.log(data_code,data_start,data_end,data_day,data_date)
});

//qty in cartpage

$('.plus').click ( function () {
	var form = $(this).closest('form');
	var productCode = form.find('input[name=quantity]').val();
	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
	var newCartQuantity =  parseFloat(initialCartQuantity) + 1;
	form.find('input[name=quantity]').val(newCartQuantity);

	  form.submit();
    });
$('.minus').click (function () { 
    	var form = $(this).closest('form');
    	var productCode = form.find('input[name=productCode]').val();
    	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
    	var newCartQuantity = parseFloat(initialCartQuantity) - 1;
    	form.find('input[name=quantity]').val(newCartQuantity);
    	
    	  form.submit();
    	  });





	
	


$('.nav__link--drill__down').click (function () { 

$(".navigation__overflow").animate({ scrollTop: 0 });});


$("input[name='storeCredit']").click(function(){
	 var checked_item = $("input[name='storeCredit']:checked").attr('id');
	$("#sctCode").val(checked_item)
	if(checked_item == "REDEEM_SPECIFIC_AMOUNT"){
		
	$(".storeCreditAmount").css("display","inline-block")	
	}
	else
	{
		
		$(".storeCreditAmount").css("display","none");
		
	}
	
});

/**
 *
 */
package com.sacc.saccstorefront.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author monzer
 *
 */
public interface ThirdPartyAutoLoginStrategy
{
	void login(String username, Object token, String provider, HttpServletRequest request, final HttpServletResponse response);
}

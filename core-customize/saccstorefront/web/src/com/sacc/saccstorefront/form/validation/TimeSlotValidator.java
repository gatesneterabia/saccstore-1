/**
 *
 */
package com.sacc.saccstorefront.form.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccstorefront.form.TimeSlotForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("timeSlotValidator")
public class TimeSlotValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return TimeSlotForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final TimeSlotForm form = (TimeSlotForm) object;
		final String periodCode = form.getPeriodCode();
		final String start = form.getStart();
		final String end = form.getEnd();
		final String day = form.getDay();
		final String date = form.getDate();

		if (StringUtils.isEmpty(periodCode))
		{
			errors.rejectValue("periodCode", "timeslot.periodCode.invalid");
		}
		if (StringUtils.isEmpty(start))
		{
			errors.rejectValue("start", "timeslot.start.invalid");
		}
		if (StringUtils.isEmpty(end))
		{
			errors.rejectValue("end", "timeslot.end.invalid");
		}
		if (StringUtils.isEmpty(day))
		{
			errors.rejectValue("day", "timeslot.day.invalid");
		}
		if (StringUtils.isEmpty(date))
		{
			errors.rejectValue("date", "timeslot.date.invalid");
		}
	}

}

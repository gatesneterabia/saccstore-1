/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.form.validation;



import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccstorefront.form.AddressForm;
import com.sacc.saccuser.service.MobilePhoneService;


/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("customAddressValidator")
public class AddressValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		validateStandardFields(addressForm, errors);
		validateCountrySpecificFields(addressForm, errors);

	}

	private void validateCity(final AddressForm addressForm, final Errors errors)
	{

	}

	private void validateArea(final AddressForm addressForm, final Errors errors)
	{

	}

	private boolean isAreaRequired()
	{
		final String siteID = cmsSiteService.getCurrentSite() == null ? "" : cmsSiteService.getCurrentSite().getUid();

		return configurationService.getConfiguration().getBoolean("website.address.area.required." + siteID);
	}

	protected void validateStandardFields(final AddressForm addressForm, final Errors errors)
	{
		validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getAddressName(), AddressField.ADDRESSNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getMobile(), AddressField.MOBILE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getMobileCountry(), AddressField.MOBILECOUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getDistrictName(), AddressField.DISTRICT_NAME, MAX_FIELD_LENGTH, errors);
		vaildateMobile(addressForm, errors);
	}

	protected void vaildateMobile(final AddressForm addressForm, final Errors errors)
	{
		if (addressForm.getMobileCountry() != null && !StringUtils.isEmpty(addressForm.getMobile()))
		{
			addressForm.setMobile(addressForm.getMobile());
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(addressForm.getMobileCountry(), addressForm.getMobile());

			if (normalizedPhoneNumber.isEmpty())
			{
				errors.rejectValue("mobile", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.countryMobileNumber.invalid");
			}
			else
			{
				addressForm.setMobile(normalizedPhoneNumber.get());
			}

		}
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null)
		{
			switch (CountryCode.lookup(isoCode))
			{
				case CHINA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case CANADA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case USA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case JAPAN:
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
					break;
				default:
					validateStringField(addressForm.getCityCode(), AddressField.CITY, MAX_FIELD_LENGTH, errors);
					final boolean isAreaRequired = isAreaRequired();
					if (isAreaRequired)
					{
						validateStringField(addressForm.getAreaCode(), AddressField.AREA, MAX_FIELD_LENGTH, errors);

					}
					break;
			}
		}
	}

	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateStringFieldLength(final String field, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(field) && StringUtils.length(field) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN(
						"townCity", "address.townCity.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
								"address.regionIso.invalid"), COUNTRY("countryIso", "address.country.invalid"), CITY("cityCode",
										"address.city.invalid"), AREA("areaCode", "address.area.invalid"), MOBILECOUNTRY("mobileCountry",
												"address.mobile.country.filed.error"), MOBILE("mobile",
														"address.mobile.filed.error"), DISTRICT_NAME("districtName",
																"address.districtName.invalid"), ADDRESSNAME("addressName",
																		"address.addressName.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}

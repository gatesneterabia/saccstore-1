package com.sacc.saccstorefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccstorefront.form.RegisterForm;
import com.sacc.saccuser.service.MobilePhoneService;



/**
 * Validates registration forms.
 *
 * @author mnasro
 *
 */
@Component("customRegistrationValidator")
public class RegistrationValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(RegistrationValidator.class);
	private static final String PASSWORD_REGEX = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";

	private static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final RegisterForm registerForm = (RegisterForm) object;
		final String titleCode = registerForm.getTitleCode();
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();
		final boolean termsCheck = registerForm.isTermsCheck();
		final boolean otpConsentCheck = registerForm.isOtpConsentCheck();
		final String mobileNumber = registerForm.getMobileNumber();
		final String mobileCountry = registerForm.getMobileCountry();


		if (StringUtils.isEmpty(mobileCountry))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(mobileNumber) || !validateMobileNumber(mobileNumber))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(mobileCountry))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				registerForm.setMobileNumber(normalizedPhoneNumber.get());

			}
			else
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			}
		}

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);
		validateTermsAndConditions(errors, termsCheck);
		validateOTPConsentCheck(errors, otpConsentCheck);
	}

	protected void validateOTPConsentCheck(final Errors errors, final boolean otpConsentCheck)
	{
		if (!otpConsentCheck)
		{
			errors.rejectValue("otpConsentCheck", "register.otpConsentCheck.not.accepted");
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 * @param checkPwd
	 */
	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}

	/**
	 *
	 * @param errors
	 * @param pwd
	 */
	protected void validatePassword(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd))
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
		else if (StringUtils.length(pwd) < 6 || StringUtils.length(pwd) > 255)
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
		else if (!isValidPassword(pwd))
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
	}

	private boolean isValidPassword(final String password)
	{
		final Matcher matcher = PASSWORD_PATTERN.matcher(password);
		return matcher.matches();
	}

	/**
	 *
	 * @param errors
	 * @param email
	 */
	protected void validateEmail(final Errors errors, final String email)
	{
		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}
		else
		{
			try
			{
				if (userFacade.getUserUID(email) != null)
				{
					errors.rejectValue("email", "registration.error.account.exists.title");
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.info("Customer is not existed this is valid where UID=[" + email + "]");

			}
		}

	}

	/**
	 *
	 * @param errors
	 * @param name
	 * @param propertyName
	 * @param property
	 */
	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, property);
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 *
	 * @param errors
	 * @param titleCode
	 */
	protected void validateTitleCode(final Errors errors, final String titleCode)
	{
		if (StringUtils.isEmpty(titleCode) | StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "register.title.invalid");
		}
	}

	/**
	 *
	 * @param number
	 * @return true or false
	 */
	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

	/**
	 *
	 * @param email
	 * @return true or false
	 */
	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = Pattern.compile(configurationService.getConfiguration().getString(WebConstants.EMAIL_REGEX))
				.matcher(email);
		return matcher.matches();
	}

	/**
	 *
	 * @param errors
	 * @param termsCheck
	 */
	protected void validateTermsAndConditions(final Errors errors, final boolean termsCheck)
	{
		if (!termsCheck)
		{
			errors.rejectValue("termsCheck", "register.terms.not.accepted");
		}
	}
}

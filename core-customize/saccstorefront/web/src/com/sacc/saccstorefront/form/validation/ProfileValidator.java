/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.form.validation;


import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccuser.service.MobilePhoneService;
import com.sacc.saccstorefront.form.UpdateProfileForm;


/**
 * Validator for profile forms.
 */
@Component("customProfileValidator")
public class ProfileValidator implements Validator
{
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;
	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdateProfileForm profileForm = (UpdateProfileForm) object;
		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();

		if (StringUtils.isNotEmpty(title) && StringUtils.length(title) > 255)
		{
			errors.rejectValue("titleCode", "profile.title.invalid");
		}

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}
		else if (StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		else if (StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
		final String mobileNumber = profileForm.getMobileNumber();
		final String mobileCountry = profileForm.getMobileCountry();


		if (StringUtils.isEmpty(mobileCountry))
		{
			errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(mobileNumber) || !validateMobileNumber(mobileNumber))
		{
			errors.rejectValue("mobileNumber", "profile.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(mobileCountry))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService.validateAndNormalizePhoneNumberByIsoCode(mobileCountry,
					mobileNumber);

			if (normalizedPhoneNumber.isPresent())
			{
				profileForm.setMobileNumber(normalizedPhoneNumber.get());

			}
			else
			{
				errors.rejectValue("mobileNumber", "profile.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
			}
		}

	}

	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}
}

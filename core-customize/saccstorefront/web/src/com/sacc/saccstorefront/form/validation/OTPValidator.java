package com.sacc.saccstorefront.form.validation;

import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sacc.saccotp.context.OTPContext;
import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccuser.service.MobilePhoneService;
import com.sacc.saccstorefront.form.OTPForm;


/**
 * The Class OTPValidator.
 *
 * @author mnasro
 */
@Component("otpValidator")
public class OTPValidator implements Validator
{

	/** The mobile phone service. */
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateStandardFields(otpForm, errors);
		validateOTPCode(otpForm, errors);
	}

	public void validateSend(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateStandardFields(otpForm, errors);
	}
	protected void validateOTPCode(final OTPForm otpForm, final Errors errors)
	{
		if (StringUtils.isEmpty(otpForm.getOtpCode()))
		{
			errors.rejectValue("otpCode", "otp.otpCode.invalid");
		}else
		{
			try
			{
				if (!otpContext.verifyCodeByCurrentSite(otpForm.getMobileCountry(), otpForm.getMobileNumber(), otpForm.getOtpCode()))
				{
					errors.rejectValue("otpCode", "otp.otpCode.format.invalid");
				}

			}
			catch (final OTPException e)
			{
				errors.rejectValue("otpCode", "otp.otpCode.format.invalid");
			}
		}

	}




	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return OTPForm.class.equals(aClass);
	}

	/**
	 * Validate standard fields.
	 *
	 * @param otpForm
	 *           the otp form
	 * @param errors
	 *           the errors
	 */
	protected void validateStandardFields(final OTPForm otpForm, final Errors errors)
	{

		if (StringUtils.isEmpty(otpForm.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "otp.mobileCountry.format.invalid");
		}

		if (StringUtils.isEmpty(otpForm.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
		}

		try
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(otpForm.getMobileCountry(), otpForm.getMobileNumber());
			if (normalizedPhoneNumber.isPresent())
			{
				otpForm.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
			}
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("mobileNumber", "otp.mobileNumber.format.invalid");
		}
	}


}




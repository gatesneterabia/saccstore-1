/**
 *
 */
package com.sacc.saccstorefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sacc.saccstorefront.controllers.ControllerConstants;
import com.sacc.saccuserfacades.area.facade.AreaFacade;
import com.sacc.saccuserfacades.city.facade.CityFacade;
import com.sacc.saccwishlistfacade.data.MetaData;
import com.sacc.saccwishlistfacade.data.ResponseData;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Controller
@RequestMapping("/misc")
public class MiscController extends AbstractController
{
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/**
	 * Gets the cites.
	 *
	 * @param response
	 *           the response
	 * @param code
	 *           the code
	 * @return the cites
	 */
	@RequestMapping(value = "/country/{code}/cites", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCites(final HttpServletResponse response, @PathVariable
	final String code)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(cityFacade.getByCountryIsocode(code).get());
			meta.setMessage("getting cites by isoCode of country");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get cites by isoCode of country");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	/**
	 * Gets the areas.
	 *
	 * @param response
	 *           the response
	 * @param cityCode
	 *           the city code
	 * @return the areas
	 */
	@RequestMapping(value = "/city/{cityCode}/areas", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getAreas(final HttpServletResponse response, @PathVariable
	final String cityCode)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(areaFacade.getByCityCode(cityCode).get());
			meta.setMessage("getting areas by city code ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get areas by city code");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	/**
	 * Gets the country.
	 *
	 * @param response
	 *           the response
	 * @param code
	 *           the code
	 * @return the country
	 */
	@RequestMapping(value = "/country/{code}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData getCountry(final HttpServletResponse response, @PathVariable
	final String code)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();

		try
		{
			data.setData(countryConverter.convert(commonI18NService.getCountry(code)));
			meta.setMessage("getting country by isoCode ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);
		}
		catch (final IllegalArgumentException e)
		{
			data.setData(null);
			meta.setMessage("failed to get country by isoCode");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}

		return data;
	}

	@RequestMapping(value = "/plp/view/set", method = RequestMethod.POST)
	@ResponseBody
	public ResponseData setPlpView(final HttpServletResponse response, @NotNull
	@RequestParam(required = true, defaultValue = "grid", name = "viewType")
	final String viewType)
	{
		final ResponseData data = new ResponseData();
		final MetaData meta = new MetaData();
		try
		{
			sessionService.setAttribute(ControllerConstants.Actions.SessionPlpView.plpViewKey, viewType);
			data.setData(viewType);
			meta.setMessage("setting plp view  ");
			meta.setStatusCode(HttpStatus.ACCEPTED.value());
			data.setMeta(meta);



		}
		catch (final Exception e)
		{
			data.setData(null);
			meta.setMessage("failed to set Plp View");
			meta.setStatusCode(HttpStatus.BAD_REQUEST.value());
			data.setMeta(meta);
		}
		return data;
	}


}

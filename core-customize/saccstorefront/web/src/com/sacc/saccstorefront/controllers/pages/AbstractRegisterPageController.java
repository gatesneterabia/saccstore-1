package com.sacc.saccstorefront.controllers.pages;

import static de.hybris.platform.commercefacades.constants.CommerceFacadesConstants.CONSENT_GIVEN;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ConsentForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.strategy.CustomerConsentDataStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.commercefacades.consent.data.AnonymousConsentData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.ConsentData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sacc.saccotp.context.OTPContext;
import com.sacc.saccotp.entity.SessionData;
import com.sacc.saccotp.exception.OTPException;
import com.sacc.saccstorefront.controllers.ControllerConstants;
import com.sacc.saccstorefront.form.RegisterForm;
import com.sacc.saccuserfacades.country.facade.CountryFacade;


public abstract class AbstractRegisterPageController extends AbstractPageController
{
	protected static final String MESSAGE_NOT_SENT_MESSAGE = "otp.message.not.sent.message";
	protected static final String PHONENUMBER_VERIFICATION_PAGE_LABEL = "/verify";
	protected static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";

	private static final Logger LOGGER = Logger.getLogger(AbstractRegisterPageController.class);

	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	private static final String CONSENT_FORM_GLOBAL_ERROR = "consent.form.global.error";

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "customRegistrationValidator")
	private Validator registrationValidator;

	@Resource(name = "consentFacade")
	protected ConsentFacade consentFacade;

	@Resource(name = "customerConsentDataStrategy")
	protected CustomerConsentDataStrategy customerConsentDataStrategy;

	@Resource(name = "otpContext")
	protected OTPContext otpContext;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;


	protected CountryFacade getCountryFacade()
	{
		return countryFacade;
	}

	protected OTPContext getOtpContext()
	{
		return otpContext;
	}

	protected abstract AbstractPageModel getCmsPage() throws CMSItemNotFoundException;

	protected abstract String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response);

	protected abstract String getView();



	/**
	 * @return the registrationValidator
	 */
	protected Validator getRegistrationValidator()
	{
		return registrationValidator;
	}

	/**
	 * @return the autoLoginStrategy
	 */
	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	/**
	 *
	 * @return GUIDCookieStrategy
	 */
	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	protected String getDefaultRegistrationPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage("header.link.login", null, getI18nService().getCurrentLocale()), null);
		model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));

		model.addAttribute(getNewRegisterForm());
		return getView();
	}

	protected RegisterForm getNewRegisterForm()
	{
		final RegisterForm registerForm = new RegisterForm();
		registerForm.setMobileCountry(getCmsSiteService().getCurrentSite().getDefaultMobileCountry() == null ? null
				: getCmsSiteService().getCurrentSite().getDefaultMobileCountry().getIsocode());
		return registerForm;
	}

	/**
	 * This method takes data from the registration form and create a new customer account and attempts to log in using
	 * the credentials of this new user.
	 *
	 * @return true if there are no binding errors or the account does not already exists.
	 * @throws CMSItemNotFoundException
	 */
	protected String processRegisterUserRequest(final String referer, final RegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException // NOSONAR
	{
		if (bindingResult.hasErrors())
		{
			form.setTermsCheck(false);
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}



		final RegisterData data = getRegisterData(form);
		if (getOtpContext().isEnabledByCurrentSite())
		{
			try
			{
				getOtpContext().sendOTPCodeByCurrentSiteAndSessionData(data.getMobileCountry(), data.getMobileNumber(),
						new SessionData(ControllerConstants.Actions.RegistrationOTP.SessionSaveDataKey, data));
				return REDIRECT_PREFIX + PHONENUMBER_VERIFICATION_PAGE_LABEL;
			}
			catch (final OTPException ex)
			{
				switch (ex.geType())
				{
					case CMS_SITE_NOT_FOUND:
					case DISABLED:
					case OTP_CONFIG_UNAVAILABLE:
					case OTP_TYPE_NOTE_FOUND:
					case SERVICE_UNAVAILABLE:
						GlobalMessages.addErrorMessage(model, MESSAGE_NOT_SENT_MESSAGE);
				}

				form.setTermsCheck(false);
				model.addAttribute(form);
				model.addAttribute(new LoginForm());
				model.addAttribute(new GuestForm());
				return handleRegistrationError(model);
			}
		}

		return registrationCustomer(model, redirectModel, data, request, response, bindingResult);
	}

	private ConsentForm getConsentForm(final ConsentData data)
	{
		if (data == null)
		{
			return null;
		}
		final ConsentForm form = new ConsentForm();
		form.setConsentCode(data.getConsentCode());
		form.setConsentGiven(data.getConsentGiven());
		form.setConsentTemplateVersion(data.getConsentTemplateVersion());
		form.setConsentTemplateId(data.getConsentTemplateId());
		return form;

	}

	private ConsentData getConsentData(final ConsentForm form)
	{
		if (form == null)
		{
			return null;
		}
		final ConsentData data = new ConsentData();
		data.setConsentCode(form.getConsentCode());
		data.setConsentGiven(form.getConsentGiven());
		data.setConsentTemplateVersion(form.getConsentTemplateVersion());
		data.setConsentTemplateId(form.getConsentTemplateId());
		return data;

	}

	/**
	 * @param form
	 * @return
	 */
	private RegisterData getRegisterData(final RegisterForm form)
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(form.getFirstName());
		data.setLastName(form.getLastName());
		data.setLogin(form.getEmail());
		data.setPassword(form.getPwd());
		data.setTitleCode(form.getTitleCode());
		data.setMobileNumber(form.getMobileNumber());
		data.setMobileCountry(form.getMobileCountry());
		data.setConsentData(getConsentData(form.getConsentForm()));
		return data;
	}

	private RegisterForm getRegisterForm(final RegisterData data)
	{
		final RegisterForm form = getNewRegisterForm();
		form.setFirstName(data.getFirstName());
		form.setLastName(data.getLastName());
		form.setEmail(data.getLogin());
		form.setPwd(data.getPassword());
		form.setTitleCode(data.getTitleCode());
		form.setMobileNumber(data.getMobileNumber());
		form.setMobileCountry(data.getMobileCountry());
		form.setConsentForm(getConsentForm(data.getConsentData()));
		form.setTermsCheck(false);

		return form;
	}

	/**
	 * Anonymous checkout process.
	 *
	 * Creates a new guest customer and updates the session cart with this user. The session user will be anonymous and
	 * it's never updated with this guest user.
	 *
	 * If email is required, grab the email from the form and set it as uid with "guid|email" format.
	 *
	 * @throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	 */
	protected String processAnonymousCheckoutUserRequest(final GuestForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		try
		{
			if (bindingResult.hasErrors())
			{
				model.addAttribute(form);
				model.addAttribute(new LoginForm());
				model.addAttribute(getNewRegisterForm());
				GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
				return handleRegistrationError(model);
			}

			getCustomerFacade().createGuestUserForAnonymousCheckout(form.getEmail(),
					getMessageSource().getMessage("text.guest.customer", null, getI18nService().getCurrentLocale()));
			getGuidCookieStrategy().setCookie(request, response);
			getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.debug("guest registration failed.");
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

	protected String handleRegistrationError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		addRegistrationConsentDataToModel(model);
		return getView();
	}

	@ModelAttribute("mobileCountries")
	public Collection<CountryData> getMobileCountries()
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		return mobileCountries.isPresent() ? mobileCountries.get() : null;
	}

	protected String registrationCustomer(final Model model, final RedirectAttributes redirectModel, final RegisterData data,
			final HttpServletRequest request, final HttpServletResponse response, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		try
		{
			getCustomerFacade().register(data);
			getAutoLoginStrategy().login(data.getLogin().toLowerCase(), data.getPassword(), request, response);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"registration.confirmation.message.title");

		}
		catch (final DuplicateUidException e)
		{
			LOGGER.debug("registration failed.");
			model.addAttribute(getRegisterForm(data));
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}

		try
		{
			final ConsentForm consentForm = getConsentForm(data.getConsentData());
			if (consentForm != null && consentForm.getConsentGiven())
			{
				getConsentFacade().giveConsent(consentForm.getConsentTemplateId(), consentForm.getConsentTemplateVersion());
			}
		}
		catch (final Exception e)
		{
			LOGGER.error("Error occurred while creating consents during registration", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, CONSENT_FORM_GLOBAL_ERROR);
		}

		// save anonymous-consent cookies as ConsentData
		final Cookie cookie = WebUtils.getCookie(request, WebConstants.ANONYMOUS_CONSENT_COOKIE);
		if (cookie != null)
		{
			try
			{
				final ObjectMapper mapper = new ObjectMapper();
				final List<AnonymousConsentData> anonymousConsentDataList = Arrays.asList(mapper.readValue(
						URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8.displayName()), AnonymousConsentData[].class));
				anonymousConsentDataList.stream().filter(consentData -> CONSENT_GIVEN.equals(consentData.getConsentState()))
						.forEach(consentData -> consentFacade.giveConsent(consentData.getTemplateCode(),
								Integer.valueOf(consentData.getTemplateVersion())));
			}
			catch (final UnsupportedEncodingException e)
			{
				LOGGER.error(String.format("Cookie Data could not be decoded : %s", cookie.getValue()), e);
			}
			catch (final IOException e)
			{
				LOGGER.error("Cookie Data could not be mapped into the Object", e);
			}
			catch (final Exception e)
			{
				LOGGER.error("Error occurred while creating Anonymous cookie consents", e);
			}
		}

		customerConsentDataStrategy.populateCustomerConsentDataInSession();

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}


}

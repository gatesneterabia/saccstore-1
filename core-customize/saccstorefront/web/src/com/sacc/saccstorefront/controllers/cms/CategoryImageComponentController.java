/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccstorefront.controllers.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sacc.sacccomponents.model.CategoryImageComponentModel;
import com.sacc.saccstorefront.controllers.ControllerConstants;




/**
 * @author amjad.shati@erabia.com
 *
 *         Controller for CMS CategoryImageComponent
 */
@Controller("CategoryImageComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CategoryImageComponent)
public class CategoryImageComponentController extends AbstractAcceleratorCMSComponentController<CategoryImageComponentModel>
{
	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CategoryImageComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			final CategoryModel currentCategory = getRequestContextData(request).getCategory();
			if (currentCategory != null)
			{
				final List<MediaModel> medias = new ArrayList<>();
				if (currentCategory.getPlpPicture() != null)
				{
					final ImageData wideImage = imageConverter.convert(currentCategory.getPlpPicture());
					model.addAttribute("wideImage", wideImage);
				}
				if (currentCategory.getPlpPictureResponsive() != null)
				{
					final ImageData responsiveImage = imageConverter.convert(currentCategory.getPlpPictureResponsive());
					model.addAttribute("responsiveImage", responsiveImage);
				}
				model.addAttribute("categoryContent", currentCategory.getName());
			}
		}
	}
}

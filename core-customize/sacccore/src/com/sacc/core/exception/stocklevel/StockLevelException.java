/**
 *
 */
package com.sacc.core.exception.stocklevel;

import com.sacc.core.exception.stocklevel.type.StockLevelExceptionType;


/**
 * @author monzer
 *
 */
public class StockLevelException extends Exception
{

	private final StockLevelExceptionType type;
	private final String message;

	/**
	 * @param type
	 * @param message
	 */
	public StockLevelException(final StockLevelExceptionType type, final String message)
	{
		super(message);
		this.type = type;
		this.message = message;
	}

	/**
	 * @return the type
	 */
	public StockLevelExceptionType getType()
	{
		return type;
	}
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.core.service;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public interface BarcodeGenaratorService
{
	/**
	 * @param value,
	 *           String you want to display under the barcode.
	 * @return ByteArrayOutputStream representing the image.
	 */
	public ByteArrayOutputStream genarateCode128Barcode(String value) throws IOException;

	/**
	 * Method creates MediaModel object for storing barcode.
	 *
	 * @param consignment
	 *           - to use trackingID and display it under the barcode.
	 * @return created MediaModel object
	 */
	public MediaModel generateBarcodeAsMedia(ConsignmentModel consignment) throws IOException;
}

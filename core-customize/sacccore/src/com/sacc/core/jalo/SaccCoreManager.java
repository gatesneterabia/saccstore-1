/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.sacc.core.constants.SaccCoreConstants;
import com.sacc.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class SaccCoreManager extends GeneratedSaccCoreManager
{
	public static final SaccCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SaccCoreManager) em.getExtension(SaccCoreConstants.EXTENSIONNAME);
	}
}

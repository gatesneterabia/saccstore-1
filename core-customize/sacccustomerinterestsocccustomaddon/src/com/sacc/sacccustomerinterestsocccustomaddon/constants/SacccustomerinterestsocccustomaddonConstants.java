/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomerinterestsocccustomaddon.constants;

/**
 * Global class for all sacccustomerinterestsocccustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class SacccustomerinterestsocccustomaddonConstants extends GeneratedSacccustomerinterestsocccustomaddonConstants
{
	public static final String EXTENSIONNAME = "sacccustomerinterestsocccustomaddon"; //NOSONAR

	private SacccustomerinterestsocccustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}

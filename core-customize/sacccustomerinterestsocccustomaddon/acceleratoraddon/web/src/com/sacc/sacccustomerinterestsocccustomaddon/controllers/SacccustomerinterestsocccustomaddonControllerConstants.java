/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacccustomerinterestsocccustomaddon.controllers;

/**
 */
public interface SacccustomerinterestsocccustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}

/**
 *
 */
package com.sacc.saccstorecredit.exception.enums;

/**
 * @author yhammad
 *
 */
public enum StoreCreditExceptionType
{
	NOT_AVAILABLE, EMPTY;
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorderfacades.facade.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.sacc.saccorder.model.OrderNoteModel;
import com.sacc.saccorder.service.OrderNoteService;
import com.sacc.saccorderfacades.data.OrderNoteData;
import com.sacc.saccorderfacades.facade.OrderNoteFacade;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderNoteFacade implements OrderNoteFacade
{
	@Resource(name = "orderNoteService")
	private OrderNoteService orderNoteService;

	@Resource(name = "orderNoteConverter")
	private Converter<OrderNoteModel, OrderNoteData> orderNoteConverter;

	@Override
	public List<OrderNoteData> getOrderNotesByCurrentSite()
	{
		final List<OrderNoteModel> orderNotes = orderNoteService.getOrderNotesByCurrentSite();
		if (CollectionUtils.isEmpty(orderNotes))
		{
			return Collections.emptyList();
		}
		return orderNoteConverter.convertAll(orderNotes);
	}

}

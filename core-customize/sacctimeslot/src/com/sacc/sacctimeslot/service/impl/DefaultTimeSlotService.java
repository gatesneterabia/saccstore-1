/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.sacctimeslot.service.impl;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.sacc.sacctimeslot.dao.TimeSlotDao;
import com.sacc.sacctimeslot.model.TimeSlotInfoModel;
import com.sacc.sacctimeslot.model.TimeSlotModel;
import com.sacc.sacctimeslot.service.TimeSlotService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultTimeSlotService implements TimeSlotService
{
	private static final String ZONE_DELIVERY_MODE_ILLEGAL_ARGS = "zoneDeliveryModeCode must not be null or empty.";

	@Resource(name = "deliveryService")
	private DeliveryService deliveryService;

	@Resource(name = "timeSlotDao")
	private TimeSlotDao timeSlotDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public Optional<TimeSlotModel> getActive(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);
		final ZoneDeliveryModeModel zoneDeliveryModeModel = (ZoneDeliveryModeModel) deliveryService
				.getDeliveryModeForCode(zoneDeliveryModeCode);
		if (zoneDeliveryModeModel != null)
		{
			return Optional.ofNullable(zoneDeliveryModeModel.getTimeSlot());
		}
		return Optional.ofNullable(null);
	}

	@Override
	public boolean isTimeSlotEnabled(final String zoneDeliveryModeCode)
	{
		Preconditions.checkArgument(!StringUtils.isEmpty(zoneDeliveryModeCode), ZONE_DELIVERY_MODE_ILLEGAL_ARGS);
		final ZoneDeliveryModeModel deliveryMode = (ZoneDeliveryModeModel) deliveryService
				.getDeliveryModeForCode(zoneDeliveryModeCode);
		return deliveryMode != null && deliveryMode.getTimeSlot() != null && deliveryMode.getTimeSlot().isActive();
	}

	@Override
	public int getNumberOfOrdersByTimeSlot(final BaseStoreModel store, final String start, final String end, final String date,
			final String timezone)
	{
		return timeSlotDao.get(store, start, end, date, timezone).size();
	}

	@Override
	public List<OrderModel> getOrdersByInterval(final BaseStoreModel store, final String timezone, final long interval)
	{
		return timeSlotDao.get(store, timezone, interval);
	}

	@Override
	public void saveTimeSlotInfo(final TimeSlotInfoModel info, final CartModel cart, final String date, final LocalTime start)
	{
		final Optional<TimeSlotModel> timeSlot = getActive(cart.getDeliveryMode().getCode());
		final String timezone = timeSlot.get().getTimezone();
		info.setTimezone(timezone);

		final ZonedDateTime startDateTime = ZonedDateTime.of(LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				start, ZoneId.of(timezone));

		info.setStartDate(Date.from(startDateTime.toInstant()));
		info.setStartHour(startDateTime.getHour());
		info.setStartMinute(startDateTime.getMinute());

		modelService.save(info);
		modelService.refresh(info);
		cart.setTimeSlotInfo(info);
		modelService.save(cart);
	}
}

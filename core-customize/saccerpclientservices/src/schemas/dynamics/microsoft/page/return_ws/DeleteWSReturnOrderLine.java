
package schemas.dynamics.microsoft.page.return_ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WS_Return_Order_Line_Key" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wsReturnOrderLineKey"
})
@XmlRootElement(name = "Delete_WS_Return_Order_Line")
public class DeleteWSReturnOrderLine {

    @XmlElement(name = "WS_Return_Order_Line_Key", required = true)
    protected String wsReturnOrderLineKey;

    /**
     * Gets the value of the wsReturnOrderLineKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWSReturnOrderLineKey() {
        return wsReturnOrderLineKey;
    }

    /**
     * Sets the value of the wsReturnOrderLineKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWSReturnOrderLineKey(String value) {
        this.wsReturnOrderLineKey = value;
    }

}

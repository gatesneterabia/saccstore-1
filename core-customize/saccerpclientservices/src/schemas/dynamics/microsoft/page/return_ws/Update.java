
package schemas.dynamics.microsoft.page.return_ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Return_WS" type="{urn:microsoft-dynamics-schemas/page/return_ws}Return_WS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnWS"
})
@XmlRootElement(name = "Update")
public class Update {

    @XmlElement(name = "Return_WS", required = true)
    protected ReturnWS returnWS;

    /**
     * Gets the value of the returnWS property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnWS }
     *     
     */
    public ReturnWS getReturnWS() {
        return returnWS;
    }

    /**
     * Sets the value of the returnWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnWS }
     *     
     */
    public void setReturnWS(ReturnWS value) {
        this.returnWS = value;
    }

}


package schemas.dynamics.microsoft.page.return_ws;

import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sun.xml.ws.client.BindingProviderProperties;

import schemas.dynamics.microsoft.page.AbstractSaccERPWebService;


/**
 * <p>Java class for Return_WS complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Return_WS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="External_Document_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sell_to_Customer_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Order_Date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="WS_Return_Order_Line" type="{urn:microsoft-dynamics-schemas/page/return_ws}WS_Return_Order_Line_List" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

/**
 *
 * @author Baha Almtoor
 *
 */
@XmlRootElement(name = "ReturnWS")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReturnWebService", propOrder =
{ "key", "no", "externalDocumentNo", "sellToCustomerNo", "orderDate", "wsReturnOrderLine" })
public class ReturnWS extends AbstractSaccERPWebService
{
	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "No")
	protected String no = "";
	@XmlElement(name = "External_Document_No")
	protected String externalDocumentNo = "";
	@XmlElement(name = "Sell_to_Customer_No")
	protected String sellToCustomerNo = "";
	@XmlElement(name = "Order_Date")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar orderDate;
	@XmlElement(name = "WS_Return_Order_Line")
	protected WSReturnOrderLineList wsReturnOrderLine;

	private void setTimeoutValue(final ReturnWSPort returnWSPort)
	{
		final BindingProvider provider = (BindingProvider) returnWSPort;
		final Map<String, Object> requestContext = provider.getRequestContext();
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getTimeout());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getTimeout());
	}

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the no property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getNo()
	{
		return no;
	}

	/**
	 * Sets the value of the no property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setNo(final String value)
	{
		this.no = value;
	}

	/**
	 * Gets the value of the externalDocumentNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getExternalDocumentNo()
	{
		return externalDocumentNo;
	}

	/**
	 * Sets the value of the externalDocumentNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setExternalDocumentNo(final String value)
	{
		this.externalDocumentNo = value;
	}

	/**
	 * Gets the value of the sellToCustomerNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getSellToCustomerNo()
	{
		return sellToCustomerNo;
	}

	/**
	 * Sets the value of the sellToCustomerNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setSellToCustomerNo(final String value)
	{
		this.sellToCustomerNo = value;
	}

	/**
	 * Gets the value of the orderDate property.
	 *
	 * @return possible object is {@link XMLGregorianCalendar }
	 *
	 */
	public XMLGregorianCalendar getOrderDate()
	{
		return orderDate;
	}

	/**
	 * Sets the value of the orderDate property.
	 *
	 * @param value
	 *           allowed object is {@link XMLGregorianCalendar }
	 *
	 */
	public void setOrderDate(final XMLGregorianCalendar value)
	{
		this.orderDate = value;
	}

	public void setOrderDate(final Date value)
	{
		try
		{
			this.orderDate = convertDateToXMLGregorianCalendar(value);
		}
		catch (final SaccERPWebServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) throws SaccERPWebServiceException
	{

		XMLGregorianCalendar xmlGregorianCalendar = null;
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		try
		{
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
			xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		}
		catch (final DatatypeConfigurationException ex)
		{
			ex.printStackTrace();
		}

		return xmlGregorianCalendar;
	}

	/**
	 * Gets the value of the wsReturnOrderLine property.
	 *
	 * @return possible object is {@link WSReturnOrderLineList }
	 *
	 */
	public WSReturnOrderLineList getWSReturnOrderLine()
	{
		return wsReturnOrderLine;
	}

	/**
	 * Sets the value of the wsReturnOrderLine property.
	 *
	 * @param value
	 *           allowed object is {@link WSReturnOrderLineList }
	 *
	 */
	public void setWSReturnOrderLine(final WSReturnOrderLineList value)
	{
		this.wsReturnOrderLine = value;
	}

	protected ReturnWSPort getReturnWSPort()
	{
		return new ReturnWSService(isTestEnv()).getReturnWSPort();
	}

	public List<ReturnWS> readMultiple(final List<ReturnWSFilter> filters, final String bookmarkKey, final int size)
			throws SaccERPWebServiceException
	{
		try
		{
			final ReturnWSPort returnWSPort = getReturnWSPort();
			setAuthenticator();
			setTimeoutValue(returnWSPort);
			final ReturnWSList returnWebServiceList = returnWSPort.readMultiple(filters, bookmarkKey, size);
			if (returnWebServiceList != null)
			{
				return returnWebServiceList.getReturnWS();
			}

			return Collections.emptyList();
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't readMultiple[ReturnWS]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public ReturnWS read() throws SaccERPWebServiceException
	{
		try
		{
			final ReturnWSPort returnWSPort = getReturnWSPort();
			setAuthenticator();
			setTimeoutValue(returnWSPort);
			return returnWSPort.read(getNo());
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't read[ReturnWS]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public ReturnWS update() throws SaccERPWebServiceException
	{
		try
		{
			final ReturnWSPort returnWSPort = getReturnWSPort();
			setAuthenticator();
			setTimeoutValue(returnWSPort);
			final Holder<ReturnWS> returnWebServiceHolder = new Holder<ReturnWS>(this);
			returnWSPort.update(returnWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't update[ReturnWS]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public ReturnWS create() throws SaccERPWebServiceException
	{
		try
		{
			final ReturnWSPort returnWSPort = getReturnWSPort();
			setAuthenticator();
			setTimeoutValue(returnWSPort);
			final Holder<ReturnWS> returnWebServiceHolder = new Holder<ReturnWS>(this);
			returnWSPort.create(returnWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't create[ReturnWS]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public String toString()
	{
		return "ReturnWS [key=" + key + ", no=" + no + ", externalDocumentNo=" + externalDocumentNo + ", sellToCustomerNo="
				+ sellToCustomerNo + ", orderDate=" + orderDate + ", wsReturnOrderLine=" + wsReturnOrderLine + "]";
	}


}

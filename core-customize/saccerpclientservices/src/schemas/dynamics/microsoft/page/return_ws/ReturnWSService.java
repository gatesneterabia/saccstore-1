
package schemas.dynamics.microsoft.page.return_ws;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import org.apache.log4j.Logger;


/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.9-b130926.1035 Generated source version: 2.2
 *
 */
@WebServiceClient(name = "Return_WS_Service", targetNamespace = "urn:microsoft-dynamics-schemas/page/return_ws")
public class ReturnWSService extends Service
{
	private static final Logger LOG = Logger.getLogger(ReturnWSService.class);

	private static URL RETURNWEBSERVICESERVICEWSDLLOCATION;
	private static WebServiceException RETURNWEBSERVICESERVICEEXCEPTION;
	private final static QName RETURNWSSERVICE_QNAME = new QName("urn:microsoft-dynamics-schemas/page/return_ws",
			"Return_WS_Service");

	private final static String LIVE_WSDL_FILE_LOCATION = "/wsdls/live/Return_WS.wsdl";
	private final static String TEST_WSDL_FILE_LOCATION = "/wsdls/test/Return_WS.wsdl";

	public ReturnWSService(final boolean flag)
	{
		super(__getWsdlLocation(flag), RETURNWSSERVICE_QNAME);
	}

	public ReturnWSService(final boolean flag, final WebServiceFeature... features)
	{
		super(__getWsdlLocation(flag), RETURNWSSERVICE_QNAME, features);
	}

	public ReturnWSService(final URL wsdlLocation)
	{
		super(wsdlLocation, RETURNWSSERVICE_QNAME);
	}

	public ReturnWSService(final URL wsdlLocation, final WebServiceFeature... features)
	{
		super(wsdlLocation, RETURNWSSERVICE_QNAME, features);
	}

	public ReturnWSService(final URL wsdlLocation, final QName serviceName)
	{
		super(wsdlLocation, serviceName);
	}

	public ReturnWSService(final URL wsdlLocation, final QName serviceName, final WebServiceFeature... features)
	{
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns ReturnWSPort
	 */
	@WebEndpoint(name = "Return_WS_Port")
	public ReturnWSPort getReturnWSPort()
	{
		return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/return_ws", "Return_WS_Port"), ReturnWSPort.class);
	}

	/**
	 *
	 * @param features
	 *           A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy. Supported features not in
	 *           the <code>features</code> parameter will have their default values.
	 * @return returns ReturnWSPort
	 */
	@WebEndpoint(name = "Return_WS_Port")
	public ReturnWSPort getReturnWSPort(final WebServiceFeature... features)
	{
		return super.getPort(new QName("urn:microsoft-dynamics-schemas/page/return_ws", "Return_WS_Port"), ReturnWSPort.class,
				features);
	}


	private static URL __getWsdlLocation(final boolean flag)
	{
		URL url = null;
		WebServiceException e = null;
		try
		{
			if (flag)
			{
				url = ReturnWSService.class.getResource(TEST_WSDL_FILE_LOCATION);
				LOG.info("test env is value : " + flag + "  and the below url will be used : " + url);
			}
			else
			{
				url = ReturnWSService.class.getResource(LIVE_WSDL_FILE_LOCATION);
				LOG.info("test env is value  : " + flag + "  and the below url will be used : " + url);
			}
		}
		catch (final Exception ex)
		{
			e = new WebServiceException(ex);
		}
		RETURNWEBSERVICESERVICEWSDLLOCATION = url;
		RETURNWEBSERVICESERVICEEXCEPTION = e;



		if (RETURNWEBSERVICESERVICEEXCEPTION != null)
		{
			throw RETURNWEBSERVICESERVICEEXCEPTION;
		}
		return RETURNWEBSERVICESERVICEWSDLLOCATION;
	}
}

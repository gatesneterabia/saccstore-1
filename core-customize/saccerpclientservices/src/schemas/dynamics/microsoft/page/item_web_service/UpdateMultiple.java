
package schemas.dynamics.microsoft.page.item_web_service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Item_Web_Service_List" type="{urn:microsoft-dynamics-schemas/page/item_web_service}Item_Web_Service_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itemWebServiceList"
})
@XmlRootElement(name = "UpdateMultiple")
public class UpdateMultiple {

    @XmlElement(name = "Item_Web_Service_List", required = true)
    protected ItemWebServiceList itemWebServiceList;

    /**
     * Gets the value of the itemWebServiceList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemWebServiceList }
     *     
     */
    public ItemWebServiceList getItemWebServiceList() {
        return itemWebServiceList;
    }

    /**
     * Sets the value of the itemWebServiceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemWebServiceList }
     *     
     */
    public void setItemWebServiceList(ItemWebServiceList value) {
        this.itemWebServiceList = value;
    }

}

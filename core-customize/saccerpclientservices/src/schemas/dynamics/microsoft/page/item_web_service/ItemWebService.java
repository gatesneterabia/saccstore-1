
package schemas.dynamics.microsoft.page.item_web_service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.apache.commons.collections.CollectionUtils;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sun.xml.ws.client.BindingProviderProperties;

import schemas.dynamics.microsoft.page.AbstractSaccERPWebService;


/**
 * <p>Java class for Item_Web_Service complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Item_Web_Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Unit_Cost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Sales_Unit_of_Measure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Unit_Price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Base_Unit_of_Measure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Stockkeeping_Unit_Exists" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Inventory" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Net_Weight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Arabic_Summary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBaseProduct" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Warranty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Product_Label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BarCode_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

/**
 *
 * @author Baha Almtoor
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Item_Web_Service", propOrder =
{ "key", "no", "description", "description2", "unitCost", "salesUnitOfMeasure", "unitPrice", "baseUnitOfMeasure",
		"stockkeepingUnitExists", "inventory", "netWeight", "summary", "arabicSummary", "isBaseProduct", "warranty", "productLabel",
		"barCodeNo", "onlineVatPercent" })
@XmlRootElement
public class ItemWebService extends AbstractSaccERPWebService
{
	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "No")
	protected String no = "";
	@XmlElement(name = "Description")
	protected String description = "";
	@XmlElement(name = "Description_2")
	protected String description2 = "";
	@XmlElement(name = "Unit_Cost")
	protected String unitCost = "0";
	@XmlElement(name = "Sales_Unit_of_Measure")
	protected String salesUnitOfMeasure = "";
	@XmlElement(name = "Unit_Price")
	protected String unitPrice = "0";
	@XmlElement(name = "Base_Unit_of_Measure")
	protected String baseUnitOfMeasure = "";
	@XmlElement(name = "Stockkeeping_Unit_Exists")
	protected Boolean stockkeepingUnitExists;
	@XmlElement(name = "Inventory")
	protected String inventory = "0";
	@XmlElement(name = "Net_Weight")
	protected String netWeight = "0";
	@XmlElement(name = "Summary")
	protected String summary = "";
	@XmlElement(name = "Arabic_Summary")
	protected String arabicSummary = "";
	@XmlElement(name = "IsBaseProduct")
	protected Boolean isBaseProduct;
	@XmlElement(name = "Warranty")
	protected String warranty = "";
	@XmlElement(name = "Product_Label")
	protected String productLabel = "";
	@XmlElement(name = "BarCode_No")
	protected String barCodeNo = "";
	@XmlElement(name = "Online_Vat_Percent")
	protected BigDecimal onlineVatPercent;

	private void setTimeoutValue(final ItemWebServicePort itemWebServicePort)
	{
		final BindingProvider provider = (BindingProvider) itemWebServicePort;
		final Map<String, Object> requestContext = provider.getRequestContext();
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getTimeout());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getTimeout());
	}

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the no property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getNo()
	{
		return no;
	}

	/**
	 * Sets the value of the no property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setNo(final String value)
	{
		this.no = value;
	}

	/**
	 * @return the onlineVatPercent
	 */
	public BigDecimal getOnlineVatPercent()
	{
		return onlineVatPercent;
	}

	/**
	 * @param onlineVatPercent
	 *           the onlineVatPercent to set
	 */
	public void setOnlineVatPercent(final BigDecimal onlineVatPercent)
	{
		this.onlineVatPercent = onlineVatPercent;
	}

	/**
	 * @return the stockkeepingUnitExists
	 */
	public Boolean getStockkeepingUnitExists()
	{
		return stockkeepingUnitExists;
	}

	/**
	 * @return the isBaseProduct
	 */
	public Boolean getIsBaseProduct()
	{
		return isBaseProduct;
	}

	/**
	 * Gets the value of the description property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Sets the value of the description property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setDescription(final String value)
	{
		this.description = value;
	}

	/**
	 * Gets the value of the description2 property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getDescription2()
	{
		return description2;
	}

	/**
	 * Sets the value of the description2 property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setDescription2(final String value)
	{
		this.description2 = value;
	}

	/**
	 * Gets the value of the unitCost property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getUnitCost()
	{
		return unitCost;
	}

	/**
	 * Sets the value of the unitCost property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setUnitCost(final String value)
	{
		this.unitCost = value;
	}

	/**
	 * Gets the value of the salesUnitOfMeasure property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getSalesUnitOfMeasure()
	{
		return salesUnitOfMeasure;
	}

	/**
	 * Sets the value of the salesUnitOfMeasure property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setSalesUnitOfMeasure(final String value)
	{
		this.salesUnitOfMeasure = value;
	}

	/**
	 * Gets the value of the unitPrice property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * Sets the value of the unitPrice property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setUnitPrice(final String value)
	{
		this.unitPrice = value;
	}

	/**
	 * Gets the value of the baseUnitOfMeasure property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBaseUnitOfMeasure()
	{
		return baseUnitOfMeasure;
	}

	/**
	 * Sets the value of the baseUnitOfMeasure property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setBaseUnitOfMeasure(final String value)
	{
		this.baseUnitOfMeasure = value;
	}

	/**
	 * Gets the value of the stockkeepingUnitExists property.
	 *
	 * @return possible object is {@link Boolean }
	 *
	 */
	public Boolean isStockkeepingUnitExists()
	{
		return stockkeepingUnitExists;
	}

	/**
	 * Sets the value of the stockkeepingUnitExists property.
	 *
	 * @param value
	 *           allowed object is {@link Boolean }
	 *
	 */
	public void setStockkeepingUnitExists(final Boolean value)
	{
		this.stockkeepingUnitExists = value;
	}

	/**
	 * Gets the value of the inventory property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getInventory()
	{
		return inventory;
	}

	/**
	 * Sets the value of the inventory property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setInventory(final String value)
	{
		this.inventory = value;
	}

	/**
	 * Gets the value of the netWeight property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getNetWeight()
	{
		return netWeight;
	}

	/**
	 * Sets the value of the netWeight property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setNetWeight(final String value)
	{
		this.netWeight = value;
	}

	/**
	 * Gets the value of the summary property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getSummary()
	{
		return summary;
	}

	/**
	 * Sets the value of the summary property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setSummary(final String value)
	{
		this.summary = value;
	}

	/**
	 * Gets the value of the arabicSummary property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getArabicSummary()
	{
		return arabicSummary;
	}

	/**
	 * Sets the value of the arabicSummary property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setArabicSummary(final String value)
	{
		this.arabicSummary = value;
	}

	/**
	 * Gets the value of the isBaseProduct property.
	 *
	 * @return possible object is {@link Boolean }
	 *
	 */
	public Boolean isIsBaseProduct()
	{
		return isBaseProduct;
	}

	/**
	 * Sets the value of the isBaseProduct property.
	 *
	 * @param value
	 *           allowed object is {@link Boolean }
	 *
	 */
	public void setIsBaseProduct(final Boolean value)
	{
		this.isBaseProduct = value;
	}

	/**
	 * Gets the value of the warranty property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getWarranty()
	{
		return warranty;
	}

	/**
	 * Sets the value of the warranty property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setWarranty(final String value)
	{
		this.warranty = value;
	}

	/**
	 * Gets the value of the productLabel property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getProductLabel()
	{
		return productLabel;
	}

	/**
	 * Sets the value of the productLabel property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setProductLabel(final String value)
	{
		this.productLabel = value;
	}

	/**
	 * Gets the value of the barCodeNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getBarCodeNo()
	{
		return barCodeNo;
	}

	/**
	 * Sets the value of the barCodeNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setBarCodeNo(final String value)
	{
		this.barCodeNo = value;
	}

	protected ItemWebServicePort getItemWebServicePort()
	{
		return new ItemWebServiceService(isTestEnv()).getItemWebServicePort();
	}

	public List<ItemWebService> readMultiple(final List<ItemWebServiceFilter> filters, final String bookmarkKey, final int size)
			throws SaccERPWebServiceException
	{
		try
		{
			final ItemWebServicePort itemWebServicePort = getItemWebServicePort();
			setAuthenticator();
			setTimeoutValue(itemWebServicePort);
			final ItemWebServiceList itemWebServiceList = itemWebServicePort.readMultiple(filters, bookmarkKey, size);
			if (itemWebServiceList != null && CollectionUtils.isNotEmpty(itemWebServiceList.getItemWebService()))
			{
				return itemWebServiceList.getItemWebService();
			}

			return Collections.emptyList();
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't readMultiple[ItemWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public ItemWebService read() throws SaccERPWebServiceException
	{
		try
		{
			final ItemWebServicePort itemWebServicePort = getItemWebServicePort();
			setAuthenticator();
			setTimeoutValue(itemWebServicePort);
			return itemWebServicePort.read(getNo());
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't read[ItemWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public ItemWebService update() throws SaccERPWebServiceException
	{
		try
		{
			final ItemWebServicePort itemWebServicePort = getItemWebServicePort();
			setAuthenticator();
			setTimeoutValue(itemWebServicePort);
			final Holder<ItemWebService> itemWebServiceHolder = new Holder<ItemWebService>(this);
			itemWebServicePort.update(itemWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't update[ItemWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public ItemWebService create() throws SaccERPWebServiceException
	{
		try
		{
			final ItemWebServicePort itemWebServicePort = getItemWebServicePort();
			setAuthenticator();
			setTimeoutValue(itemWebServicePort);
			final Holder<ItemWebService> itemWebServiceHolder = new Holder<ItemWebService>(this);
			itemWebServicePort.create(itemWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't create[ItemWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public String toString()
	{
		return "ItemWebService [key=" + key + ", no=" + no + ", description=" + description + ", description2=" + description2
				+ ", unitCost=" + unitCost + ", salesUnitOfMeasure=" + salesUnitOfMeasure + ", unitPrice=" + unitPrice
				+ ", baseUnitOfMeasure=" + baseUnitOfMeasure + ", stockkeepingUnitExists=" + stockkeepingUnitExists + ", inventory="
				+ inventory + ", netWeight=" + netWeight + ", summary=" + summary + ", arabicSummary=" + arabicSummary
				+ ", isBaseProduct=" + isBaseProduct + ", warranty=" + warranty + ", productLabel=" + productLabel + ", barCodeNo="
				+ barCodeNo + ", onlineVatPercent=" + onlineVatPercent + "]";
	}



}


package schemas.dynamics.microsoft.page.payment_web_service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Payment_Web_Service_List" type="{urn:microsoft-dynamics-schemas/page/payment_web_service}Payment_Web_Service_List"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paymentWebServiceList"
})
@XmlRootElement(name = "CreateMultiple_Result")
public class CreateMultipleResult {

    @XmlElement(name = "Payment_Web_Service_List", required = true)
    protected PaymentWebServiceList paymentWebServiceList;

    /**
     * Gets the value of the paymentWebServiceList property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentWebServiceList }
     *     
     */
    public PaymentWebServiceList getPaymentWebServiceList() {
        return paymentWebServiceList;
    }

    /**
     * Sets the value of the paymentWebServiceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentWebServiceList }
     *     
     */
    public void setPaymentWebServiceList(PaymentWebServiceList value) {
        this.paymentWebServiceList = value;
    }

}


package schemas.dynamics.microsoft.page.payment_web_service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Payment_Web_Service" type="{urn:microsoft-dynamics-schemas/page/payment_web_service}Payment_Web_Service"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paymentWebService"
})
@XmlRootElement(name = "Create_Result")
public class CreateResult {

    @XmlElement(name = "Payment_Web_Service", required = true)
    protected PaymentWebService paymentWebService;

    /**
     * Gets the value of the paymentWebService property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentWebService }
     *     
     */
    public PaymentWebService getPaymentWebService() {
        return paymentWebService;
    }

    /**
     * Sets the value of the paymentWebService property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentWebService }
     *     
     */
    public void setPaymentWebService(PaymentWebService value) {
        this.paymentWebService = value;
    }

}

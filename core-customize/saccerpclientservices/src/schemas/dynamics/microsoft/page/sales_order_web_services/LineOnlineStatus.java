
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Line_Online_Status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Line_Online_Status">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Ready_to_ship"/>
 *     &lt;enumeration value="Shipped"/>
 *     &lt;enumeration value="Delivered"/>
 *     &lt;enumeration value="Partial_Cancelation"/>
 *     &lt;enumeration value="Fully_Cancel"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Line_Online_Status")
@XmlEnum
public enum LineOnlineStatus {

    @XmlEnumValue("Ready_to_ship")
    READY_TO_SHIP("Ready_to_ship"),
    @XmlEnumValue("Shipped")
    SHIPPED("Shipped"),
    @XmlEnumValue("Delivered")
    DELIVERED("Delivered"),
    @XmlEnumValue("Partial_Cancelation")
    PARTIAL_CANCELATION("Partial_Cancelation"),
    @XmlEnumValue("Fully_Cancel")
    FULLY_CANCEL("Fully_Cancel");
    private final String value;

    LineOnlineStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LineOnlineStatus fromValue(String v) {
        for (LineOnlineStatus c: LineOnlineStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

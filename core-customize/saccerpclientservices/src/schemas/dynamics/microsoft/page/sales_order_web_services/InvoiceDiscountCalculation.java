
package schemas.dynamics.microsoft.page.sales_order_web_services;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Invoice_Discount_Calculation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Invoice_Discount_Calculation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Percent"/>
 *     &lt;enumeration value="Amount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Invoice_Discount_Calculation")
@XmlEnum
public enum InvoiceDiscountCalculation {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Percent")
    PERCENT("Percent"),
    @XmlEnumValue("Amount")
    AMOUNT("Amount");
    private final String value;

    InvoiceDiscountCalculation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvoiceDiscountCalculation fromValue(String v) {
        for (InvoiceDiscountCalculation c: InvoiceDiscountCalculation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

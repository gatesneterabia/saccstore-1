/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package schemas.dynamics.microsoft.page;

import java.net.Authenticator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import com.sacc.saccerpclientservices.client.auth.NTLMAuthenticator;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;


/**
 *
 * @author Baha Almtoor
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractSaccERPWebService
{
	@XmlTransient
	private String username = "";
	@XmlTransient
	private String password = "";
	@XmlTransient
	private boolean testEnv = true;
	@XmlTransient
	private int timeout = 20000;

	public String getUsername()
	{
		return username;
	}

	public void setUsername(final String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public boolean isTestEnv()
	{
		return testEnv;
	}


	public void setTestEnv(final boolean testEnv)
	{
		this.testEnv = testEnv;
	}

	public int getTimeout()
	{
		return timeout;
	}

	public void setTimeout(final int timeout)
	{
		this.timeout = timeout;
	}

	protected void setAuthenticator()
	{
		Authenticator.setDefault(new NTLMAuthenticator(getUsername(), getPassword()));
	}

	abstract public AbstractSaccERPWebService update() throws SaccERPWebServiceException;

	abstract public AbstractSaccERPWebService create() throws SaccERPWebServiceException;

	abstract public AbstractSaccERPWebService read() throws SaccERPWebServiceException;
}


package schemas.dynamics.microsoft.page.customer_web_service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Customer_Web_Service" type="{urn:microsoft-dynamics-schemas/page/customer_web_service}Customer_Web_Service"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerWebService"
})
@XmlRootElement(name = "Create_Result")
public class CreateResult {

    @XmlElement(name = "Customer_Web_Service", required = true)
    protected CustomerWebService customerWebService;

    /**
     * Gets the value of the customerWebService property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerWebService }
     *     
     */
    public CustomerWebService getCustomerWebService() {
        return customerWebService;
    }

    /**
     * Sets the value of the customerWebService property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerWebService }
     *     
     */
    public void setCustomerWebService(CustomerWebService value) {
        this.customerWebService = value;
    }

}


package schemas.dynamics.microsoft.page.customer_web_service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sun.xml.ws.client.BindingProviderProperties;

import schemas.dynamics.microsoft.page.AbstractSaccERPWebService;


/**
 * <p>
 * Java class for Customer_Web_Service complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Customer_Web_Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Search_Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone_No" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="E_Mail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Territory_Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

/**
 *
 * @author Baha Almtoor
 *
 */
@XmlRootElement(name = "CustomerWebService")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer_Web_Service", propOrder =
{ "key", "no", "name", "searchName", "name2", "address", "address2", "city", "contact", "phoneNo", "eMail", "territoryCode" })
public class CustomerWebService extends AbstractSaccERPWebService
{
	@XmlElement(name = "Key")
	protected String key = "";
	@XmlElement(name = "No")
	protected String no = "";
	@XmlElement(name = "Name")
	protected String name = "";
	@XmlElement(name = "Search_Name")
	protected String searchName = "";
	@XmlElement(name = "Name_2")
	protected String name2 = "";
	@XmlElement(name = "Address")
	protected String address = "";
	@XmlElement(name = "Address_2")
	protected String address2 = "";
	@XmlElement(name = "City")
	protected String city = "";
	@XmlElement(name = "Contact")
	protected String contact = "";
	@XmlElement(name = "Phone_No")
	protected String phoneNo = "";
	@XmlElement(name = "E_Mail")
	protected String eMail = "";
	@XmlElement(name = "Territory_Code")
	protected String territoryCode = "";

	/**
	 * Gets the value of the key property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * Sets the value of the key property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setKey(final String value)
	{
		this.key = value;
	}

	/**
	 * Gets the value of the no property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getNo()
	{
		return no;
	}

	/**
	 * Sets the value of the no property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setNo(final String value)
	{
		this.no = value;
	}

	/**
	 * Gets the value of the name property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the value of the name property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setName(final String value)
	{
		this.name = value;
	}

	/**
	 * Gets the value of the searchName property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getSearchName()
	{
		return searchName;
	}

	/**
	 * Sets the value of the searchName property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setSearchName(final String value)
	{
		this.searchName = value;
	}

	/**
	 * Gets the value of the name2 property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getName2()
	{
		return name2;
	}

	/**
	 * Sets the value of the name2 property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setName2(final String value)
	{
		this.name2 = value;
	}

	/**
	 * Gets the value of the address property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * Sets the value of the address property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setAddress(final String value)
	{
		this.address = value;
	}

	/**
	 * Gets the value of the address2 property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getAddress2()
	{
		return address2;
	}

	/**
	 * Sets the value of the address2 property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setAddress2(final String value)
	{
		this.address2 = value;
	}

	/**
	 * Gets the value of the city property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * Sets the value of the city property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setCity(final String value)
	{
		this.city = value;
	}

	/**
	 * Gets the value of the contact property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getContact()
	{
		return contact;
	}

	/**
	 * Sets the value of the contact property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setContact(final String value)
	{
		this.contact = value;
	}

	/**
	 * Gets the value of the phoneNo property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getPhoneNo()
	{
		return phoneNo;
	}

	/**
	 * Sets the value of the phoneNo property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setPhoneNo(final String value)
	{
		this.phoneNo = value;
	}

	/**
	 * Gets the value of the eMail property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getEMail()
	{
		return eMail;
	}

	/**
	 * Sets the value of the eMail property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setEMail(final String value)
	{
		this.eMail = value;
	}

	/**
	 * Gets the value of the territoryCode property.
	 *
	 * @return possible object is {@link String }
	 *
	 */
	public String getTerritoryCode()
	{
		return territoryCode;
	}

	/**
	 * Sets the value of the territoryCode property.
	 *
	 * @param value
	 *           allowed object is {@link String }
	 *
	 */
	public void setTerritoryCode(final String value)
	{
		this.territoryCode = value;
	}

	protected CustomerWebServicePort getCustomerWebServicePort()
	{
		return new CustomerWebServiceService(isTestEnv()).getCustomerWebServicePort();
	}

	public List<CustomerWebService> readMultiple(final List<CustomerWebServiceFilter> filters, final String bookmarkKey,
			final int size) throws SaccERPWebServiceException
	{
		try
		{
			final CustomerWebServicePort customerWebServicePort = getCustomerWebServicePort();
			setAuthenticator();
			setTimeoutValue(customerWebServicePort);
			final CustomerWebServiceList customerWebServiceList = customerWebServicePort.readMultiple(filters, bookmarkKey, size);
			if (customerWebServiceList != null)
			{
				return customerWebServiceList.getCustomerWebService();
			}

			return Collections.emptyList();
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't readMultiple[CustomerWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	private void setTimeoutValue(final CustomerWebServicePort customerWebServicePort)
	{
		final BindingProvider provider = (BindingProvider) customerWebServicePort;
		final Map<String, Object> requestContext = provider.getRequestContext();
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getTimeout());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getTimeout());
	}

	@Override
	public CustomerWebService read() throws SaccERPWebServiceException
	{
		try
		{
			final CustomerWebServicePort customerWebServicePort = getCustomerWebServicePort();
			setAuthenticator();
			setTimeoutValue(customerWebServicePort);
			return customerWebServicePort.read(getNo());
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't read[CustomerWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}
	}

	@Override
	public CustomerWebService update() throws SaccERPWebServiceException
	{
		throw new SaccERPWebServiceException("Service update[CustomerWebService] not found", SaccERPExeptionType.CLIENT_FORBIDDEN);
	}

	@Override
	public CustomerWebService create() throws SaccERPWebServiceException
	{
		try
		{
			final CustomerWebServicePort customerWebServicePort = getCustomerWebServicePort();
			setAuthenticator();
			setTimeoutValue(customerWebServicePort);
			final Holder<CustomerWebService> customerWebServiceHolder = new Holder<CustomerWebService>(this);
			customerWebServicePort.create(customerWebServiceHolder);
			try
			{
				return read();
			}
			catch (final SaccERPWebServiceException ex)
			{
				ex.printStackTrace();
			}
		}
		catch (final Exception ex)
		{
			throw new SaccERPWebServiceException("Can't create[CustomerWebService]", ex, SaccERPExeptionType.CLIENT_FORBIDDEN);
		}

		return this;
	}

	@Override
	public String toString()
	{
		return "CustomerWebService [key=" + key + ", no=" + no + ", name=" + name + ", searchName=" + searchName + ", name2="
				+ name2 + ", address=" + address + ", address2=" + address2 + ", city=" + city + ", contact=" + contact + ", phoneNo="
				+ phoneNo + ", eMail=" + eMail + ", territoryCode=" + territoryCode + "]";
	}
}

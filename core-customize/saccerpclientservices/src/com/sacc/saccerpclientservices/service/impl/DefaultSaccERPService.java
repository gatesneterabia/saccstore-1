/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.core.enums.IntegrationProvider;
import com.sacc.core.event.SendErrorEmailEvent;
import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPCustomerWebService;
import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.client.service.SaccERPPaymentWebService;
import com.sacc.saccerpclientservices.client.service.SaccERPReturnWebService;
import com.sacc.saccerpclientservices.client.service.SaccERPSalesOrderWebService;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccerpclientservices.service.SaccERPService;

import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebService;
import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;
import schemas.dynamics.microsoft.page.payment_web_service.PaymentWebService;
import schemas.dynamics.microsoft.page.return_ws.ReturnWS;
import schemas.dynamics.microsoft.page.return_ws.WSReturnOrderLine;
import schemas.dynamics.microsoft.page.return_ws.WSReturnOrderLineList;
import schemas.dynamics.microsoft.page.sales_order_web_services.OnlineStatus;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;
import schemas.dynamics.microsoft.page.sales_order_web_services.WSSalesOrderLine;
import schemas.dynamics.microsoft.page.sales_order_web_services.WSSalesOrderLineList;


/**
 * @author mnasro The Class DefaultSaccERPService.
 */
public class DefaultSaccERPService implements SaccERPService
{
	private static final String ORDER_ENV_POSTFIX = "erp.environment.ordercode.postfix";
	private static final Logger LOG = Logger.getLogger(DefaultSaccERPService.class);

	@Resource(name = "orderOnlineStatusMap")
	private Map<String, OnlineStatus> orderOnlineStatusMap;

	@Resource(name = "saccERPSalesOrderWebService")
	private SaccERPSalesOrderWebService salesOrderWebServices;

	@Resource(name = "saccERPCustomerWebService")
	private SaccERPCustomerWebService customerWebService;

	@Resource(name = "saccERPReturnWebService")
	private SaccERPReturnWebService returnWebService;

	@Resource(name = "saccERPPaymentWebService")
	private SaccERPPaymentWebService paymentWebService;

	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService itemWebService;

	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "eventService")
	private EventService eventService;

	@Override
	public void createSalesOrderAction(final OrderModel orderModel)
	{
		if (!StringUtils.isEmpty(orderModel.getSaccERPKey()))
		{
			LOG.warn("Order " + orderModel.getCode() + " already created on SACC ERP, skipping!");
			return;
		}
		boolean isCustomerCreated = false;
		boolean isSalesOrderCreated = false;
		boolean isPaymentCreated = false;

		isCustomerCreated = createCustomer(orderModel);
		isSalesOrderCreated = createSalesOrder(orderModel);
		if (isSalesOrderCreated)
		{
			isPaymentCreated = createPayment(orderModel);
		}
		else
		{
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PAYMENT, orderModel.getCode(), orderModel.getVersionID(),
					extractConsignmentCode(orderModel), "Payment not created because Sales Order was not created successfully",
					"Check Sales Order Creation Record for Exception Info", Boolean.FALSE);
		}
		if (isPaymentCreated)
		{
			LOG.info("Sales order created full successfully for order : " + orderModel.getCode());
		}
	}

	/**
	 * Creates the sales order.
	 *
	 * @param orderModel
	 *           the order model
	 */
	@Override
	public boolean createSalesOrder(final OrderModel orderModel)
	{
		SalesOrderWebServices obj = null;
		try
		{
			obj = getSalesOrderWebServices(orderModel);
		}
		catch (final SaccERPWebServiceException e1)
		{
			LOG.error(e1.getMessage(), e1);
			LOG.error("Could not create SalesOrderWebService");
			final String stacktrace = ExceptionUtils.getStackTrace(e1);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_SALES_ORDER, orderModel.getCode(),
					orderModel.getVersionID(), extractConsignmentCode(orderModel), obj.toString(), ExceptionUtils.getStackTrace(e1),
					Boolean.FALSE);
			LOG.info("Record created for " + ERPWebServiceType.CREATE_SALES_ORDER + " Web Service : " + orderModel.getCode()
					+ " successfully : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, e1.getCause() != null ? e1.getCause().getMessage() : e1.getMessage());
			return false;
		}
		try
		{
			if (obj != null)
			{
				final SalesOrderWebServices createdSalesOrder = salesOrderWebServices.create(obj);
				LOG.info("Sales Order " + createdSalesOrder.getKey() + " has been created successfully");
				saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_SALES_ORDER, orderModel.getCode(),
						orderModel.getVersionID(), extractConsignmentCode(orderModel), obj.toString(), createdSalesOrder.toString(),
						Boolean.TRUE);
				LOG.info("Record created for " + ERPWebServiceType.CREATE_SALES_ORDER + " Web Service : " + orderModel.getCode()
						+ " successfully : " + Boolean.TRUE);
				orderModel.setSaccERPKey(createdSalesOrder.getNo());
				modelService.save(orderModel);
				modelService.refresh(orderModel);
				setAllRecordsOfThisOperationAsDone(orderModel, ERPWebServiceType.CREATE_SALES_ORDER);
				return true;
			}
			else
			{

				LOG.error("Could not create SalesOrderWebService");
				saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_SALES_ORDER, orderModel.getCode(),
						orderModel.getVersionID(), extractConsignmentCode(orderModel), obj.toString(),
						"couldn't create SalesOrderWebServices from order", Boolean.FALSE);
				LOG.info("Record created for " + ERPWebServiceType.CREATE_SALES_ORDER + " Web Service : " + orderModel.getCode()
						+ " successfully : " + Boolean.FALSE);
				publishErrorEmailEvent(orderModel, "Could not build SalesOrderDTO, DTO is null.");
				return false;
			}
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stacktrace = ExceptionUtils.getStackTrace(e);
			LOG.error("ERROR SAVING ORDER " + obj.getNo() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_SALES_ORDER, orderModel.getCode(),
					orderModel.getVersionID(), extractConsignmentCode(orderModel), obj.toString(), stacktrace, Boolean.FALSE);
			LOG.info("Record created for " + ERPWebServiceType.CREATE_SALES_ORDER + " Web Service : " + orderModel.getCode()
					+ " successfully : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}
	}

	/**
	 *
	 */
	private SalesOrderWebServices createPaymentCostLine(final OrderModel orderModel, final SalesOrderWebServices createdSalesOrder)
	{
		LOG.info("Creating OCOD line in sale order with code : " + orderModel.getCode());
		final List<WSSalesOrderLine> wsSalesOrderLine = createdSalesOrder.getWSSalesOrderLine().getWSSalesOrderLine();
		final WSSalesOrderLine paymentCostLine = new WSSalesOrderLine();
		paymentCostLine.setBarcodeNo("OCOD");
		paymentCostLine.setQuantity("1");
		paymentCostLine.setLineDiscountAmount("0");
		paymentCostLine.setUnitPrice(orderModel.getPaymentCost().toString());
		paymentCostLine.setLineAmount(orderModel.getPaymentCost().toString());
		paymentCostLine.setCancelQuantity("0");
		paymentCostLine.setPickedQuantity("0");
		wsSalesOrderLine.add(paymentCostLine);
		return createdSalesOrder;
	}

	/**
	 *
	 */
	private SalesOrderWebServices createDeliveryCostLine(final OrderModel orderModel,
			final SalesOrderWebServices createdSalesOrder)
	{
		LOG.info("Creating DEL line in sale order with code : " + orderModel.getCode());
		final List<WSSalesOrderLine> wsSalesOrderLine = createdSalesOrder.getWSSalesOrderLine().getWSSalesOrderLine();
		final WSSalesOrderLine deliveryCostLine = new WSSalesOrderLine();
		deliveryCostLine.setBarcodeNo("DEL");
		deliveryCostLine.setQuantity("1");
		deliveryCostLine.setLineDiscountAmount("0");
		deliveryCostLine.setUnitPrice(orderModel.getDeliveryCost().toString());
		deliveryCostLine.setLineAmount(orderModel.getDeliveryCost().toString());
		deliveryCostLine.setCancelQuantity("0");
		deliveryCostLine.setPickedQuantity("0");
		wsSalesOrderLine.add(deliveryCostLine);
		return createdSalesOrder;

	}

	private String extractConsignmentCode(final OrderModel order)
	{
		if (CollectionUtils.isEmpty(order.getConsignments()))
		{
			return null;
		}
		final List<String> consignmentsCodes = order.getConsignments().stream().map(con -> con.getCode())
				.collect(Collectors.toList());
		return consignmentsCodes.get(consignmentsCodes.size() - 1);
	}

	/**
	 * Gets the sales order web services.
	 *
	 * @param orderModel
	 *           the order model
	 * @return the sales order web services
	 * @throws SaccERPWebServiceException
	 */
	private SalesOrderWebServices getSalesOrderWebServices(final OrderModel orderModel) throws SaccERPWebServiceException
	{
		final CustomerModel customer = (CustomerModel) orderModel.getUser();
		final SalesOrderWebServices salesOrderWebServices = new SalesOrderWebServices();
		final String orderCode = getSalesOrderCode(orderModel.getCode());
		salesOrderWebServices.setNo(orderCode);
		salesOrderWebServices.setKey("");
		salesOrderWebServices.setExternalDocumentNo(orderCode);
		salesOrderWebServices.setSellToCustomerNo(customer.getPk().getLongValueAsString());
		salesOrderWebServices.setOrderDate(convertDateToXMLGregorianCalendar(orderModel.getCreationtime()));
		//				setDeliveryDate(salesOrderWebServices, saccERPClientSalesOrderWebService, consignment);
		final String status = orderModel.getConsignments().stream().map(con -> con.getStatus().getCode()).findFirst().get();
		salesOrderWebServices.setOnlineStatus(OnlineStatus.READY);
		setWSSalesOrderLine(salesOrderWebServices, orderModel);

		final Double deliveryCost = orderModel.getDeliveryCost();
		final Double paymentCost = orderModel.getPaymentCost();

		if (deliveryCost != null && deliveryCost != 0)
		{
			createDeliveryCostLine(orderModel, salesOrderWebServices);
		}

		if (paymentCost != null && paymentCost != 0)
		{
			createPaymentCostLine(orderModel, salesOrderWebServices);
		}

		return salesOrderWebServices;
	}

	private String getSalesOrderCode(final String code)
	{
		final String postfix = (String) configurationService.getConfiguration().getProperty(ORDER_ENV_POSTFIX);
		if (!StringUtils.isEmpty(postfix))
		{
			return code + postfix;
		}
		return code;
	}

	protected void pickSaleOrderLine(final SalesOrderWebServices salesOrderWebServices, final OrderModel order)
	{

		for (final WSSalesOrderLine line : salesOrderWebServices.getWSSalesOrderLine().getWSSalesOrderLine())
		{
			line.setPickedQuantity(line.getQuantity());

		}
	}

	protected void setWSSalesOrderLine(final SalesOrderWebServices salesOrderWebServices, final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			salesOrderWebServices.setWSSalesOrderLine(new WSSalesOrderLineList());
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry != null)
				{
					final WSSalesOrderLine wsSalesOrderLine = new WSSalesOrderLine();
					wsSalesOrderLine.setKey("");
					if (entry.getQuantity() == null || entry.getQuantity() == 0)
					{
						continue;
					}
					if (entry.getProduct() != null)
					{
						try
						{
							final List<ItemWebService> read = itemWebService.readMultiple(1, entry.getProduct().getCode());
							if (read == null || read.isEmpty())
							{
								throw new SaccERPWebServiceException(
										"Item with barcode " + entry.getProduct().getCode() + " not available",
										SaccERPExeptionType.INVALID_PRODUCT);
							}
							LOG.info("Item with barcode " + read.get(0).getBarCodeNo() + " is available");
							LOG.info("The returend data from  itemWebService : " + read);



						}
						catch (final SaccERPWebServiceException e)
						{
							//							saccERPOrderOperationService.saveRecord(order.getCode(), entry.getProduct().getCode(), order.getVersionID(),
							//									extractConsignmentCode(order), entry.toString(), ExceptionUtils.getStackTrace(e), Boolean.FALSE);
							LOG.error("Item with barcode " + entry.getProduct().getCode() + " not available");
						}
						wsSalesOrderLine.setBarcodeNo(entry.getProduct().getCode());
					}
					if (entry.getQuantity() != null)
					{
						wsSalesOrderLine.setQuantity(String.valueOf(entry.getQuantity()));
					}
					if (entry.getBasePrice() != null)
					{
						wsSalesOrderLine.setUnitPrice(String.valueOf(entry.getBasePrice()));
					}
					if (CollectionUtils.isNotEmpty(entry.getDiscountValues()))
					{
						wsSalesOrderLine.setLineDiscountAmount(
								String.valueOf(entry.getDiscountValues().stream().mapToDouble(DiscountValue::getValue).sum()));
					}
					else
					{
						wsSalesOrderLine.setLineDiscountAmount("0");
					}
					wsSalesOrderLine.setLineAmount(String.valueOf(entry.getTotalPrice()));
					wsSalesOrderLine.setCancelQuantity("0");
					wsSalesOrderLine.setPickedQuantity("0");
					salesOrderWebServices.getWSSalesOrderLine().getWSSalesOrderLine().add(wsSalesOrderLine);
				}
			}
		}
	}

	protected XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) throws SaccERPWebServiceException
	{
		XMLGregorianCalendar xmlGregorianCalendar = null;
		if (date != null)
		{
			final GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTime(date);
			try
			{
				xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
				xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			}
			catch (final DatatypeConfigurationException ex)
			{
				throw new SaccERPWebServiceException("Can't convert from Date to XMLGregorianCalendar", ex);
			}
		}

		return xmlGregorianCalendar;
	}

	/**
	 * Update sales order.
	 *
	 * @param orderModel
	 *           the order model
	 */
	/**
	 * Update sales order.
	 *
	 * @param orderModel
	 *           the order model
	 * @throws SaccERPWebServiceException
	 *            the sacc ERP web service exception
	 */
	@Override
	public void updateSalesOrderStatus(final OrderModel orderModel, final ERPWebServiceType webServiceType)
			throws SaccERPWebServiceException
	{

		Preconditions.checkArgument(orderModel != null, "orderModel  is null");
		final CustomerModel customer = (CustomerModel) orderModel.getUser();
		Preconditions.checkArgument(customer != null, "customer  is null");
		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = salesOrderWebServices.read(orderModel.getSaccERPKey());
			if (salesOrder == null || org.apache.commons.lang3.StringUtils.isBlank(salesOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + orderModel.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}

			String status = null;
			if (ERPWebServiceType.PACK_SALES_ORDER.equals(webServiceType))
			{
				status = ConsignmentStatus.READY_FOR_SHIPPING.toString();
			}
			else if (ERPWebServiceType.SHIP_SALES_ORDER.equals(webServiceType))
			{
				status = ConsignmentStatus.SHIPPED.toString();
			}
			else if (ERPWebServiceType.DELIVER_SALES_ORDER.equals(webServiceType))
			{
				status = ConsignmentStatus.DELIVERY_COMPLETED.toString();
			}
			salesOrder.setOnlineStatus(orderOnlineStatusMap.get(status));


			final SalesOrderWebServices update = salesOrderWebServices.update(salesOrder);
			saccERPOrderOperationService.saveRecord(webServiceType, orderModel.getCode(), orderModel.getVersionID(),
					extractConsignmentCode(orderModel), salesOrder.toString(), update.toString(), Boolean.TRUE);
			setAllRecordsOfThisOperationAsDone(orderModel, webServiceType);
			LOG.info("Update Record created for " + orderModel.getCode() + " successfully , Status(done) is :" + Boolean.TRUE);

		}
		catch (final SaccERPWebServiceException e)
		{
			final String stacktrace = ExceptionUtils.getStackTrace(e);
			LOG.error("Could not Update SalesOrderWebService");
			LOG.error("ERROR UPDATING ORDER " + orderModel.getSaccERPKey() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(webServiceType, orderModel.getCode(), orderModel.getVersionID(),
					extractConsignmentCode(orderModel), salesOrder == null ? orderModel.getSaccERPKey() : salesOrder.toString(),
					stacktrace, Boolean.FALSE);
			LOG.info("Update Record created for " + orderModel.getCode() + " successfully,Status(done) is : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			throw e;
		}
	}

	@Override
	public boolean createCustomer(final OrderModel orderModel)
	{
		final CustomerWebService customer = getCustomerWebService(orderModel);
		if (customer != null)
		{
			try
			{
				// read by number
				final CustomerWebService readCustomer = customerWebService.read(customer.getNo());
				if (readCustomer == null)
				{
					throw new SaccERPWebServiceException("Retrieved customer is null.");
				}
				LOG.info("Customer " + readCustomer.getKey() + " does exist.");
				return true;
			}
			catch (final SaccERPWebServiceException e1)
			{
				try
				{
					final CustomerWebService createdCustomer = customerWebService.create(customer);
					LOG.info("Customer " + createdCustomer.getKey() + " has been created successfully");
					saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_CUSTOMER, orderModel.getCode(),
							orderModel.getVersionID(), extractConsignmentCode(orderModel), customer.toString(),
							createdCustomer.toString(), Boolean.TRUE);
					setAllRecordsOfThisOperationAsDone(orderModel, ERPWebServiceType.CREATE_CUSTOMER);
					LOG.info("Record created for " + ERPWebServiceType.CREATE_CUSTOMER + " Web Service : " + orderModel.getCode()
							+ " successfully : " + Boolean.TRUE);
					return true;
				}
				catch (final SaccERPWebServiceException e)
				{
					final String stacktrace = ExceptionUtils.getStackTrace(e);
					LOG.error("ERROR CREATING CUSTOMER FOR ORDER " + orderModel.getCode() + " ON SACC ERP: " + e.getMessage(), e);
					saccERPOrderOperationService.saveRecord(ERPWebServiceType.CREATE_CUSTOMER, orderModel.getCode(),
							orderModel.getVersionID(), extractConsignmentCode(orderModel), customer.toString(), stacktrace,
							Boolean.FALSE);
					LOG.info("Record created for " + ERPWebServiceType.CREATE_CUSTOMER + " Web Service : " + orderModel.getCode()
							+ " successfully : " + Boolean.FALSE);
					publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
					return false;
				}
			}
		}
		else
		{
			LOG.error("Customer could not be created, null");
			LOG.info("Record created for " + ERPWebServiceType.CREATE_CUSTOMER + " Web Service : " + orderModel.getCode()
					+ " successfully : " + Boolean.FALSE);
			return false;
		}
	}

	private void setAllRecordsOfThisOperationAsDone(final OrderModel order, final ERPWebServiceType webServiceOperation)
	{
		final List<SaccERPOrderOperationRecordModel> records = saccERPOrderOperationService
				.getAllOrderRecords(Arrays.asList(webServiceOperation), order.getCode(), null, null, Boolean.FALSE);
		if (CollectionUtils.isNotEmpty(records))
		{
			records.forEach(record -> updateRecord(record));
		}
	}

	private void updateRecord(final SaccERPOrderOperationRecordModel record)
	{
		record.setDone(true);
		modelService.save(record);
	}

	private void publishErrorEmailEvent(final OrderModel order, final String stacktrace)
	{
		getEventService().publishEvent(new SendErrorEmailEvent(IntegrationProvider.ERP, order, stacktrace));
	}

	private CustomerWebService getCustomerWebService(final OrderModel order)
	{

		final CustomerModel customer = (CustomerModel) order.getUser();
		final CustomerWebService customerWS = new CustomerWebService();
		customerWS.setNo(customer.getPk().getLongValueAsString());
		customerWS.setName(customer.getName());
		customerWS.setPhoneNo(customer.getMobileNumber());
		customerWS.setEMail(customer.getContactEmail());


		return customerWS;
	}

	@Override
	public boolean createPayment(final OrderModel orderModel)
	{
		PaymentWebService payment = null;
		try
		{
			payment = getPaymentWebService(orderModel);
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stacktrace = ExceptionUtils.getStackTrace(e);
			LOG.error("ERROR CREATING PAYMENT FOR ORDER " + orderModel.getCode() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PAYMENT, orderModel.getCode(), orderModel.getVersionID(),
					extractConsignmentCode(orderModel), payment.toString(), stacktrace, Boolean.FALSE);
			LOG.info("Record created for " + ERPWebServiceType.PAYMENT + " Web Service : " + orderModel.getCode()
					+ " successfully : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}
		if (payment != null)
		{
			try
			{
				final PaymentWebService createdPayment = paymentWebService.create(payment);
				LOG.info("payment created with key : " + createdPayment.getKey());
				saccERPOrderOperationService.saveRecord(ERPWebServiceType.PAYMENT, orderModel.getCode(), orderModel.getVersionID(),
						extractConsignmentCode(orderModel), payment.toString(), createdPayment.toString(), Boolean.TRUE);
				setAllRecordsOfThisOperationAsDone(orderModel, ERPWebServiceType.PAYMENT);
				LOG.info("Record created for " + ERPWebServiceType.PAYMENT + " Web Service : " + orderModel.getCode()
						+ " successfully : " + Boolean.TRUE);
				return true;
			}
			catch (final SaccERPWebServiceException e)
			{
				final String stackTrace = ExceptionUtils.getStackTrace(e);
				LOG.error("ERROR CREATING PAYMENT FOR ORDER " + orderModel.getCode() + " ON SACC ERP: " + e.getMessage(), e);
				saccERPOrderOperationService.saveRecord(ERPWebServiceType.PAYMENT, orderModel.getCode(), orderModel.getVersionID(),
						extractConsignmentCode(orderModel), payment.toString(), stackTrace, Boolean.FALSE);
				LOG.info("Record created for " + ERPWebServiceType.PAYMENT + " Web Service : " + orderModel.getCode()
						+ " successfully : " + Boolean.FALSE);
				publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
				return false;
			}
		}
		else
		{
			LOG.error("Payment could request could not be created, null");
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PAYMENT, orderModel.getCode(), orderModel.getVersionID(),
					extractConsignmentCode(orderModel), "null", "Could not create PaymentWebService from order", Boolean.FALSE);
			LOG.info("Record created for " + ERPWebServiceType.PAYMENT + " Web Service : " + orderModel.getCode()
					+ " successfully : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, "Could not build Payment DTO");
			return false;
		}
	}

	@Override
	public boolean pickSaleOrder(final OrderModel orderModel)
	{
		Preconditions.checkArgument(orderModel != null, "orderModel  is null");
		final CustomerModel customer = (CustomerModel) orderModel.getUser();
		Preconditions.checkArgument(customer != null, "customer  is null");
		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = salesOrderWebServices.read(orderModel.getSaccERPKey());
			if (salesOrder == null || org.apache.commons.lang3.StringUtils.isBlank(salesOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + orderModel.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}
			pickSaleOrderLine(salesOrder, orderModel);
			salesOrder.setOnlineStatus(OnlineStatus.PICK);
			final SalesOrderWebServices update = salesOrderWebServices.update(salesOrder);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PICK_SALES_ORDER, orderModel.getCode(),
					orderModel.getVersionID(), extractConsignmentCode(orderModel), salesOrder.toString(), update.toString(),
					Boolean.TRUE);
			setAllRecordsOfThisOperationAsDone(orderModel, ERPWebServiceType.PICK_SALES_ORDER);
			LOG.info("Update Record created for " + orderModel.getCode() + " successfully , Status(done) is :" + Boolean.TRUE);
			return true;
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			LOG.error("Could not Update SalesOrderWebService");
			LOG.error("ERROR CREATING PICK FOR ORDER " + orderModel.getCode() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PICK_SALES_ORDER, orderModel.getCode(),
					orderModel.getVersionID(), extractConsignmentCode(orderModel),
					salesOrder == null ? orderModel.getSaccERPKey() : salesOrder.toString(), stackTrace, Boolean.FALSE);
			LOG.info("Update Record created for " + orderModel.getCode() + " successfully,Status(done) is : " + Boolean.FALSE);
			publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}

	}

	private PaymentWebService getPaymentWebService(final OrderModel order) throws SaccERPWebServiceException
	{
		if (StringUtils.isEmpty(order.getSaccERPKey()))
		{
			return null;
		}
		final CustomerModel customer = (CustomerModel) order.getUser();
		final PaymentWebService paymentWS = new PaymentWebService();
		paymentWS.setKey(order.getSaccERPKey());
		paymentWS.setCustomerNo(customer.getPk().toString());
		paymentWS.setExternalDocumentNo(order.getSaccERPKey());
		paymentWS.setAmount(order.getTotalPrice() != null ? String.valueOf(order.getTotalPrice()) : "");
		paymentWS.setPostingDate(convertDateToXMLGregorianCalendar(order.getCreationtime()));
		//		setBalAccountType(paymentWebService, saccERPClientPaymentWebService); // set it after getting the model, model attribute.
		//		paymentWebService.setBalAccountNo(saccERPClientPaymentWebService.getBalAccountNo()); // set it after getting the model, model attribute.
		//		paymentWebService.setPost(saccERPClientPaymentWebService.getPost());
		//		paymentWebService.setOnlinePosted(saccERPClientPaymentWebService.getOnlinePosted());

		return paymentWS;
	}

	@Override
	public void createReturnRequest(final ReturnRequestModel returnRequest) throws SaccERPWebServiceException
	{
		Preconditions.checkArgument(returnRequest != null, "returnRequest must not be null");
		Preconditions.checkArgument(returnRequest.getOrder() != null, "returnRequest.order must not be null");
		Preconditions.checkArgument(CollectionUtils.isNotEmpty(returnRequest.getReturnEntries()),
				"returnRequest.returnEntries must not be null");
		ReturnWS obj = null;
		try
		{
			obj = getReturnWebService(returnRequest);
		}
		catch (final SaccERPWebServiceException e)
		{
			LOG.error("ERROR CREATING RETURN FOR ORDER " + returnRequest.getOrder().getCode() + " ON SACC ERP: " + e.getMessage(),
					e);
			LOG.error("Could not create ReturnWebService");
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.RETURN, returnRequest.getOrder().getCode(),
					returnRequest.getOrder().getVersionID(), extractConsignmentCode(returnRequest.getOrder()), obj.toString(),
					ExceptionUtils.getStackTrace(e), Boolean.FALSE);
			LOG.info("Record created for " + returnRequest.getOrder().getCode() + " result : " + Boolean.FALSE);
			publishErrorEmailEvent(returnRequest.getOrder(), e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
		}
		try
		{
			final ReturnWS createdReturn = returnWebService.create(obj);
			LOG.info("Return " + createdReturn.getKey() + " has been created successfully");
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.RETURN, returnRequest.getOrder().getCode(),
					returnRequest.getOrder().getVersionID(), extractConsignmentCode(returnRequest.getOrder()), obj.toString(),
					createdReturn.toString(), Boolean.TRUE);
			setAllRecordsOfThisOperationAsDone(returnRequest.getOrder(), ERPWebServiceType.RETURN);
			LOG.info("Record created for " + returnRequest.getOrder().getCode() + " successfully : " + Boolean.TRUE);
		}
		catch (final SaccERPWebServiceException ex)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(ex);
			LOG.error("ERROR CREATING RETURN FOR ORDER " + returnRequest.getOrder().getCode() + " ON SACC ERP: " + ex.getMessage(),
					ex);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.RETURN, returnRequest.getOrder().getCode(),
					returnRequest.getOrder().getVersionID(), extractConsignmentCode(returnRequest.getOrder()), obj.toString(),
					stackTrace, Boolean.FALSE);
			LOG.info("Record created for " + returnRequest.getOrder().getCode() + " successfully : " + Boolean.FALSE);
			publishErrorEmailEvent(returnRequest.getOrder(), ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage());
			throw ex;
		}
	}

	private ReturnWS updateReturnWebService(final ReturnWS obj, final ReturnRequestModel returnRequest)
			throws SaccERPWebServiceException
	{
		if (returnRequest.getOrder() == null || CollectionUtils.isEmpty(returnRequest.getReturnEntries()))
		{
			throw new SaccERPWebServiceException("Missing required data to create sacc erp return",
					new IllegalArgumentException("Order or return entries must not be null"));
		}
		if (StringUtils.isEmpty(returnRequest.getOrder().getSaccERPKey()))
		{
			throw new SaccERPWebServiceException("Missing SACC Sales Order ERP Key",
					new IllegalArgumentException("SaccERPKey must not be null"));
		}

		updateWSReturnLine(obj, returnRequest);

		return obj;
	}

	protected ReturnWS getReturnWebService(final ReturnRequestModel returnRequest) throws SaccERPWebServiceException
	{
		if (returnRequest.getOrder() == null || CollectionUtils.isEmpty(returnRequest.getReturnEntries()))
		{
			throw new SaccERPWebServiceException("Missing required data to create sacc erp return",
					new IllegalArgumentException("Order or return entries must not be null"));
		}
		if (StringUtils.isEmpty(returnRequest.getOrder().getSaccERPKey()))
		{
			throw new SaccERPWebServiceException("Missing SACC Sales Order ERP Key",
					new IllegalArgumentException("SaccERPKey must not be null"));
		}
		final CustomerModel customer = (CustomerModel) returnRequest.getOrder().getUser();
		final ReturnWS returnWebService = new ReturnWS();
		returnWebService.setNo(String.valueOf(System.currentTimeMillis()));
		returnWebService.setKey("");
		returnWebService.setExternalDocumentNo(returnRequest.getOrder().getSaccERPKey());
		returnWebService.setSellToCustomerNo(customer.getPk().getLongValueAsString());
		returnWebService.setOrderDate(convertDateToXMLGregorianCalendar(returnRequest.getOrder().getCreationtime()));


		setWSReturnLine(returnWebService, returnRequest);

		return returnWebService;
	}

	private void updateWSReturnLine(final ReturnWS returnWebService, final ReturnRequestModel returnRequest)
	{
		final List<ReturnEntryModel> entries = returnRequest.getReturnEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			for (final ReturnEntryModel entry : entries)
			{
				if (entry != null)
				{
					final Optional<WSReturnOrderLine> findAny = returnWebService.getWSReturnOrderLine().getWSReturnOrderLine().stream()
							.filter(line -> line.getBarcodeNo().equals(entry.getOrderEntry().getProduct().getCode())).findAny();

					if (findAny.isPresent())
					{
						final String quantity = findAny.get().getQuantity();
						final double totalQuantity = Double.parseDouble(quantity) + entry.getReceivedQuantity().intValue();
						findAny.get().setQuantity(String.valueOf(totalQuantity));
						final String lineAmount = findAny.get().getLineAmount();
						final double totalLineAmount = Double.parseDouble(lineAmount)
								+ (entry.getOrderEntry().getBasePrice().doubleValue() * entry.getReceivedQuantity().intValue());
						findAny.get().setLineAmount(String.valueOf(totalLineAmount));
						continue;
					}

					final WSReturnOrderLine wsReturnOrderLine = new WSReturnOrderLine();
					wsReturnOrderLine.setKey("");

					if (entry.getOrderEntry().getProduct() != null)
					{
						wsReturnOrderLine.setBarcodeNo(entry.getOrderEntry().getProduct().getCode());
					}
					if (entry.getReceivedQuantity() != null)
					{
						wsReturnOrderLine.setQuantity(String.valueOf(entry.getReceivedQuantity().intValue()));
					}
					if (entry.getOrderEntry().getBasePrice() != null)
					{
						wsReturnOrderLine.setUnitPrice(String.valueOf(entry.getOrderEntry().getBasePrice().doubleValue()));
					}
					final double lineTotal = entry.getOrderEntry().getBasePrice().doubleValue()
							* entry.getReceivedQuantity().intValue();
					wsReturnOrderLine.setLineAmount(String.valueOf(lineTotal));
					returnWebService.getWSReturnOrderLine().getWSReturnOrderLine().add(wsReturnOrderLine);
				}
			}
		}
	}

	protected void setWSReturnLine(final ReturnWS returnWebService, final ReturnRequestModel returnRequest)
	{
		final List<ReturnEntryModel> entries = returnRequest.getReturnEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			returnWebService.setWSReturnOrderLine(new WSReturnOrderLineList());
			for (final ReturnEntryModel entry : entries)
			{
				if (entry != null)
				{
					final WSReturnOrderLine wsReturnOrderLine = new WSReturnOrderLine();
					wsReturnOrderLine.setKey("");
					if (entry.getOrderEntry().getProduct() != null)
					{
						wsReturnOrderLine.setBarcodeNo(entry.getOrderEntry().getProduct().getCode());
					}
					if (entry.getExpectedQuantity() != null)
					{
						wsReturnOrderLine.setQuantity(String.valueOf(entry.getExpectedQuantity().intValue()));
					}
					if (entry.getOrderEntry().getBasePrice() != null)
					{
						wsReturnOrderLine.setUnitPrice(String.valueOf(entry.getOrderEntry().getBasePrice().doubleValue()));
					}
					final double lineTotal = entry.getOrderEntry().getBasePrice().doubleValue()
							* entry.getReceivedQuantity().intValue();
					wsReturnOrderLine.setLineAmount(String.valueOf(lineTotal));
					returnWebService.getWSReturnOrderLine().getWSReturnOrderLine().add(wsReturnOrderLine);
				}
			}
		}
	}

	@Override
	public boolean performFullyCancelSaleOrder(final OrderModel order)
	{
		Preconditions.checkArgument(order != null, "orderModel  is null");
		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = salesOrderWebServices.read(order.getSaccERPKey());
			if (salesOrder == null || org.apache.commons.lang3.StringUtils.isBlank(salesOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + order.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}
			if (salesOrder != null)
			{
				salesOrder.setOnlineStatus(OnlineStatus.FULLY_CANCEL);
				salesOrder.setWSSalesOrderLine(null);
				final SalesOrderWebServices update = salesOrderWebServices.update(salesOrder);
				saccERPOrderOperationService.saveRecord(ERPWebServiceType.FULLY_CANCEL, order.getCode(), order.getVersionID(),
						extractConsignmentCode(order), salesOrder.toString(), update.toString(), Boolean.TRUE);
				setAllRecordsOfThisOperationAsDone(order, ERPWebServiceType.FULLY_CANCEL);
				LOG.info("Fully Cancel Record created for " + order.getCode() + " successfully , Status(done) is :" + Boolean.TRUE);
				return true;
			}
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			LOG.error("Could not  preformFullyCancelSaleOrder");
			LOG.error("ERROR CREATING FULL CANCEL FOR ORDER " + order.getCode() + " ON SACC ERP: " + e.getMessage(), e);
			// do not forget to change the type ....
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.FULLY_CANCEL, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), salesOrder == null ? order.getSaccERPKey() : salesOrder.toString(), stackTrace,
					Boolean.FALSE);
			LOG.info("Fully Cancel Record created for " + order.getCode() + " successfully,Status(done) is : " + Boolean.FALSE);
			publishErrorEmailEvent(order, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}
		return false;

	}

	@Override
	public void performPartialCancelSaleOrderLine(final OrderModel order, final List<OrderCancelEntry> entries)
	{
		Preconditions.checkArgument((entries != null || !entries.isEmpty()), "Cancels entries  is null or empty");
		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = salesOrderWebServices.read(order.getSaccERPKey());
			if (salesOrder == null || org.apache.commons.lang3.StringUtils.isBlank(salesOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + order.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}
			final WSSalesOrderLineList wsSalesOrderLine = salesOrder.getWSSalesOrderLine();
			final List<WSSalesOrderLine> lines = wsSalesOrderLine.getWSSalesOrderLine();



			for (final OrderCancelEntry orderCancelEntry : entries)
			{
				final String code = orderCancelEntry.getOrderEntry().getProduct().getCode();

				for (final WSSalesOrderLine line : lines)
				{

					if (line.getBarcodeNo().equalsIgnoreCase(code))
					{
						final long cancelQuantity = orderCancelEntry.getCancelQuantity();
						final long quantity = Long.parseLong(line.getQuantity());
						line.setCancelQuantity(cancelQuantity + "");
						line.setQuantity((quantity - cancelQuantity) + "");

					}

				}


			}
			final SalesOrderWebServices update = salesOrderWebServices.update(salesOrder);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PARTIAL_CANCEL, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), salesOrder.toString(), update.toString(), Boolean.TRUE);
			setSyncPartialCancelFailed(order, false);
			LOG.info("Partial Cancel Record created for " + order.getCode() + " successfully , Status(done) is :" + Boolean.TRUE);
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			LOG.error("Could not  preformPartialCancelSaleOrderLine");
			LOG.error("ERROR CREATING PARTIAL CANCEL FOR ORDER " + order.getCode() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PARTIAL_CANCEL, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), salesOrder == null ? order.getSaccERPKey() : salesOrder.toString(), stackTrace,
					Boolean.FALSE);
			LOG.info("Partial Cancel Record created for " + order.getCode() + " successfully,Status(done) is : " + Boolean.FALSE);
			setSyncPartialCancelFailed(order, true);
			publishErrorEmailEvent(order, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
		}

	}

	private void setSyncPartialCancelFailed(final OrderModel order, final boolean failed)
	{
		modelService.refresh(order);
		order.setSyncPartialCancelFailed(failed);
		modelService.save(order);
	}

	@Override
	public boolean updateSaleOrderDeliveredStatus(final ConsignmentModel consignment)
	{
		OrderModel order = null;
		SalesOrderWebServices saleOrder = new SalesOrderWebServices();
		order = (OrderModel) consignment.getOrder();
		try
		{
			final List<SaccERPOrderOperationRecordModel> allOrderRecords = saccERPOrderOperationService.getAllOrderRecords(
					Arrays.asList(ERPWebServiceType.DELIVER_SALES_ORDER), order.getCode(), null, null, Boolean.TRUE);
			if (!CollectionUtils.isEmpty(allOrderRecords))
			{
				return false;
			}
			saleOrder = salesOrderWebServices.read(order.getSaccERPKey());
			if (saleOrder == null || org.apache.commons.lang3.StringUtils.isBlank(saleOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + order.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}
			saleOrder.setOnlineStatus(OnlineStatus.DELIVERED);
			final SalesOrderWebServices updateDeliveredSaleOrder = salesOrderWebServices.update(saleOrder);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.DELIVER_SALES_ORDER, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), saleOrder.toString(), updateDeliveredSaleOrder.toString(), Boolean.TRUE);
			setAllRecordsOfThisOperationAsDone(order, ERPWebServiceType.DELIVER_SALES_ORDER);
			LOG.info("[DefaultSaccERPService] Update Status TO (DELIVERED) Record created for " + order.getCode()
					+ " successfully , Status(done) is :" + Boolean.TRUE);
			return true;
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			LOG.error("[DefaultSaccERPService]Could not Update SalesOrderWebService");
			LOG.error("ERROR UPDATING TO DELIVERED FOR ORDER " + order.getCode() + " ON SACC ERP: " + e.getMessage(), e);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.DELIVER_SALES_ORDER, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), saleOrder == null ? order.getSaccERPKey() : saleOrder.toString(), stackTrace,
					Boolean.FALSE);
			LOG.error("[DefaultSaccERPService]Update Record created for " + order.getCode() + " successfully,Status(done) is : "
					+ Boolean.FALSE);
			publishErrorEmailEvent(order, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}
	}

	@Override
	public boolean performPartialSaleOrderLineCancellationByModificationRecord(final OrderModel order,
			final List<OrderEntryModificationRecordEntryModel> entries, final SaccERPOrderOperationRecordModel orderRecord)
	{
		Preconditions.checkArgument((entries != null || !entries.isEmpty()), "Cancels entries  is null or empty");
		SalesOrderWebServices salesOrder = null;
		try
		{
			salesOrder = salesOrderWebServices.read(order.getSaccERPKey());
			if (salesOrder == null || org.apache.commons.lang3.StringUtils.isBlank(salesOrder.getNo()))
			{
				throw new SaccERPWebServiceException("Sales order not found with No : " + order.getSaccERPKey(),
						SaccERPExeptionType.CONFIGURATION_NOT_FOUND);
			}
			final WSSalesOrderLineList wsSalesOrderLine = salesOrder.getWSSalesOrderLine();
			final List<WSSalesOrderLine> lines = wsSalesOrderLine.getWSSalesOrderLine();

			for (final OrderEntryModificationRecordEntryModel orderCancelEntry : entries)
			{
				final String code = orderCancelEntry.getOrderEntry().getProduct().getCode();

				for (final WSSalesOrderLine line : lines)
				{

					if (line.getBarcodeNo().equalsIgnoreCase(code))
					{
						final long cancelQuantity = orderCancelEntry.getOrderEntry().getQuantityCancelled();
						final long quantity = Long.parseLong(line.getQuantity());
						line.setCancelQuantity(cancelQuantity + "");
						line.setQuantity((quantity - cancelQuantity) + "");
					}
				}
			}
			final SalesOrderWebServices update = salesOrderWebServices.update(salesOrder);
			saccERPOrderOperationService.saveRecord(ERPWebServiceType.PARTIAL_CANCEL, order.getCode(), order.getVersionID(),
					extractConsignmentCode(order), salesOrder.toString(), update.toString(), Boolean.TRUE);
			LOG.info("Partial Cancel Record created for " + order.getCode() + " successfully , Status(done) is :" + Boolean.TRUE);
			setSyncPartialCancelFailed(order, false);
			return true;
		}
		catch (final SaccERPWebServiceException e)
		{
			final String stackTrace = ExceptionUtils.getStackTrace(e);
			LOG.error("Could not  performPartialCancelSaleOrderLine");
			LOG.error(e.getMessage(), e);
			saccERPOrderOperationService.updateRecord(orderRecord, salesOrder.toString(), stackTrace, false);
			LOG.info("Partial Cancel Record created for " + order.getCode() + " successfully,Status(done) is : " + Boolean.FALSE);
			publishErrorEmailEvent(order, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			setSyncPartialCancelFailed(order, true);
			return false;
		}
	}

	private void updateRecordAsDone(final SaccERPOrderOperationRecordModel orderRecord)
	{
		modelService.refresh(orderRecord);
		orderRecord.setDone(true);
		modelService.save(orderRecord);
	}

	@Override
	public boolean packSaleOrder(final OrderModel orderModel)
	{
		try
		{
			updateSalesOrderStatus(orderModel, ERPWebServiceType.PACK_SALES_ORDER);
			return true;
		}
		catch (final SaccERPWebServiceException e)
		{
			LOG.error("Could not packSaleOrder for order code : " + orderModel.getCode());
			publishErrorEmailEvent(orderModel, e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
			return false;
		}

	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}

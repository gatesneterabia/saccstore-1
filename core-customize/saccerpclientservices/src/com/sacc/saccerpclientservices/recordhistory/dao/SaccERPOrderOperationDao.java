/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.dao;

import java.util.List;

import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;




/**
 *
 */
public interface SaccERPOrderOperationDao
{

	public List<SaccERPOrderOperationRecordModel> getAllRecords();

	public List<SaccERPOrderOperationRecordModel> getAllRecordsByOrderCode(final List<ERPWebServiceType> webServiceTypes,
			final String orderCode, final String orderVersionId,
			final String consignmentCode, final Boolean done);

	public List<SaccERPOrderOperationRecordModel> getAllFialedRecords();
}

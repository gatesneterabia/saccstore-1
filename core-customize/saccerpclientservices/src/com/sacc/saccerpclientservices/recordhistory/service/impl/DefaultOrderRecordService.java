/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service.impl;

import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.sacc.saccerpclientservices.recordhistory.dao.OrderRecordDao;
import com.sacc.saccerpclientservices.recordhistory.service.OrderRecordService;


/**
 *
 */
public class DefaultOrderRecordService implements OrderRecordService
{

	@Resource(name = "orderRecordDao")
	private OrderRecordDao orderRecordDao;

	@Override
	public OrderModel getOrderByCode(final String orderCode)
	{
		Preconditions.checkArgument(orderCode != null, "Order code cannot be null");
		final OrderModel orderByCode = orderRecordDao.getOrderByCode(orderCode);
		return orderByCode;
	}

}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.recordhistory.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.dao.SaccERPOrderOperationDao;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;


/**
 *
 */
public class DefaultSaccERPOrderOperationService implements SaccERPOrderOperationService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultSaccERPOrderOperationService.class);

	private SaccERPOrderOperationDao saccERPOrderOperationDao;

	private ModelService modelService;

	@Override
	public void saveRecord(final ERPWebServiceType webServiceType, final String orderCode, final String orderVersionId,
			final String consignmentCode, final String requestData, final String responseData, final Boolean done)
	{
		Preconditions.checkArgument(webServiceType != null, "Web Service Type is null");
		Preconditions.checkArgument(orderCode != null, "order code is null");

		final List<SaccERPOrderOperationRecordModel> orderRecords = getAllOrderRecords(Arrays.asList(webServiceType), orderCode,
				null, null, done);

		if (!CollectionUtils.isEmpty(orderRecords))
		{
			orderRecords.forEach(r -> modelService.remove(r));
		}
		final SaccERPOrderOperationRecordModel operationRecord = modelService.create(SaccERPOrderOperationRecordModel.class);
		operationRecord.setWebServiceType(webServiceType);
		operationRecord.setOrderCode(orderCode);
		operationRecord.setOrderVersionID(orderVersionId);
		operationRecord.setConsignmentCode(consignmentCode);
		operationRecord.setRequestData(requestData);
		operationRecord.setResponseData(responseData);
		operationRecord.setDone(done);
		modelService.save(operationRecord);
		LOG.info("record {} is saved successfully", operationRecord);
	}

	@Override
	public void updateRecord(final SaccERPOrderOperationRecordModel operationRecord, final String requestData,
			final String responseData, final boolean done)
	{
		Preconditions.checkArgument(operationRecord != null, "operationRecord is null");
		operationRecord.setRequestData(requestData);
		operationRecord.setResponseData(responseData);
		operationRecord.setDone(done);
		modelService.save(operationRecord);
		LOG.info("record {} is saved successfully", operationRecord);
	}

	@Override
	public void saveRecord(final String orderCode, final String productBarcode, final String orderVersionId,
			final String consignmentCode, final String requestData, final String responseData, final Boolean done)
	{
		Preconditions.checkArgument(orderCode != null, "order code is null");
		Preconditions.checkArgument(productBarcode != null, "productBarcode is null");
		final SaccERPOrderOperationRecordModel operationRecord = modelService.create(SaccERPOrderOperationRecordModel.class);
		operationRecord.setWebServiceType(ERPWebServiceType.ITEM);
		operationRecord.setOrderCode(orderCode);
		operationRecord.setOrderVersionID(orderVersionId);
		operationRecord.setConsignmentCode(consignmentCode);
		operationRecord.setRequestData(requestData);
		operationRecord.setResponseData(responseData);
		operationRecord.setProductBarcode(productBarcode);
		operationRecord.setDone(done);
		modelService.save(operationRecord);
		LOG.info("record {} is saved successfully", operationRecord);
	}

	@Override
	public List<SaccERPOrderOperationRecordModel> getAllOrderRecords(final List<ERPWebServiceType> webServiceTypes,
			final String orderCode, final String orderVersionId, final String consignmentCode, final Boolean done)
	{
		LOG.info("List of order records by order code");
		return saccERPOrderOperationDao.getAllRecordsByOrderCode(webServiceTypes, orderCode, orderVersionId, consignmentCode, done);
	}



	@Override
	public List<SaccERPOrderOperationRecordModel> getAll()
	{
		LOG.info("List of all order records");
		return saccERPOrderOperationDao.getAllRecords();
	}


	/**
	 * @return the saccERPOrderOperationDao
	 */
	public SaccERPOrderOperationDao getSaccERPOrderOperationDao()
	{
		return saccERPOrderOperationDao;
	}

	/**
	 * @param saccERPOrderOperationDao
	 *           the saccERPOrderOperationDao to set
	 */
	public void setSaccERPOrderOperationDao(final SaccERPOrderOperationDao saccERPOrderOperationDao)
	{
		this.saccERPOrderOperationDao = saccERPOrderOperationDao;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	public List<SaccERPOrderOperationRecordModel> getAllFialedRecords()
	{
		return getSaccERPOrderOperationDao().getAllFialedRecords();
	}

}

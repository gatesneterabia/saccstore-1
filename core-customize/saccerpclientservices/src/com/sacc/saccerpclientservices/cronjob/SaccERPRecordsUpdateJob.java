/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.cronjob;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.service.SaccERPSalesOrderWebService;
import com.sacc.saccerpclientservices.model.SaccERPRecordsUpdateCronJobModel;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccerpclientservices.service.SaccERPService;
import com.sacc.saccfulfillment.service.CustomConsignmentService;


/**
 *
 */
public class SaccERPRecordsUpdateJob extends AbstractJobPerformable<SaccERPRecordsUpdateCronJobModel>
{

	private static final String CRONJOB_FINISHED = "UpdateFulfillmentStatusJob is Finished ...";
	protected static final Logger LOG = Logger.getLogger(SaccERPRecordsUpdateJob.class);
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	@Resource(name = "saccERPSalesOrderWebService")
	private SaccERPSalesOrderWebService salesOrderWebServices;

	@Override
	public PerformResult perform(final SaccERPRecordsUpdateCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");

		final List<ConsignmentModel> consignments = customConsignmentService
				.getConsignmentsByStatus(ConsignmentStatus.DELIVERY_COMPLETED);


		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		for (final ConsignmentModel consignmentModel : consignments)
		{
			saccERPService.updateSaleOrderDeliveredStatus(consignmentModel);
		}
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	private String extractConsignmentCode(final OrderModel order)
	{
		return order.getConsignments().stream().map(con -> con.getCode()).findFirst().get();
	}



}

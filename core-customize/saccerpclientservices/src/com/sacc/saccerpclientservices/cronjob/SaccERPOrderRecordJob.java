/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.cronjob;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordermodify.model.OrderEntryModificationRecordEntryModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPItemWebService;
import com.sacc.saccerpclientservices.enums.ERPWebServiceType;
import com.sacc.saccerpclientservices.model.SaccERPOperationRecordsCronJobModel;
import com.sacc.saccerpclientservices.model.SaccERPOrderOperationRecordModel;
import com.sacc.saccerpclientservices.recordhistory.service.OrderRecordService;
import com.sacc.saccerpclientservices.recordhistory.service.SaccERPOrderOperationService;
import com.sacc.saccerpclientservices.service.SaccERPService;


/**
 *
 */
public class SaccERPOrderRecordJob extends AbstractJobPerformable<SaccERPOperationRecordsCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(SaccERPOrderRecordJob.class);

	@Resource(name = "webServiceTypeOrder")
	private List<ERPWebServiceType> webServiceTypeOrder;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "orderRecordService")
	private OrderRecordService orderRecordService;

	@Resource(name = "saccERPOrderOperationService")
	private SaccERPOrderOperationService saccERPOrderOperationService;

	@Resource(name = "saccERPService")
	private SaccERPService saccERPService;

	@Resource(name = "saccERPItemWebService")
	private SaccERPItemWebService itemWebService;

	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Override
	public PerformResult perform(final SaccERPOperationRecordsCronJobModel cronJobModel)
	{
		final List<SaccERPOrderOperationRecordModel> allFailedRecords = saccERPOrderOperationService.getAllOrderRecords(null, null,
				null, null, Boolean.FALSE);
		LOG.info("Got {} failed records", allFailedRecords.size());

		webServiceTypeOrder.stream().forEach(type -> {
			allFailedRecords.stream().filter(record -> !record.isDuplicateRecord())
					.filter(record -> type.equals(record.getWebServiceType())).forEach(orderRecord -> {
						final OrderModel orderByCode = orderRecordService.getOrderByCode(orderRecord.getOrderCode());
						LOG.info("Record type : {}", orderRecord.getWebServiceType());
						boolean done = false;
						switch (orderRecord.getWebServiceType().getCode())
						{
							case "CREATE_CUSTOMER":
								done = saccERPService.createCustomer(orderByCode);
								break;
							case "CREATE_SALES_ORDER":
								done = saccERPService.createSalesOrder(orderByCode);
								break;
							case "PAYMENT":
								done = saccERPService.createPayment(orderByCode);
								break;
							case "PICK_SALES_ORDER":
								done = saccERPService.pickSaleOrder(orderByCode);
								break;
							case "PACK_SALES_ORDER":
								done = saccERPService.packSaleOrder(orderByCode);
								break;
							case "PARTIAL_CANCEL":
								try
								{
									done = partialCanelation(orderByCode, orderRecord);
								}
								catch (final Exception e)
								{
									LOG.error(e.getMessage(), e);
								}
								break;
							case "FULLY_CANCEL":
								done = saccERPService.performFullyCancelSaleOrder(orderByCode);
								break;
							case "SHIP_SALES_ORDER":
								break;
							case "DELIVER_SALES_ORDER":
								try
								{
									saccERPService.updateSalesOrderStatus(orderByCode, orderRecord.getWebServiceType());
									done = true;
								}
								catch (final SaccERPWebServiceException e1)
								{
									LOG.error("");
								}
								break;
							case "RETURN":
								break;
							case "ITEM":
								break;
							default:
								throw new IllegalArgumentException(orderRecord.getWebServiceType().getCode() + " Type Not supported");
						}
						if (done)
						{
							orderRecord.setResponseData("CREATED SUCCESSFULLY AFTER UPDATE");
							orderRecord.setDone(Boolean.TRUE);
							modelService.save(orderRecord);
						}
					});
		});
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private boolean checkPackSalesOrder(final OrderModel order) throws SaccERPWebServiceException
	{
		return saccERPService.packSaleOrder(order);
	}

	private boolean checkPickSalesOrder(final OrderModel order)
	{
		return saccERPService.pickSaleOrder(order);
	}

	private boolean partialCanelation(final OrderModel order, final SaccERPOrderOperationRecordModel orderRecord)
			throws OrderCancelException
	{
		final OrderCancelRecordModel cancelRecordEntry = orderCancelService.getCancelRecordForOrder(order);
		final List<OrderEntryModificationRecordEntryModel> canceledEntries = new ArrayList<OrderEntryModificationRecordEntryModel>();
		cancelRecordEntry.getModificationRecordEntries().stream().forEach(entry -> {
			entry.getOrderEntriesModificationEntries().forEach(e -> {
				canceledEntries.add(e);
			});
		});
		LOG.info("Got " + canceledEntries.size() + " cancelled items");
		canceledEntries.forEach(entry -> LOG.info(entry.getOrderEntry().getProduct().getCode() + " with "
				+ entry.getOrderEntry().getQuantityCancelled() + " quantity cancelled"));
		return saccERPService.performPartialSaleOrderLineCancellationByModificationRecord(order, canceledEntries, orderRecord);
	}

}

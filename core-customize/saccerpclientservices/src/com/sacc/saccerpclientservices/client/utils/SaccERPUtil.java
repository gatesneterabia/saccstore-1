package com.sacc.saccerpclientservices.client.utils;

import org.apache.commons.lang.StringUtils;


/**
 *
 * @author Baha Almtoor
 *
 */
public class SaccERPUtil
{
	private SaccERPUtil()
	{
	}

	public static boolean isNumeric(final String value)
	{
		try
		{
			Double.parseDouble(value);
		}
		catch (final NumberFormatException | NullPointerException ex)
		{
			return false;
		}

		return true;
	}

	public static String trim(final String value)
	{
		if (StringUtils.isNotBlank(value))
		{
			return value.trim();
		}

		return "";
	}
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.factory;

import com.sacc.saccerpclientservices.model.SaccERPClientCustomerWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientItemWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientPaymentWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientReturnWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientSalesOrderWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public class SaccERPModelFactory
{
	public static String getTypeCode(final Class<? extends SaccERPClientWebServiceModel> clazz)
	{
		if (SaccERPClientCustomerWebServiceModel.class.equals(clazz))
		{
			return SaccERPClientCustomerWebServiceModel._TYPECODE;
		}
		else if (SaccERPClientItemWebServiceModel.class.equals(clazz))
		{
			return SaccERPClientItemWebServiceModel._TYPECODE;
		}
		else if (SaccERPClientPaymentWebServiceModel.class.equals(clazz))
		{
			return SaccERPClientPaymentWebServiceModel._TYPECODE;
		}
		else if (SaccERPClientReturnWebServiceModel.class.equals(clazz))
		{
			return SaccERPClientReturnWebServiceModel._TYPECODE;
		}
		else if (SaccERPClientSalesOrderWebServiceModel.class.equals(clazz))
		{
			return SaccERPClientSalesOrderWebServiceModel._TYPECODE;
		}

		return null;
	}
}

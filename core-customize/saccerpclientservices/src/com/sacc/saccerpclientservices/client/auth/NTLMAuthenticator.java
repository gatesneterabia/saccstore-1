package com.sacc.saccerpclientservices.client.auth;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.apache.log4j.Logger;


/**
 *
 * @author Baha Almtoor
 *
 */
public class NTLMAuthenticator extends Authenticator
{
	private static final Logger LOG = Logger.getLogger(NTLMAuthenticator.class.getName());

	private final PasswordAuthentication passwordAuthentication;

	public NTLMAuthenticator(final String username, String password)
	{
		if (password == null)
		{
			password = "";
		}

		passwordAuthentication = new PasswordAuthentication(username, password.toCharArray());
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication()
	{
		if (getRequestingScheme().trim().equalsIgnoreCase("NTLM") || getRequestingScheme().trim().equalsIgnoreCase("digest"))
		{
			LOG.info("NTLM::ProxyAuthenticator::Providing authentication credentials");
			return passwordAuthentication;
		}
		else
		{
			LOG.info("NO::NTLM");
			return null;
		}
	}
}
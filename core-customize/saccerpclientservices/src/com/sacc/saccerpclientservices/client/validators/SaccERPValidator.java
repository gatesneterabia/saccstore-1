/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2020 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sacc.saccerpclientservices.client.validators;

import java.util.Collection;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPValidator
{
	default void validate(final Object obj, String msg, final SaccERPExeptionType type) throws SaccERPWebServiceException
	{
		if (obj == null)
		{
			msg = StringUtils.isNotBlank(msg) ? msg.trim() : "Invalid object";

			if (type != null)
			{
				throw new SaccERPWebServiceException(msg, type);
			}

			throw new SaccERPWebServiceException(msg);
		}
	}

	default void validate(final Object obj, final String msg) throws SaccERPWebServiceException
	{
		validate(obj, msg, null);
	}

	default void validate(final String value, String msg, final SaccERPExeptionType type) throws SaccERPWebServiceException
	{
		if (StringUtils.isBlank(value))
		{
			msg = StringUtils.isNotBlank(msg) ? msg.trim() : String.format("Invalid object where value=[%s]", value);

			if (type != null)
			{
				throw new SaccERPWebServiceException(msg, type);
			}

			throw new SaccERPWebServiceException(msg);
		}
	}

	default void validate(final String value, final String msg) throws SaccERPWebServiceException
	{
		validate(value, msg, null);
	}

	default void validate(final Collection collection, String msg, final SaccERPExeptionType type)
			throws SaccERPWebServiceException
	{
		if (CollectionUtils.isEmpty(collection))
		{
			msg = StringUtils.isNotBlank(msg) ? msg.trim() : "Invalid object";

			if (type != null)
			{
				throw new SaccERPWebServiceException(msg, type);
			}

			throw new SaccERPWebServiceException(msg);
		}
	}

	default void validate(final Collection collection, final String msg) throws SaccERPWebServiceException
	{
		validate(collection, msg, null);
	}

	default void validateOp(final Optional<? extends SaccERPClientWebServiceModel> optional, String msg,
			final SaccERPExeptionType type) throws SaccERPWebServiceException
	{
		if (optional == null || !optional.isPresent())
		{
			msg = StringUtils.isNotBlank(msg) ? msg.trim() : "Invalid object";

			if (type != null)
			{
				throw new SaccERPWebServiceException(msg, type);
			}

			throw new SaccERPWebServiceException(msg);
		}
	}

	default void validateOp(final Optional<? extends SaccERPClientWebServiceModel> optional, final String msg)
			throws SaccERPWebServiceException
	{
		validateOp(optional, msg, null);
	}

}

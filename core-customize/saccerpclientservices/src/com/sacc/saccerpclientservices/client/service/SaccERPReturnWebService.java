/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientReturnWebServiceModel;

import schemas.dynamics.microsoft.page.return_ws.ReturnWS;
import schemas.dynamics.microsoft.page.return_ws.ReturnWSFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPReturnWebService
{
	ReturnWS read(final String search) throws SaccERPWebServiceException;

	ReturnWS read(final String search, final String username, final String password) throws SaccERPWebServiceException;

	ReturnWS read(final String search, final String modelCode) throws SaccERPWebServiceException;

	ReturnWS read(final String search, final SaccERPClientReturnWebServiceModel model) throws SaccERPWebServiceException;

	ReturnWS update(ReturnWS t) throws SaccERPWebServiceException;

	ReturnWS update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	ReturnWS update(final ConsignmentModel consignment, final SaccERPClientReturnWebServiceModel model)
			throws SaccERPWebServiceException;

	ReturnWS create(final ReturnWS t) throws SaccERPWebServiceException;

	ReturnWS create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	ReturnWS create(final ConsignmentModel consignment, final SaccERPClientReturnWebServiceModel model)
			throws SaccERPWebServiceException;

	List<ReturnWS> readMultiple(int size, final List<ReturnWSFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException;

	List<ReturnWS> readMultiple(final String username, final String password, final int size, final List<ReturnWSFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;

	List<ReturnWS> readMultiple(final int size, final String modelCode, final List<ReturnWSFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;

	List<ReturnWS> readMultiple(final int size, final SaccERPClientReturnWebServiceModel model, final List<ReturnWSFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;
}

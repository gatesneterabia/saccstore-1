/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientSalesOrderWebServiceModel;

import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServicesFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPSalesOrderWebService
{
	SalesOrderWebServices read(final String search) throws SaccERPWebServiceException;

	SalesOrderWebServices read(final String search, final String username, final String password)
			throws SaccERPWebServiceException;

	SalesOrderWebServices read(final String search, final String modelCode) throws SaccERPWebServiceException;

	SalesOrderWebServices read(final String search, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException;

	SalesOrderWebServices update(SalesOrderWebServices t) throws SaccERPWebServiceException;

	SalesOrderWebServices update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	SalesOrderWebServices update(final ConsignmentModel consignment, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException;

	SalesOrderWebServices create(final SalesOrderWebServices t) throws SaccERPWebServiceException;

	SalesOrderWebServices create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	SalesOrderWebServices create(final ConsignmentModel consignment, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException;

	List<SalesOrderWebServices> readMultiple(int size, final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException;

	List<SalesOrderWebServices> readMultiple(final String username, final String password, final int size,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

	List<SalesOrderWebServices> readMultiple(final int size, final String modelCode,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

	List<SalesOrderWebServices> readMultiple(final int size, final SaccERPClientSalesOrderWebServiceModel model,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

}

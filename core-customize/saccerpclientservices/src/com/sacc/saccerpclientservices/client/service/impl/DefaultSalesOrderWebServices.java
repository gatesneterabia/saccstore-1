/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.DiscountValue;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.sacc.saccerpclientservices.client.enums.SaccERPExeptionType;
import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.client.service.SaccERPSalesOrderWebService;
import com.sacc.saccerpclientservices.model.SaccERPClientReturnWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientSalesOrderWebServiceModel;
import com.sacc.saccerpclientservices.model.SaccERPClientWebServiceModel;

import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServices;
import schemas.dynamics.microsoft.page.sales_order_web_services.SalesOrderWebServicesFilter;
import schemas.dynamics.microsoft.page.sales_order_web_services.WSSalesOrderLine;
import schemas.dynamics.microsoft.page.sales_order_web_services.WSSalesOrderLineList;


/**
 *
 * @author Baha Almtoor
 *
 */
public class DefaultSalesOrderWebServices extends DefaultWebService implements SaccERPSalesOrderWebService
{
	private static final Logger LOG = Logger.getLogger(DefaultSalesOrderWebServices.class);

	@Resource(name = "orderOnlineStatusMap")
	private Map<String, schemas.dynamics.microsoft.page.sales_order_web_services.OnlineStatus> orderOnlineStatusMap;

	protected void setWSSalesOrderLine(final SalesOrderWebServices salesOrderWebServices, final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			salesOrderWebServices.setWSSalesOrderLine(new WSSalesOrderLineList());
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry != null)
				{
					final WSSalesOrderLine wsSalesOrderLine = new WSSalesOrderLine();
					wsSalesOrderLine.setKey(entry.getOrder().getCode());
					if (entry.getProduct() != null)
					{
						wsSalesOrderLine.setBarcodeNo(entry.getProduct().getCode());
					}
					if (entry.getQuantity() != null)
					{
						wsSalesOrderLine.setQuantity(String.valueOf(entry.getQuantity()));
					}
					if (entry.getBasePrice() != null)
					{
						wsSalesOrderLine.setUnitPrice(String.valueOf(entry.getBasePrice()));
					}
					if (CollectionUtils.isNotEmpty(entry.getDiscountValues()))
					{
						wsSalesOrderLine.setLineDiscountAmount(
								String.valueOf(entry.getDiscountValues().stream().mapToDouble(DiscountValue::getValue).sum()));
					}

					wsSalesOrderLine.setLineAmount(String.valueOf(entry.getTotalPrice()));
					salesOrderWebServices.getWSSalesOrderLine().getWSSalesOrderLine().add(wsSalesOrderLine);
				}
			}
		}
	}

	protected Date getDeliveryDateGap(final SaccERPClientSalesOrderWebServiceModel saccERPClientSalesOrderWebService)
	{
		final Integer deliveryDateGap = saccERPClientSalesOrderWebService.getDeliveryDateGap();
		if (deliveryDateGap != null && deliveryDateGap >= 0)
		{
			final Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, deliveryDateGap);

			return calendar.getTime();
		}

		return null;
	}

	protected SalesOrderWebServices buildSalesOrderWebService(final String no, final String username, final String password)
			throws SaccERPWebServiceException
	{
		final SalesOrderWebServices salesOrderWebServices = new SalesOrderWebServices();
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
		if (model != null)
		{
			salesOrderWebServices.setTestEnv(model.isTestEnvironment());
		}
		salesOrderWebServices.setTimeout(getTimeout());
		salesOrderWebServices.setUsername(username);
		salesOrderWebServices.setPassword(password);
		salesOrderWebServices.setNo(no);

		return salesOrderWebServices;
	}

	protected SalesOrderWebServices buildSalesOrderWebService(final ConsignmentModel consignment,
			final SaccERPClientSalesOrderWebServiceModel saccERPClientSalesOrderWebService) throws SaccERPWebServiceException
	{
		validateUser(consignment.getOrder().getUser());
		final OrderModel order = (OrderModel) consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();
		final SalesOrderWebServices salesOrderWebServices = buildSalesOrderWebService(order.getCode(),
				saccERPClientSalesOrderWebService.getUserName(), saccERPClientSalesOrderWebService.getPassword());
		salesOrderWebServices.setExternalDocumentNo(order.getCode());
		salesOrderWebServices.setSellToCustomerNo(customer.getPk().getLongValueAsString());
		salesOrderWebServices.setOrderDate(convertDateToXMLGregorianCalendar(order.getCreationtime()));
		salesOrderWebServices.setOnlineStatus(orderOnlineStatusMap.get(consignment.getStatus().getCode()));
		setWSSalesOrderLine(salesOrderWebServices, order);

		return salesOrderWebServices;
	}

	protected void checkInstance(final SaccERPClientWebServiceModel model) throws SaccERPWebServiceException
	{
		if (!(model instanceof SaccERPClientSalesOrderWebServiceModel))
		{
			throw new SaccERPWebServiceException(
					String.format("Model not instance from[%]", SaccERPClientSalesOrderWebServiceModel._TYPECODE),
					SaccERPExeptionType.BAD_REQUEST);
		}
	}

	protected SaccERPClientSalesOrderWebServiceModel getModel(final String modelCode) throws SaccERPWebServiceException
	{
		validate(modelCode, String.format("Invalid modelCode[%s]", modelCode), SaccERPExeptionType.BAD_REQUEST);

		final Optional<SaccERPClientWebServiceModel> result = getSaccERPModelService().find(modelCode, true,
				SaccERPClientSalesOrderWebServiceModel.class);
		validateOp(result, String.format("[%s] not found where code[%s]", SaccERPClientReturnWebServiceModel._TYPECODE, modelCode),
				SaccERPExeptionType.CONFIGURATION_NOT_FOUND);

		final SaccERPClientWebServiceModel model = result.get();
		checkInstance(model);
		validateCredentials(model.getUserName(), model.getPassword());

		return (SaccERPClientSalesOrderWebServiceModel) model;
	}

	@Override
	public List<SalesOrderWebServices> readMultiple(final int size, final List<SalesOrderWebServicesFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService("", model.getUserName(), model.getPassword());

		return salesOrderWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<SalesOrderWebServices> readMultiple(final String username, final String password, final int size,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, "notReq");
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService("", username, password);

		return salesOrderWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<SalesOrderWebServices> readMultiple(final int size, final String modelCode,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		final SaccERPClientSalesOrderWebServiceModel model = getModel(modelCode);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService("", model.getUserName(), model.getPassword());

		return salesOrderWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public List<SalesOrderWebServices> readMultiple(final int size, final SaccERPClientSalesOrderWebServiceModel model,
			final List<SalesOrderWebServicesFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException
	{
		validate(model, String.format("Invalid [%s]", SaccERPClientSalesOrderWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		validateEssentialData(model.getUserName(), model.getPassword(), "notReq");
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService("", model.getUserName(), model.getPassword());

		return salesOrderWebService.readMultiple(filters, bookmarkKey, size);
	}

	@Override
	public SalesOrderWebServices read(final String no) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(no, model.getUserName(), model.getPassword());

		return salesOrderWebService.read();
	}

	@Override
	public SalesOrderWebServices read(final String no, final String username, final String password)
			throws SaccERPWebServiceException
	{
		validateEssentialData(username, password, no);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(no, username, password);

		return salesOrderWebService.read();
	}

	@Override
	public SalesOrderWebServices read(final String no, final String modelCode) throws SaccERPWebServiceException
	{
		validate(no, "Invalid number", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientSalesOrderWebServiceModel model = getModel(modelCode);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(no, model.getUserName(), model.getPassword());

		return salesOrderWebService.read();
	}

	@Override
	public SalesOrderWebServices read(final String no, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(no, String.format("Invalid number", no), SaccERPExeptionType.BAD_REQUEST);
		validate(model, String.format("Invalid [%s]", SaccERPClientSalesOrderWebServiceModel._TYPECODE),
				SaccERPExeptionType.BAD_REQUEST);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(no, model.getUserName(), model.getPassword());

		return salesOrderWebService.read();
	}

	protected SalesOrderWebServices executeUpdate(final SalesOrderWebServices saalesOrderWebServices)
			throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP update saalesOrderWebServices request[%s]", marshal(saalesOrderWebServices)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}

		return saalesOrderWebServices.update();
	}

	protected SalesOrderWebServices update(final SalesOrderWebServices salesOrderWebServices, SaccERPClientWebServiceModel model)
			throws SaccERPWebServiceException
	{
		if (model == null)
		{
			model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
			salesOrderWebServices.setUsername(model.getUserName());
			salesOrderWebServices.setPassword(model.getPassword());
			salesOrderWebServices.setTestEnv(model.isTestEnvironment());
		}
		salesOrderWebServices.setTimeout(getTimeout());
		if (StringUtils.isBlank(salesOrderWebServices.getKey()))
		{
			final SalesOrderWebServices getSalesOrderWebService = salesOrderWebServices.read();
			if (getSalesOrderWebService == null || StringUtils.isBlank(getSalesOrderWebService.getKey()))
			{
				throw new SaccERPWebServiceException(
						String.format("Invalid getSalesOrderWebServices where number=[%s]", getSalesOrderWebService.getNo()),
						SaccERPExeptionType.CLIENT_FORBIDDEN);
			}

			salesOrderWebServices.setKey(getSalesOrderWebService.getKey());
		}

		return executeUpdate(salesOrderWebServices);
	}

	@Override
	public SalesOrderWebServices update(final SalesOrderWebServices salesOrderWebService) throws SaccERPWebServiceException
	{
		validate(salesOrderWebService, "Invalid salesOrderWebService", SaccERPExeptionType.BAD_REQUEST);
		validate(salesOrderWebService.getNo(), "Invalid salesOrderWebService.no", SaccERPExeptionType.BAD_REQUEST);

		return update(salesOrderWebService, null);
	}

	@Override
	public SalesOrderWebServices update(final ConsignmentModel consignment, final String modelCode)
			throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SaccERPClientSalesOrderWebServiceModel model = getModel(modelCode);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(consignment, model);

		return update(salesOrderWebService, model);
	}

	@Override
	public SalesOrderWebServices update(final ConsignmentModel consignment, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(consignment, model);

		return update(salesOrderWebService, model);
	}

	protected SalesOrderWebServices executeCreate(final SalesOrderWebServices salesOrderWebService)
			throws SaccERPWebServiceException
	{
		try
		{
			LOG.info(String.format("Sacc ERP create salesOrderWebService request[%s]", marshal(salesOrderWebService)));
		}
		catch (final JAXBException ex)
		{
			ex.printStackTrace();
		}
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
		salesOrderWebService.setTestEnv(model.isTestEnvironment());
		salesOrderWebService.setTimeout(getTimeout());
		return salesOrderWebService.create();
	}

	@Override
	public SalesOrderWebServices create(final SalesOrderWebServices salesOrderWebService) throws SaccERPWebServiceException
	{
		validate(salesOrderWebService, "Invalid salesOrderWebService", SaccERPExeptionType.BAD_REQUEST);
		final SaccERPClientWebServiceModel model = findModel(SaccERPClientSalesOrderWebServiceModel.class);
		salesOrderWebService.setUsername(model.getUserName());
		salesOrderWebService.setPassword(model.getPassword());

		return executeCreate(salesOrderWebService);
	}

	@Override
	public SalesOrderWebServices create(final ConsignmentModel consignment, final String modelCode)
			throws SaccERPWebServiceException
	{
		validate(consignment, modelCode);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(consignment, getModel(modelCode));

		return executeCreate(salesOrderWebService);
	}

	@Override
	public SalesOrderWebServices create(final ConsignmentModel consignment, final SaccERPClientSalesOrderWebServiceModel model)
			throws SaccERPWebServiceException
	{
		validate(consignment, model);
		final SalesOrderWebServices salesOrderWebService = buildSalesOrderWebService(consignment, model);

		return executeCreate(salesOrderWebService);
	}
}
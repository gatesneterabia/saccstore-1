/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientItemWebServiceModel;

import schemas.dynamics.microsoft.page.item_web_service.ItemWebService;
import schemas.dynamics.microsoft.page.item_web_service.ItemWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPItemWebService
{
	ItemWebService read(final String search) throws SaccERPWebServiceException;

	ItemWebService read(final String search, final String username, final String password) throws SaccERPWebServiceException;

	ItemWebService read(final String search, final String modelCode) throws SaccERPWebServiceException;

	ItemWebService read(final String search, final SaccERPClientItemWebServiceModel model) throws SaccERPWebServiceException;

	ItemWebService update(ItemWebService t) throws SaccERPWebServiceException;

	ItemWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	ItemWebService update(final ConsignmentModel consignment, final SaccERPClientItemWebServiceModel model)
			throws SaccERPWebServiceException;

	ItemWebService create(final ItemWebService t) throws SaccERPWebServiceException;

	ItemWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	ItemWebService create(final ConsignmentModel consignment, final SaccERPClientItemWebServiceModel model)
			throws SaccERPWebServiceException;

	List<ItemWebService> readMultiple(int size, final String barcodeNo) throws SaccERPWebServiceException;

	List<ItemWebService> readMultiple(int size, final List<ItemWebServiceFilter> filters, final String bookmarkKey)
			throws SaccERPWebServiceException;

	List<ItemWebService> readMultiple(final String username, final String password, final int size,
			final List<ItemWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

	List<ItemWebService> readMultiple(final int size, final String modelCode, final List<ItemWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;

	List<ItemWebService> readMultiple(final int size, final SaccERPClientItemWebServiceModel model,
			final List<ItemWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;
}

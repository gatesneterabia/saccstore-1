/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccerpclientservices.client.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;

import com.sacc.saccerpclientservices.client.exeptions.SaccERPWebServiceException;
import com.sacc.saccerpclientservices.model.SaccERPClientCustomerWebServiceModel;

import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebService;
import schemas.dynamics.microsoft.page.customer_web_service.CustomerWebServiceFilter;


/**
 *
 * @author Baha Almtoor
 *
 */
public interface SaccERPCustomerWebService
{
	CustomerWebService read(final String search) throws SaccERPWebServiceException;

	CustomerWebService read(final String search, final String username, final String password) throws SaccERPWebServiceException;

	CustomerWebService read(final String search, final String modelCode) throws SaccERPWebServiceException;

	CustomerWebService read(final String search, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException;

	CustomerWebService update(CustomerWebService t) throws SaccERPWebServiceException;

	CustomerWebService update(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	CustomerWebService update(final ConsignmentModel consignment, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException;

	CustomerWebService create(final CustomerWebService t) throws SaccERPWebServiceException;

	CustomerWebService create(final ConsignmentModel consignment, final String modelCode) throws SaccERPWebServiceException;

	CustomerWebService create(final ConsignmentModel consignment, final SaccERPClientCustomerWebServiceModel model)
			throws SaccERPWebServiceException;

	List<CustomerWebService> readMultiple(int size, List<CustomerWebServiceFilter> filters, String bookmarkKey)
			throws SaccERPWebServiceException;

	List<CustomerWebService> readMultiple(final String username, final String password, final int size,
			final List<CustomerWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;

	List<CustomerWebService> readMultiple(final int size, final String modelCode, final List<CustomerWebServiceFilter> filters,
			final String bookmarkKey) throws SaccERPWebServiceException;

	List<CustomerWebService> readMultiple(final int size, final SaccERPClientCustomerWebServiceModel model,
			final List<CustomerWebServiceFilter> filters, final String bookmarkKey) throws SaccERPWebServiceException;
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.dao.impl;

import com.sacc.saccpayment.dao.PaymentProviderDao;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultHyperpayPaymentProviderDao.
 */
public class DefaultHyperpayPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultHyperpayPaymentProviderDao()
	{
		super(HyperpayPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return HyperpayPaymentProviderModel._TYPECODE;
	}

}

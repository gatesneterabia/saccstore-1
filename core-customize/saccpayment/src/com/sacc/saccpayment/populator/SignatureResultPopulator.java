/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;

import java.math.BigDecimal;
import java.util.Map;

import com.sacc.saccpayment.ccavenue.CcavenueStatusReason;


/**
 * @author amjad.shati@erabia.com
 */
public class SignatureResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final BigDecimal amount = new BigDecimal((Double) source.get(CcavenueStatusReason.ORDER_AMT.getKey()));

		final String currency = String.valueOf(source.get(CcavenueStatusReason.ORDER_CURRNCY.getKey()));
		final String merchantID = "";//TODO
		final String orderPageSerialNumber = null;
		final String sharedSecret = null;
		final String orderPageVersion = "1.1";
		final String amountPublicSignature = null;
		final String currencyPublicSignature = String.valueOf(source.get(CcavenueStatusReason.ORDER_CURRNCY.getKey()));
		final String transactionSignature = String.valueOf(source.get(CcavenueStatusReason.REFERENCE_NO.getKey()));
		final String signedFields = null;

		final SignatureData data = new SignatureData();

		data.setAmount(amount);
		data.setCurrency(currency);
		data.setMerchantID(merchantID);
		data.setOrderPageSerialNumber(orderPageSerialNumber);
		data.setSharedSecret(sharedSecret);
		data.setOrderPageVersion(orderPageVersion);
		data.setAmountPublicSignature(amountPublicSignature);
		data.setCurrencyPublicSignature(currencyPublicSignature);
		data.setTransactionSignature(transactionSignature);
		data.setSignedFields(signedFields);

		target.setSignatureData(data);
	}

}

package com.sacc.saccpayment.hyperpay.exception;

/**
 * 
 * @author monzer
 *
 */
public enum ExceptionType {
	
	BAD_REQUEST,
	INVALID_PARAMETER_FORMAT,
	SERVER_ERROR;
	
}

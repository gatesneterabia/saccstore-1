package com.sacc.saccpayment.hyperpay.service.impl;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.saccpayment.hyperpay.enums.HyperpayPaymentDataParameters;
import com.sacc.saccpayment.hyperpay.service.HyperpayPaymentHttpsConnectionService;

/**
 * The Class HyperpayPaymentHttpsConnectionServiceImpl.
 *
 * @author monzer
 */
public class DefaultHyperpayPaymentHttpsConnectionService implements HyperpayPaymentHttpsConnectionService{

	/** The Constant AUTH_PARAM. */
	private static final String AUTH_PARAM = "Authorization";

	/** The Constant ACCESS_TOKEN. */
	private static final String ACCESS_TOKEN_PREVIOUS = "Bearer ";

	/** The Constant EQUAL. */
	private static final Object EQUAL = "=";

	/** The Constant AMPERSAND. */
	private static final Object AMPERSAND = "&";

	private static final String CONTENT_TYPE_PARAM = "Content-Type";

	private static final String CONTECT_TYPE_VALUE = "application/x-www-form-urlencoded";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHyperpayPaymentPatternValidationService.class);

	/**
	 * Send Https request.
	 *
	 * @param body the body
	 * @param requestUrl the request url
	 * @param requestMethod the request method
	 * @return the input stream
	 * @throws MalformedURLException the malformed URL exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws IllegalAccessException the illegal access exception
	 */
	public InputStream sendRequest(final Object body, final String requestUrl, final String requestMethod,
			final String accessToken)
			throws MalformedURLException, IOException, IllegalAccessException {
		LOG.info("Preparing the request for {} {}", requestMethod, requestUrl);
		final URL url = new URL(requestUrl);
		final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod(requestMethod);
		connection.setRequestProperty(AUTH_PARAM, ACCESS_TOKEN_PREVIOUS + accessToken);
		connection.setRequestProperty(CONTENT_TYPE_PARAM, CONTECT_TYPE_VALUE);
		connection.setDoInput(true);
		connection.setDoOutput(true);

		if (body != null) {
			final String bodyString = buildRequestBody(body);
			LOG.info("Body : {}", bodyString);
			writeBodyOnStream(connection, bodyString);
		}
		LOG.info("Connection established");
		final int responseCode = connection.getResponseCode();
		LOG.info("HttpsConnectionUtil response Code : {}",responseCode);
		InputStream stream;
		if (responseCode >= 400) {
			stream = connection.getErrorStream();
		} else {
			stream = connection.getInputStream();
		}

		return stream;
	}

	/**
	 * Builds the request body.
	 *
	 * @param data the data
	 * @return the string
	 * @throws IllegalAccessException the illegal access exception
	 */
	private String buildRequestBody(final Object data)
			throws IllegalAccessException {
		LOG.info("Building body ...");
		final StringBuilder requestBodyBuilder = new StringBuilder();
		final Class<?> requestData = data.getClass();
		final Field[] fields = requestData.getDeclaredFields();
		for (final Field f : fields) {
			f.setAccessible(true);
			final String value = String.valueOf(f.get(data));
			if (StringUtils.isNotBlank(value) && !"null".equals(value))
			{
				buildRequestBody(requestBodyBuilder,
						HyperpayPaymentDataParameters.getDataParameterByName(f.getName()).getParamName(), value);
			}
		}
		return requestBodyBuilder.toString();
	}

	/**
	 * Builds the request body.
	 *
	 * @param builder the builder
	 * @param key the key
	 * @param value the value
	 */
	private void buildRequestBody(final StringBuilder builder, final String key, final String value) {
		builder.append(key).append(EQUAL).append(value).append(AMPERSAND);
	}

	/**
	 * Write body on stream.
	 *
	 * @param connection the connection
	 * @param body the body
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void writeBodyOnStream(final HttpsURLConnection connection, final String body) throws IOException {
		LOG.info("Writting body ...");
		final DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
		writer.writeBytes(body);
		writer.flush();
		writer.close();
	}

}

package com.sacc.saccpayment.hyperpay.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sacc.saccpayment.hyperpay.enums.HyperpayPaymentDataParameters;
import com.sacc.saccpayment.hyperpay.service.HyperpayPaymentPatternValidationService;

/**
 *
 * @author monzer
 *
 */
public class DefaultHyperpayPaymentPatternValidationService implements HyperpayPaymentPatternValidationService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultHyperpayPaymentPatternValidationService.class);

	/**
	 * Validate.
	 *
	 * @param param the param
	 * @param pattern the pattern
	 * @return true, if param matches pattern
	 */
	@Override
	public boolean validate(final String param, final HyperpayPaymentDataParameters hyperpayPaymentDataParameters)
	{

		if(param == null || param.isEmpty()) {
			throw new IllegalArgumentException("param that has been sent to validation service is null");
		}
		if (hyperpayPaymentDataParameters == null)
		{
			throw new IllegalArgumentException("pattern that has been sent to validation service is null");
		}
		final Pattern pat = Pattern.compile(hyperpayPaymentDataParameters.getParamPattern());
		final Matcher matcher = pat.matcher(param);
		final boolean isMatch = matcher.matches();
		LOG.info("[ {} ] is {} matches {} : {}", hyperpayPaymentDataParameters.getParamName(), param,
				hyperpayPaymentDataParameters.getParamPattern(), isMatch);
		return isMatch;
	}

}

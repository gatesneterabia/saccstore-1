package com.sacc.saccpayment.hyperpay.service;

import com.sacc.saccpayment.hyperpay.enums.HyperpayPaymentDataParameters;


/**
 * The Interface HyperpayPaymentPatternValidationService.
 *
 * @author monzer
 */
public interface HyperpayPaymentPatternValidationService {

	/**
	 * Validate.
	 *
	 * @param param the param
	 * @param pattern the pattern
	 * @return true, if param matches pattern
	 */
	boolean validate(String param, HyperpayPaymentDataParameters hyperpayPaymentDataParameters);

}

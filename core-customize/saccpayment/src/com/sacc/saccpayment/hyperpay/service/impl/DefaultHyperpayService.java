package com.sacc.saccpayment.hyperpay.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sacc.saccpayment.hyperpay.enums.HyperpayPaymentDataParameters;
import com.sacc.saccpayment.hyperpay.enums.PaymentType;
import com.sacc.saccpayment.hyperpay.enums.ResultPatterns;
import com.sacc.saccpayment.hyperpay.enums.TransactionStatus;
import com.sacc.saccpayment.hyperpay.exception.ExceptionType;
import com.sacc.saccpayment.hyperpay.exception.HyperpayPaymentException;
import com.sacc.saccpayment.hyperpay.model.HyperpayPaymentData;
import com.sacc.saccpayment.hyperpay.service.HyperpayPaymentHttpsConnectionService;
import com.sacc.saccpayment.hyperpay.service.HyperpayPaymentPatternValidationService;
import com.sacc.saccpayment.hyperpay.service.HyperpayService;


// TODO: Auto-generated Javadoc
/**
 * The Class DefaultHyperpayService.
 *
 * @author monzer
 */

public class DefaultHyperpayService implements HyperpayService {

	/** The Constant CURRENCY_CANNOT_BE_NULL_TO_PAYMENT_REFUND. */
	private static final String CURRENCY_CANNOT_BE_NULL_TO_PAYMENT_REFUND = "Currency cannot be null to Payment Refund";

	/** The Constant AMOUNT_CANNOT_BE_NULL_TO_PAYMENT_REFUND. */
	private static final String AMOUNT_CANNOT_BE_NULL_TO_PAYMENT_REFUND = "Amount cannot be null to Payment Refund";

	/** The Constant NO_PAYMET_DATA_PROVIDED. */
	private static final String NO_PAYMET_DATA_PROVIDED = "No Paymet Data provided";

	/** The Constant PAYMENT_ID_CANNOT_BE_NULL_TO_PAYMENT_REFUND. */
	private static final String PAYMENT_ID_CANNOT_BE_NULL_TO_PAYMENT_REFUND = "Payment id cannot be null to Payment Refund";

	/** The Constant CHECKOUT_ID_CAN_NOT_BE_NULL. */
	private static final String CHECKOUT_ID_CAN_NOT_BE_NULL = "Checkout Id can not be null";

	/** The Constant PAYMENT_ID_CANNOT_BE_NULL_INORDER_TO_PAYMENT_REVERSE. */
	private static final String PAYMENT_ID_CANNOT_BE_NULL_INORDER_TO_PAYMENT_REVERSE = "Payment id cannot be null inorder to Payment Reverse";

	/** The Constant COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE. */
	private static final String COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE = "Could not build the request body, could not access the attribute";

	/** The Constant INVALID_CONNECTION. */
	private static final String INVALID_CONNECTION = "Invalid connection";

	/** The Constant JSON_RESPONSE_COULD_NOT_BE_PARSED. */
	private static final String JSON_RESPONSE_COULD_NOT_BE_PARSED = "Json response could not be parsed";

	/** The Constant MALFORMED_URL_HAS_OCCURRED_INVALID_URL. */
	private static final String MALFORMED_URL_HAS_OCCURRED_INVALID_URL = "malformed URL has occurred, Invalid url";

	/** The Constant PAYMENT_TYPE_CANNOT_BE_NULL. */
	private static final String PAYMENT_TYPE_CANNOT_BE_NULL = "Payment type cannot be null";

	/** The Constant CURRENCY_CANNOT_BE_NULL. */
	private static final String CURRENCY_CANNOT_BE_NULL = "Currency cannot be null";

	/** The Constant AMOUNT_CANNOT_BE_NULL. */
	private static final String AMOUNT_CANNOT_BE_NULL = "Amount cannot be null";

	/** The Constant ENTITY_ID_CANNOT_BE_NULL. */
	private static final String ENTITY_ID_CANNOT_BE_NULL = "entity id cannot be null";

	/** The Constant AUTH_ACCESS_TOKEN_CANNOT_BE_NULL. */
	private static final String AUTH_ACCESS_TOKEN_CANNOT_BE_NULL = "Auth Access token cannot be null";

	/** The Constant NO_DATA_FOUND. */
	private static final String NO_DATA_FOUND = "no data found";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultHyperpayService.class);

	/** The Constant URL. */
	//	private static final String URL = "https://test.oppwa.com/v1/checkouts";

	/** The Constant EQUAL. */
	private static final String EQUAL = "=";

	/** The Constant ENTITY_ID_KEY. */
	private static final String ENTITY_ID_KEY = "entityId";

	/** The Constant POST_METHOD. */
	private static final String POST_METHOD = "POST";

	/** The Constant GET_METHOD. */
	private static final String GET_METHOD = "GET";

	/** The Constant ACCESS_TOKEN. */
	//	private static final String ACCESS_TOKEN = "Bearer OGFjN2E0Yzc3MmI3N2RkZjAxNzJiZDI2NGI5ODFlNWF8N2tnSzNCcG5Xbg==";

	/** The Constant ENTITY_ID_VALUE. */
	//	private static final String ENTITY_ID_VALUE = "8ac7a4c772b77ddf0172bd26cdb31e61";

	/** The Constant QUERY_URL. */
	//	private static final String QUERY_URL = "https://test.oppwa.com/v1/query/";

	/** The Constant SLASH. */
	private static final String SLASH = "/";

	/** The Constant REFUND_URL. */
	//	private static final String REFUND_URL = "https://test.oppwa.com/v1/payments/";

	/** The validation service. */
	@Autowired
	private HyperpayPaymentPatternValidationService validationService;

	/** The Https Connection Service. */
	@Autowired
	private HyperpayPaymentHttpsConnectionService connectionService;

	/**
	 * Prepare checkout.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param checkoutsURL
	 *           the checkouts URL
	 * @param data
	 *           the data
	 * @return the map
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	@Override
	public Map<String, Object> prepareCheckout(final String accessToken, final String entityId, final String checkoutsURL,
			final HyperpayPaymentData data) throws HyperpayPaymentException
	{
		LOG.info("DefaultHyperpayService : Request to Prepare the checkout {}", data);
		if(data == null) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, NO_DATA_FOUND);
		}
		if (StringUtils.isBlank(accessToken))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, AUTH_ACCESS_TOKEN_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(entityId))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(data.getAmount())) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, AMOUNT_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(data.getCurrency())) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, CURRENCY_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(data.getPaymentType().toString())) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, PAYMENT_TYPE_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(checkoutsURL))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, PAYMENT_TYPE_CANNOT_BE_NULL);
		}

		LOG.info("DefaultHyperpayService : data has been validated successfully");

		try {
			// Validate the Pattern, added under this try block because Reflection.IllegalAccessException is handled here
			validatePattern(data);
			LOG.info("DefaultHyperpayService : data has matched their pattern successfully");

			final String requestUrl = checkoutsURL;

			final InputStream result = connectionService.sendRequest(data, requestUrl, POST_METHOD, accessToken);
			return mapJsonToMap(result);

		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, MALFORMED_URL_HAS_OCCURRED_INVALID_URL);
		} catch(final JsonParseException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.INVALID_PARAMETER_FORMAT, JSON_RESPONSE_COULD_NOT_BE_PARSED);
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.SERVER_ERROR, INVALID_CONNECTION);
		} catch (final IllegalAccessException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE);
		}
	}

	/**
	 * Gets the checkout status.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param checkoutsURL
	 *           the checkouts URL
	 * @param id
	 *           the id
	 * @return the checkout status
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	@Override
	public Map<String, Object> getCheckoutStatus(final String accessToken, final String entityId, final String checkoutsURL,
			final String id)
			throws HyperpayPaymentException
	{
		LOG.info("DefaultHyperpayService : Request to get the checkout status {}", id);
		if(id == null || id.isEmpty()) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, CHECKOUT_ID_CAN_NOT_BE_NULL);
		}
		if (StringUtils.isBlank(accessToken))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(entityId))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(checkoutsURL))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}

		try {

			final String requestUrl = checkoutsURL + SLASH + id + "/payment?" + ENTITY_ID_KEY + EQUAL + entityId;
			final InputStream is = connectionService.sendRequest(null, requestUrl, GET_METHOD, accessToken);

			return mapJsonToMap(is);

		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, MALFORMED_URL_HAS_OCCURRED_INVALID_URL);
		} catch(final JsonParseException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.INVALID_PARAMETER_FORMAT, JSON_RESPONSE_COULD_NOT_BE_PARSED);
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.SERVER_ERROR, INVALID_CONNECTION);
		} catch (final IllegalAccessException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE);
		}
	}

	/**
	 * Search for transaction by id.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param queryURL
	 *           the query URL
	 * @param paymentId
	 *           the payment id
	 * @return the map
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	@Override
	public Map<String, Object> searchForTransactionById(final String accessToken, final String entityId, final String queryURL,
			final String paymentId) throws HyperpayPaymentException
	{
		LOG.info("DefaultHyperpayService : Request to search for the Transaction using payment id : {}", paymentId);

		if (StringUtils.isBlank(accessToken))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(entityId))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(queryURL))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if(StringUtils.isBlank(paymentId)) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, CHECKOUT_ID_CAN_NOT_BE_NULL);
		}

		try {

			final String requestUrl = queryURL + paymentId + "?" + ENTITY_ID_KEY + EQUAL + entityId;
			final InputStream is = connectionService.sendRequest(null, requestUrl, GET_METHOD, accessToken);

			return mapJsonToMap(is);

		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, MALFORMED_URL_HAS_OCCURRED_INVALID_URL);
		} catch(final JsonParseException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.INVALID_PARAMETER_FORMAT, JSON_RESPONSE_COULD_NOT_BE_PARSED);
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.SERVER_ERROR, INVALID_CONNECTION);
		}catch (final IllegalAccessException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE);
		}
	}

	/**
	 * Refund payment.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param paymentsURL
	 *           the payments URL
	 * @param data
	 *           the data
	 * @param paymentId
	 *           the payment id
	 * @return the map
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	@Override
	public Map<String, Object> refundPayment(final String accessToken, final String entityId, final String paymentsURL,
			final HyperpayPaymentData data, final String paymentId) throws HyperpayPaymentException
	{

		if (StringUtils.isBlank(accessToken))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(entityId))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(paymentsURL))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}

		if(StringUtils.isBlank(paymentId)) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, PAYMENT_ID_CANNOT_BE_NULL_TO_PAYMENT_REFUND);
		}
		if(data == null) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, NO_PAYMET_DATA_PROVIDED);
		}
		if(StringUtils.isBlank(data.getAmount())) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, AMOUNT_CANNOT_BE_NULL_TO_PAYMENT_REFUND);
		}
		if(StringUtils.isBlank(data.getCurrency())) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, CURRENCY_CANNOT_BE_NULL_TO_PAYMENT_REFUND);
		}
		//paymentType must be RF in-order to refund, regardless of the data entered by the user
		data.setPaymentType(PaymentType.RF.getPaymentCode());
		LOG.info("DefaultHyperpayService : data has been validated successfully");

		try {
			validatePattern(data);
			LOG.info("DefaultHyperpayService : data has matched their pattern successfully");

			final String requestUrl = paymentsURL + paymentId;
			final InputStream stream = connectionService.sendRequest(data, requestUrl, POST_METHOD, accessToken);
			return mapJsonToMap(stream);
		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, MALFORMED_URL_HAS_OCCURRED_INVALID_URL);
		} catch(final JsonParseException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.INVALID_PARAMETER_FORMAT, JSON_RESPONSE_COULD_NOT_BE_PARSED);
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.SERVER_ERROR, INVALID_CONNECTION);
		}catch (final IllegalAccessException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE);
		}
	}

	/**
	 * Reverse payment.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param refundURL
	 *           the refund URL
	 * @param data
	 *           the data
	 * @param paymentId
	 *           the payment id
	 * @return the map
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	@Override
	public Map<String, Object> reversePayment(final String accessToken, final String entityId, final String refundURL,
			final HyperpayPaymentData data, final String paymentId) throws HyperpayPaymentException
	{
		if(StringUtils.isBlank(paymentId)) {
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, PAYMENT_ID_CANNOT_BE_NULL_INORDER_TO_PAYMENT_REVERSE);
		}
		if (StringUtils.isBlank(accessToken))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(entityId))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		if (StringUtils.isBlank(refundURL))
		{
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, ENTITY_ID_CANNOT_BE_NULL);
		}
		//Payment type must be RV in-order to reverse, regardless of the data entered by the user
		data.setPaymentType(PaymentType.RV.getPaymentCode());

		LOG.info("DefaultHyperpayService : data has been validated successfully");

		try {
			validatePattern(data);
			LOG.info("DefaultHyperpayService : data has matched their pattern successfully");

			final String requestUrl = refundURL + paymentId;
			final InputStream stream = connectionService.sendRequest(data, requestUrl, POST_METHOD, accessToken);
			return mapJsonToMap(stream);
		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, MALFORMED_URL_HAS_OCCURRED_INVALID_URL);
		} catch(final JsonParseException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.INVALID_PARAMETER_FORMAT, JSON_RESPONSE_COULD_NOT_BE_PARSED);
		} catch (final IOException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.SERVER_ERROR, INVALID_CONNECTION);
		}catch (final IllegalAccessException e) {
			LOG.error(e.getMessage());
			throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, COULD_NOT_BUILD_THE_REQUEST_BODY_COULD_NOT_ACCESS_THE_ATTRIBUTE);
		}
	}

	/**
	 * Map json to map.
	 *
	 * @param stream the stream
	 * @return the map
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	protected Map<String, Object> mapJsonToMap(final InputStream stream) throws JsonMappingException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final Map<String, Object> object = mapper.readValue(stream, Map.class);
		LOG.info("DefaultHyperpayService : Mapping the response to Map : {}", object.toString());

		return object;
	}

	/**
	 * Validate pattern.
	 *
	 * @param data
	 *           the data
	 * @throws IllegalAccessException
	 *            the illegal access exception
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	protected void validatePattern(final HyperpayPaymentData data) throws IllegalAccessException, HyperpayPaymentException
	{

		LOG.info("DefaultHyperpayService : Validating each attribute with its pattern");
		final Class<?> requestData = data.getClass();
		final Field[] fields = requestData.getDeclaredFields();
		for(final Field f : fields) {
			f.setAccessible(true);
			final String value = String.valueOf(f.get(data));
			if (StringUtils.isNotBlank(value) && !value.equals(null) && !"null".equals(value))
			{
				validate(value, HyperpayPaymentDataParameters.getDataParameterByName(f.getName()));
			}
		}
	}

	/**
	 * Validate.
	 *
	 * @param param
	 *           the param
	 * @param data
	 *           the data
	 * @throws HyperpayPaymentException
	 *            the hyperpay payment exception
	 */
	protected void validate(final String param, final HyperpayPaymentDataParameters data) throws HyperpayPaymentException
	{
		if(StringUtils.isNotBlank(param)) {
			if (!validationService.validate(param, data))
			{
				throw new HyperpayPaymentException(ExceptionType.BAD_REQUEST, data.getParamName() + " " + param +
						" dose not match " + data.getParamPattern() + " Pattern");
			}
		}

	}

	/**
	 * Gets the transaction status from the response map.
	 *
	 * @param response
	 *           the response
	 * @return the transaction status (SUCCESS,FAILED).
	 */
	public TransactionStatus getTransactionStatus(final Map<String, Object> response)
	{
		if (CollectionUtils.isEmpty(response) || response.get("result") == null || !(response.get("result") instanceof Map)
				|| ((Map<String, Object>) response.get("result")).get("code") == null)
		{
			return TransactionStatus.FAILED;
		}
		final String code = String.valueOf(((Map<String, Object>) response.get("result")).get("code"));
		LOG.info("DefaultHyperpayService : transaction status is :" + getTransactionStatusGroupByCode(code));
		return getTransactionStatusGroupByCode(code) != ResultPatterns.FAILED ? TransactionStatus.SUCCESS
				: TransactionStatus.FAILED;
	}

	public TransactionStatus getTransactionStatus(final String accessToken, final String entityId, final String queryURL,
			final String paymentId)
	{
		Map<String, Object> response;
		try
		{
			response = searchForTransactionById(accessToken, entityId, queryURL, paymentId);

			if (CollectionUtils.isEmpty(response) || response.get("result") == null || !(response.get("result") instanceof Map)
					|| ((Map<String, Object>) response.get("result")).get("code") == null)
			{
				return TransactionStatus.FAILED;
			}
			final String code = String.valueOf(((Map<String, Object>) response.get("result")).get("code"));
			return getTransactionStatusGroupByCode(code) != ResultPatterns.FAILED ? TransactionStatus.SUCCESS
					: TransactionStatus.FAILED;
		}
		catch (final HyperpayPaymentException e)
		{
			return TransactionStatus.FAILED;
		}
	}

	/**
	 * Gets the transaction status group by code by checking the pattern of the code.
	 *
	 * @param code
	 *           the code
	 * @return the transaction status by code
	 */
	private ResultPatterns getTransactionStatusGroupByCode(final String code)
	{
		Pattern pattern;
		for (final ResultPatterns result : ResultPatterns.values())
		{
			pattern = Pattern.compile(result.getPattern());
			final Matcher matcher = pattern.matcher(code);
			if (matcher.find())
			{
				return result;
			}
		}
		return ResultPatterns.FAILED;
	}
}

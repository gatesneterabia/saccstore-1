package com.sacc.saccpayment.hyperpay.enums;

/**
 * 
 * @author monzer
 *
 */
public enum TransactionCategory {
	EC, MO, TO, RC, IN, PO, PM;
}

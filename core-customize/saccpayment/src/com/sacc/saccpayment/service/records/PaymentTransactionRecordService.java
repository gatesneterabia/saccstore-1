/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccpayment.service.records;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import com.sacc.saccpayment.model.PaymentTransactionRecordsModel;

/**
 *
 */
public interface PaymentTransactionRecordService
{
	void savePaymentRecords(final String action, final String paymentStatus, final String request, final String response,
			final String resultCode, final AbstractOrderModel abstractOrder);

	List<PaymentTransactionRecordsModel> getAllPaymentRecords();

	List<PaymentTransactionRecordsModel> getAllPaymentRecordsByFilter(PaymentTransactionRecordsModel model);
}

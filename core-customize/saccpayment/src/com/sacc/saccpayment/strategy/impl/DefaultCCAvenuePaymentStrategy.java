package com.sacc.saccpayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.ccavenue.entry.RequestData;
import com.sacc.saccpayment.ccavenue.enums.PaymentExceptionType;
import com.sacc.saccpayment.ccavenue.enums.TransactionStatus;
import com.sacc.saccpayment.ccavenue.exception.PaymentException;
import com.sacc.saccpayment.ccavenue.service.CCAvenueService;
import com.sacc.saccpayment.entry.PaymentRequestData;
import com.sacc.saccpayment.entry.PaymentResponseData;
import com.sacc.saccpayment.enums.PaymentResponseStatus;
import com.sacc.saccpayment.enums.TransactionOperation;
import com.sacc.saccpayment.model.CCAvenuePaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.strategy.CustomPaymentTransactionStrategy;
import com.sacc.saccpayment.strategy.PaymentStrategy;


/**
 * The Class DefaultCCAvenuePaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultCCAvenuePaymentStrategy.
 */
public class DefaultCCAvenuePaymentStrategy implements PaymentStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultCCAvenuePaymentStrategy.class);
	/** The Constant DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY. */
	private static final String DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY = "Delivery Address can not be empty";


	/** The Constant PAYMENT_ADDRESS_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_ADDRESS_CAN_NOT_BE_NULL = "Payment address can not be null";


	/** The Constant THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL. */
	private static final String THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL = "the provider moder is not a CCAvenuePaymentProviderModel ";


	/** The Constant PAYMENT_INFO_DATA_CAN_NOT_BE_NULL. */
	private static final String PAYMENT_INFO_DATA_CAN_NOT_BE_NULL = "paymentInfoData can not be null";

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	/** The payment provider service. */
	@Resource(name = "ccavenueService")
	private CCAvenueService ccavenueService;


	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "createSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/**
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}



	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";


	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";
	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data mustn't be null";
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;


	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 */
	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		if (StringUtils.isEmpty(abstractOrder.getOrderCode()))
		{
			abstractOrder.setOrderCode(generateOrderCode());
			modelService.save(abstractOrder);
		}
		final Optional<String> scriptSrc = ccavenueService.getScriptSrc(getRequestData(paymentProviderModel, abstractOrder));
		if (scriptSrc.isPresent())
		{
			modelService.refresh(abstractOrder);
			abstractOrder.setRequestPaymentBody(scriptSrc.get());
			modelService.save(abstractOrder);
			final PaymentRequestData paymentRequestData = new PaymentRequestData(scriptSrc.get(),
					CCAvenuePaymentProviderModel._TYPECODE);
			return Optional.ofNullable(paymentRequestData);

		}

		return Optional.empty();
	}

	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the optional
	 */
	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		final Optional<Map<String, Object>> responseData = ccavenueService.getResponseData((String) data,
				((CCAvenuePaymentProviderModel) paymentProviderModel).getWorkingKey());
		if (responseData.isPresent())
		{
			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(responseData.get().toString());
			final Object tracking_id = responseData.get().get("tracking_id");
			if (tracking_id != null && tracking_id instanceof String)
			{
				abstractOrder.setPaymentReferenceId((String) tracking_id);
			}
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(responseData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		return Optional.empty();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the order
	 * @return the response data
	 */
	protected RequestData getRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(paymentProviderModel instanceof CCAvenuePaymentProviderModel,
				THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL);
		//		Preconditions.checkArgument(order.getPaymentAddress() != null, PAYMENT_ADDRESS_CAN_NOT_BE_NULL);
		//		Preconditions.checkArgument(order.getDeliveryAddress() != null, DELIVERY_ADDRESS_CAN_NOT_BE_EMPTY);
		Preconditions.checkArgument(commonI18NService.getCurrentLanguage() != null, "CurrentLanguage is null");
		final CCAvenuePaymentProviderModel providerModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
		final AddressModel paymentAddress = abstractOrderModel.getPaymentAddress();
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		final RequestData requestData = new RequestData();
		final Double totalPrice = getTotalPrice(abstractOrderModel);

		final String contactEmail = ((de.hybris.platform.core.model.user.CustomerModel) abstractOrderModel.getUser())
				.getContactEmail();


		requestData.setAccessCode(providerModel.getAccessCode());
		requestData.setWorkingKey(providerModel.getWorkingKey());
		requestData.setMerchantId(providerModel.getMerchantId());
		requestData.setIntegrationType("iframe_normal");
		requestData.setOrderId(abstractOrderModel.getOrderCode());
		requestData.setAmount(totalPrice == null ? null : totalPrice.doubleValue() + "");
		requestData.setCustomerIdentifier(contactEmail);
		requestData.setCurrency(providerModel.getCurrency().getIsocode());
		requestData.setRedirectUrl(providerModel.getRedirectUrl());
		requestData.setCancelUrl(providerModel.getCancelUrl());
		requestData.setLanguage(commonI18NService.getCurrentLanguage() == null ? "EN"
				: commonI18NService.getCurrentLanguage().getIsocode().toUpperCase());
		if (paymentAddress != null)
		{
			requestData.setBillingName(paymentAddress.getFirstname() + " " + paymentAddress.getLastname());
			requestData.setBillingAddress(paymentAddress.getCountry().getName() + " - " + paymentAddress.getLine1());
			requestData
					.setBillingCity(paymentAddress.getCity() == null ? paymentAddress.getTown() : paymentAddress.getCity().getCode());
			requestData.setBillingState(getState(paymentAddress));
			requestData.setBillingZip("00000");
			requestData.setBillingCountry(paymentAddress.getCountry().getName());
			requestData.setBillingTel(paymentAddress.getMobile());
			requestData.setBillingEmail(contactEmail);
		}
		if (deliveryAddress != null)
		{
			requestData.setDeliveryName(deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname());
			requestData.setDeliveryAddress(deliveryAddress.getCountry().getName() + " - " + deliveryAddress.getLine1());
			requestData.setDeliveryCity(
					deliveryAddress.getCity() != null ? deliveryAddress.getCity().getName() : deliveryAddress.getTown());
			requestData.setDeliveryState(getState(deliveryAddress));
			requestData.setDeliveryZip("00000");
			requestData.setDeliveryCountry(deliveryAddress.getCountry().getName());
			requestData.setDeliveryTel(deliveryAddress.getMobile());
		}

		requestData.setMerchantParam1("additional Info.");
		requestData.setMerchantParam2("additional Info.");
		requestData.setMerchantParam3("additional Info.");
		requestData.setMerchantParam4("additional Info.");
		requestData.setPromoCode("");

		return requestData;
	}



	/**
	 *
	 */
	private String getState(final AddressModel addressModel)
	{
		if (addressModel == null)
		{
			return StringUtils.EMPTY;
		}
		return addressModel.getRegion() != null ? addressModel.getRegion().getName()
				: (addressModel.getCity() == null ? addressModel.getTown() : addressModel.getCity().getName());
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		CurrencyModel currency = null;
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			currency = cartCurrency;
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			currency = baseCurrency;
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDigits().intValue(),
					abstractOrderModel.getTotalPrice());
		}

		return amount;
	}



	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);

		final Optional<PaymentResponseData> buildPaymentResponseData = buildPaymentResponseData(paymentProviderModel,
				abstractOrderModel, data);
		if (!buildPaymentResponseData.isPresent())
		{
			return false;
		}
		final Map<String, Object> responseData = buildPaymentResponseData.get().getResponseData();

		return !(responseData == null || responseData.isEmpty() || !responseData.containsKey("order_status")
				|| !responseData.get("order_status").equals("Success"));
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
		final String referanceNo = (data == null ? abstractOrder.getPaymentReferenceId() : (String) data);

		final Optional<Map<String, Object>> responseData = ccavenueService.getOrderStatusData(
				ccavenuePaymentProviderModel.getWorkingKey(), ccavenuePaymentProviderModel.getAccessCode(), referanceNo,
				abstractOrder.getOrderCode());

		if (responseData.isPresent())
		{
			final PaymentResponseStatus paymentResponseStatus = getResponseStatus(responseData.get());

			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.REVIEW_NEEDED;

			final String requestPaymentBody = "WorkingKey=[" + ccavenuePaymentProviderModel.getWorkingKey() + "], AccessCode=["
					+ ccavenuePaymentProviderModel.getAccessCode() + "], referanceNo=[" + referanceNo + "], OrderCode=["
					+ abstractOrder.getOrderCode() + "]";
			final String responsePaymentBody = responseData.get().toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(responseData.get().toString());
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(responseData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{

			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.CAPTURE);

			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderConfiemData = ccavenueService.getOrderConfiemData(
					ccavenuePaymentProviderModel.getWorkingKey(), ccavenuePaymentProviderModel.getAccessCode(),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel) + "");

			final PaymentResponseStatus paymentResponseStatus = getCaptureResponseStatus(orderConfiemData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.CREDIT_FOR_VOIDED_CAPTURE;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.CAPTURE, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);


			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderConfiemData.toString());
			if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
			{
				abstractOrderModel.setPaymentStatus(PaymentStatus.PAID);
			}
			modelService.save(abstractOrderModel);

			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderConfiemData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}

		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{

			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.CANCEL);
			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderCancelData = ccavenueService.getOrderCancelData(
					ccavenuePaymentProviderModel.getWorkingKey(), ccavenuePaymentProviderModel.getAccessCode(),
					abstractOrderModel.getPaymentReferenceId(), getTotalPrice(abstractOrderModel) + "");

			final PaymentResponseStatus paymentResponseStatus = getCancelResponseStatus(orderCancelData.get());
			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.SUCCESFULL;
			final String requestPaymentBody = null;
			final String responsePaymentBody = responseData.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.CANCEL, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);








			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderCancelData.get().toString());
			if (PaymentResponseStatus.SUCCESS.equals(paymentResponseStatus))
			{
				abstractOrderModel.setPaymentStatus(PaymentStatus.NOTPAID);
			}

			modelService.save(abstractOrderModel);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderCancelData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}
		return Optional.empty();
	}

	/**
	 *
	 */
	private PaymentResponseStatus getResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}

	}

	/**
	 *
	 */
	private PaymentResponseStatus getCaptureResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}

	}





	/**
	 *
	 */
	private PaymentResponseStatus getCancelResponseStatus(final Map<String, Object> map)
	{
		try
		{
			final boolean containsErrorCode = map.containsKey("error_code");
			String errorCode = null;
			if (containsErrorCode)
			{
				errorCode = (String) map.get("error_code");
				if (!StringUtils.isBlank(errorCode))
				{
					return PaymentResponseStatus.FAILURE;
				}
			}
			return PaymentResponseStatus.SUCCESS;
		}
		catch (final Exception e)
		{
			return PaymentResponseStatus.FAILURE;
		}
	}

	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = getPaymentOrderStatusResponseData(paymentProviderModel,
				abstractOrderModel, abstractOrderModel.getPaymentReferenceId());
		if (paymentOrderStatusResponseData.isPresent())
		{
			// not sure about refund amount and how to fetch it or add it on the context as object
			final Map<String, Object> responseData = paymentOrderStatusResponseData.get().getResponseData();
			validateTransactionStatus(responseData, abstractOrderModel, TransactionOperation.REFUND_STANDALONE);
			final CCAvenuePaymentProviderModel ccavenuePaymentProviderModel = (CCAvenuePaymentProviderModel) paymentProviderModel;
			final Optional<Map<String, Object>> orderRefundData = ccavenueService.getOrderRefundData(
					ccavenuePaymentProviderModel.getWorkingKey(), ccavenuePaymentProviderModel.getAccessCode(),
					abstractOrderModel.getPaymentReferenceId(), 10 + "", "must not be constant,and it must be changed");

			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(orderRefundData.get().toString());
			modelService.save(abstractOrderModel);

			final PaymentResponseStatus paymentResponseStatus = getCaptureResponseStatus(orderRefundData.get());

			final PaymentResponseData paymentResponseData = new PaymentResponseData(orderRefundData.get(),
					CCAvenuePaymentProviderModel._TYPECODE, paymentResponseStatus);
			return Optional.ofNullable(paymentResponseData);

		}
		return Optional.empty();
	}

	private void validateTransactionStatus(final Map<String, Object> data, final AbstractOrderModel abstractOrderModel,
			final TransactionOperation operation) throws PaymentException
	{
		LOG.info("Validating transaction status .");
		Preconditions.checkArgument(!data.isEmpty(), "Response data map in empty");
		Preconditions.checkArgument(operation != null, "operation is null");
		final String orderStatus = (String) data.get("order_status");
		final TransactionStatus transactionStatus = getTransactionStatus(orderStatus);

		switch (operation)
		{
			case CAPTURE:


				//	paymentTransactionStrategy.savePaymentTransactionEntry(orderModel, requestId, paymentTransactionType, transactionStatus, transactionStatusDetails, requestPaymentBody, responsePaymentBody, entryCode)
				// TODO check is capture from CCAV -> true if Transactionentry (capture)? false cretae Hybris Transactionentry savePaymentTransactionEntry
			case CANCEL:
				//TODO check is CANCEL from CCAV -> true if Transactionentry (CANCEL)? false cretae Hybris Transactionentry savePaymentTransactionEntry
				if (TransactionStatus.CANCELED.equals(transactionStatus))
				{
					paymentTransactionStrategy.createCancelEntryIfNotExists(abstractOrderModel);
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage());
				}
				if (TransactionStatus.SHIPPED.equals(transactionStatus))
				{
					paymentTransactionStrategy.createCaptureEntryIfNotExists(abstractOrderModel);
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CAPTUARED.getMessage());
				}
				if (TransactionStatus.UNSUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage());
				}
				if (TransactionStatus.REFUNDED.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_REFUNDED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_REFUNDED.getMessage());
				}

				break;
			case REFUND_STANDALONE:
				if (TransactionStatus.UNSUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_NOT_AUTHORIZED.getMessage());
				}
				if (TransactionStatus.CANCELED.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage()));
					throw new PaymentException(PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.getMessage());
				}
				if (TransactionStatus.SUCCESSFUL.equals(transactionStatus))
				{
					LOG.error(String.format("Error when validating transaction status : %s",
							PaymentExceptionType.ORDER_IS_AUTHORIZED.getMessage()));
					throw new PaymentException(PaymentExceptionType.ORDER_IS_AUTHORIZED.getMessage());
				}

				break;
		}


	}

	/**
	 *
	 */
	private TransactionStatus getTransactionStatus(final String orderStatus)
	{
		if (StringUtils.isBlank(orderStatus))
		{
			return TransactionStatus.UNSUCCESSFUL;
		}
		try
		{
			return TransactionStatus.valueOf(orderStatus.toUpperCase());
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Order status [%s] is undefined.", orderStatus));
			return TransactionStatus.UNSUCCESSFUL;
		}

	}

	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		return false;
	}

}









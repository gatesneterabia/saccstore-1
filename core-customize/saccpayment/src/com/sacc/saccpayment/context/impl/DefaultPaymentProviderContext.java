package com.sacc.saccpayment.context.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccpayment.context.PaymentProviderContext;
import com.sacc.saccpayment.model.CCAvenuePaymentProviderModel;
import com.sacc.saccpayment.model.HyperpayPaymentProviderModel;
import com.sacc.saccpayment.model.PaymentProviderModel;
import com.sacc.saccpayment.strategy.PaymentProviderStrategy;


/**
 * @author mnasro
 *
 *         The Class DefaultPaymentProviderContext.
 */
public class DefaultPaymentProviderContext implements PaymentProviderContext
{

	/** The payment provider map. */
	@Resource(name = "paymentProviderMap")
	private Map<Class<?>, PaymentProviderStrategy> paymentProviderMap;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;



	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String BASESTORE__MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER__MUSTN_T_BE_NULL = "paymentProvider mustn't be null";



	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentProviderModel> getProvider(final Class<?> providerClass, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);

		final Optional<PaymentProviderStrategy> strategy = getStrategy(providerClass);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentProviderStrategy strategy = getPaymentProviderMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PROVIDER_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the payment provider map.
	 *
	 * @return the payment provider map
	 */
	protected Map<Class<?>, PaymentProviderStrategy> getPaymentProviderMap()
	{
		return paymentProviderMap;
	}

	@Override
	public Optional<PaymentProviderModel> getProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE__MUSTN_T_BE_NULL);

		if (StringUtils.isBlank(baseStoreModel.getPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getPaymentProvider().toUpperCase())
		{
			case "CCAVENUEPAYMENTPROVIDER":
				return getProvider(CCAvenuePaymentProviderModel.class, baseStoreModel);
			case "HYPERPAYPAYMENTPROVIDER":
				return getProvider(HyperpayPaymentProviderModel.class, baseStoreModel);
			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<PaymentProviderModel> getProviderByCurrentStore()
	{
		return getProvider(getBaseStoreService().getCurrentBaseStore());
	}

}

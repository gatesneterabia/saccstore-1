/**
 * GetRTLRetailsTimingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetRTLRetailsTimingResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getRTLRetailsTimingResult;

    public GetRTLRetailsTimingResponse() {
    }

    public GetRTLRetailsTimingResponse(
           com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getRTLRetailsTimingResult) {
           this.getRTLRetailsTimingResult = getRTLRetailsTimingResult;
    }


    /**
     * Gets the getRTLRetailsTimingResult value for this GetRTLRetailsTimingResponse.
     * 
     * @return getRTLRetailsTimingResult
     */
    public com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getGetRTLRetailsTimingResult() {
        return getRTLRetailsTimingResult;
    }


    /**
     * Sets the getRTLRetailsTimingResult value for this GetRTLRetailsTimingResponse.
     * 
     * @param getRTLRetailsTimingResult
     */
    public void setGetRTLRetailsTimingResult(com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getRTLRetailsTimingResult) {
        this.getRTLRetailsTimingResult = getRTLRetailsTimingResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRTLRetailsTimingResponse)) return false;
        GetRTLRetailsTimingResponse other = (GetRTLRetailsTimingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getRTLRetailsTimingResult==null && other.getGetRTLRetailsTimingResult()==null) || 
             (this.getRTLRetailsTimingResult!=null &&
              this.getRTLRetailsTimingResult.equals(other.getGetRTLRetailsTimingResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetRTLRetailsTimingResult() != null) {
            _hashCode += getGetRTLRetailsTimingResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRTLRetailsTimingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getRTLRetailsTimingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getRTLRetailsTimingResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getRTLRetailsTimingResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getRTLRetailsTimingResponse>getRTLRetailsTimingResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

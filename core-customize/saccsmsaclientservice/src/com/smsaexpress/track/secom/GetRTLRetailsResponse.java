/**
 * GetRTLRetailsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetRTLRetailsResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getRTLRetailsResult;

    public GetRTLRetailsResponse() {
    }

    public GetRTLRetailsResponse(
           com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getRTLRetailsResult) {
           this.getRTLRetailsResult = getRTLRetailsResult;
    }


    /**
     * Gets the getRTLRetailsResult value for this GetRTLRetailsResponse.
     * 
     * @return getRTLRetailsResult
     */
    public com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getGetRTLRetailsResult() {
        return getRTLRetailsResult;
    }


    /**
     * Sets the getRTLRetailsResult value for this GetRTLRetailsResponse.
     * 
     * @param getRTLRetailsResult
     */
    public void setGetRTLRetailsResult(com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getRTLRetailsResult) {
        this.getRTLRetailsResult = getRTLRetailsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRTLRetailsResponse)) return false;
        GetRTLRetailsResponse other = (GetRTLRetailsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getRTLRetailsResult==null && other.getGetRTLRetailsResult()==null) || 
             (this.getRTLRetailsResult!=null &&
              this.getRTLRetailsResult.equals(other.getGetRTLRetailsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetRTLRetailsResult() != null) {
            _hashCode += getGetRTLRetailsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRTLRetailsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getRTLRetailsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getRTLRetailsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getRTLRetailsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getRTLRetailsResponse>getRTLRetailsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

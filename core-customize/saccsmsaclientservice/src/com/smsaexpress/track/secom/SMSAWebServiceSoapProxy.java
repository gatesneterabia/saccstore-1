package com.smsaexpress.track.secom;

public class SMSAWebServiceSoapProxy implements com.smsaexpress.track.secom.SMSAWebServiceSoap {
  private String _endpoint = null;
  private com.smsaexpress.track.secom.SMSAWebServiceSoap sMSAWebServiceSoap = null;
  
  public SMSAWebServiceSoapProxy() {
    _initSMSAWebServiceSoapProxy();
  }
  
  public SMSAWebServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSMSAWebServiceSoapProxy();
  }
  
  private void _initSMSAWebServiceSoapProxy() {
    try {
      sMSAWebServiceSoap = (new com.smsaexpress.track.secom.SMSAWebServiceLocator()).getSMSAWebServiceSoap();
      if (sMSAWebServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sMSAWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sMSAWebServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sMSAWebServiceSoap != null)
      ((javax.xml.rpc.Stub)sMSAWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.smsaexpress.track.secom.SMSAWebServiceSoap getSMSAWebServiceSoap() {
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap;
  }
  
  public java.lang.String addShipment(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.addShipment(passKey, refNo, sentDate, idNo, cName, cntry, cCity, cZip, cPOBox, cMobile, cTel1, cTel2, cAddr1, cAddr2, shipType, PCs, cEmail, carrValue, carrCurr, codAmt, weight, custVal, custCurr, insrAmt, insrCurr, itemDesc);
  }
  
  public java.lang.String addShip(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String sName, java.lang.String sContact, java.lang.String sAddr1, java.lang.String sAddr2, java.lang.String sCity, java.lang.String sPhone, java.lang.String sCntry, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.addShip(passKey, refNo, sentDate, idNo, cName, cntry, cCity, cZip, cPOBox, cMobile, cTel1, cTel2, cAddr1, cAddr2, shipType, PCs, cEmail, carrValue, carrCurr, codAmt, weight, custVal, custCurr, insrAmt, insrCurr, itemDesc, sName, sContact, sAddr1, sAddr2, sCity, sPhone, sCntry, prefDelvDate, gpsPoints);
  }
  
  public java.lang.String addShipMPS(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String sName, java.lang.String sContact, java.lang.String sAddr1, java.lang.String sAddr2, java.lang.String sCity, java.lang.String sPhone, java.lang.String sCntry, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.addShipMPS(passKey, refNo, sentDate, idNo, cName, cntry, cCity, cZip, cPOBox, cMobile, cTel1, cTel2, cAddr1, cAddr2, shipType, PCs, cEmail, carrValue, carrCurr, codAmt, weight, custVal, custCurr, insrAmt, insrCurr, itemDesc, sName, sContact, sAddr1, sAddr2, sCity, sPhone, sCntry, prefDelvDate, gpsPoints);
  }
  
  public java.lang.String stoShipment(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.stoShipment(awbNo, passkey);
  }
  
  public java.lang.String addShipmentDelv(java.lang.String passKey, java.lang.String refNo, java.lang.String sentDate, java.lang.String idNo, java.lang.String cName, java.lang.String cntry, java.lang.String cCity, java.lang.String cZip, java.lang.String cPOBox, java.lang.String cMobile, java.lang.String cTel1, java.lang.String cTel2, java.lang.String cAddr1, java.lang.String cAddr2, java.lang.String shipType, int PCs, java.lang.String cEmail, java.lang.String carrValue, java.lang.String carrCurr, java.lang.String codAmt, java.lang.String weight, java.lang.String custVal, java.lang.String custCurr, java.lang.String insrAmt, java.lang.String insrCurr, java.lang.String itemDesc, java.lang.String prefDelvDate, java.lang.String gpsPoints) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.addShipmentDelv(passKey, refNo, sentDate, idNo, cName, cntry, cCity, cZip, cPOBox, cMobile, cTel1, cTel2, cAddr1, cAddr2, shipType, PCs, cEmail, carrValue, carrCurr, codAmt, weight, custVal, custCurr, insrAmt, insrCurr, itemDesc, prefDelvDate, gpsPoints);
  }
  
  public com.smsaexpress.track.secom.GetTrackingResponseGetTrackingResult getTracking(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getTracking(awbNo, passkey);
  }
  
  public com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getTrackingwithRef(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getTrackingwithRef(awbNo, passkey);
  }
  
  public java.lang.String getStatus(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getStatus(awbNo, passkey);
  }
  
  public java.lang.String saphOrderReady(java.lang.String passKey, java.lang.String refId, com.smsaexpress.track.secom.OrderLineItem[] orderLineItems) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.saphOrderReady(passKey, refId, orderLineItems);
  }
  
  public java.lang.String getStatusByRef(java.lang.String refNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getStatusByRef(refNo, passkey);
  }
  
  public com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getTrackingByRef(java.lang.String refNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getTrackingByRef(refNo, passkey);
  }
  
  public com.smsaexpress.track.secom.GetShipUpdatesResponseGetShipUpdatesResult getShipUpdates(int rowId, java.lang.String passKey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getShipUpdates(rowId, passKey);
  }
  
  public com.smsaexpress.track.secom.GetTrackResp getTrack(java.lang.String awbNo, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getTrack(awbNo, passkey);
  }
  
  public com.smsaexpress.track.secom.GetShipmentUpdatesResp getShipmentUpdates(int rowId, java.lang.String passKey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getShipmentUpdates(rowId, passKey);
  }
  
  public java.lang.String cancelShipment(java.lang.String awbNo, java.lang.String passkey, java.lang.String reas) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.cancelShipment(awbNo, passkey, reas);
  }
  
  public com.smsaexpress.track.secom.GetRTLCitiesResponseGetRTLCitiesResult getRTLCities(java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getRTLCities(passkey);
  }
  
  public com.smsaexpress.track.secom.GetRTLRetailsResponseGetRTLRetailsResult getRTLRetails(java.lang.String cityCode, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getRTLRetails(cityCode, passkey);
  }
  
  public com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getAllRetails(java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getAllRetails(passkey);
  }
  
  public com.smsaexpress.track.secom.GetRTLRetailsTimingResponseGetRTLRetailsTimingResult getRTLRetailsTiming(java.lang.String cityCode, java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getRTLRetailsTiming(cityCode, passkey);
  }
  
  public com.smsaexpress.track.secom.GetAllRetailsTimingResponseGetAllRetailsTimingResult getAllRetailsTiming(java.lang.String passkey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getAllRetailsTiming(passkey);
  }
  
  public byte[] getPDF(java.lang.String awbNo, java.lang.String passKey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getPDF(awbNo, passKey);
  }
  
  public byte[] getPDFSino(java.lang.String awbNo, java.lang.String passKey) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getPDFSino(awbNo, passKey);
  }
  
  public byte[] getPDFBr(java.lang.String awbNo, java.lang.String passKey, java.lang.String forwrdr) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getPDFBr(awbNo, passKey, forwrdr);
  }
  
  public com.smsaexpress.track.secom.DeliveredShipmentResponse getDeliveredShipments(java.lang.String passKey, java.lang.String fromDate, java.lang.String toDate) throws java.rmi.RemoteException{
    if (sMSAWebServiceSoap == null)
      _initSMSAWebServiceSoapProxy();
    return sMSAWebServiceSoap.getDeliveredShipments(passKey, fromDate, toDate);
  }
  
  
}
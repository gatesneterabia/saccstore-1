/**
 * GetAllRetailsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetAllRetailsResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getAllRetailsResult;

    public GetAllRetailsResponse() {
    }

    public GetAllRetailsResponse(
           com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getAllRetailsResult) {
           this.getAllRetailsResult = getAllRetailsResult;
    }


    /**
     * Gets the getAllRetailsResult value for this GetAllRetailsResponse.
     * 
     * @return getAllRetailsResult
     */
    public com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getGetAllRetailsResult() {
        return getAllRetailsResult;
    }


    /**
     * Sets the getAllRetailsResult value for this GetAllRetailsResponse.
     * 
     * @param getAllRetailsResult
     */
    public void setGetAllRetailsResult(com.smsaexpress.track.secom.GetAllRetailsResponseGetAllRetailsResult getAllRetailsResult) {
        this.getAllRetailsResult = getAllRetailsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAllRetailsResponse)) return false;
        GetAllRetailsResponse other = (GetAllRetailsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getAllRetailsResult==null && other.getGetAllRetailsResult()==null) || 
             (this.getAllRetailsResult!=null &&
              this.getAllRetailsResult.equals(other.getGetAllRetailsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetAllRetailsResult() != null) {
            _hashCode += getGetAllRetailsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAllRetailsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getAllRetailsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getAllRetailsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getAllRetailsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getAllRetailsResponse>getAllRetailsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * GetTrackingwithRefResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetTrackingwithRefResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getTrackingwithRefResult;

    public GetTrackingwithRefResponse() {
    }

    public GetTrackingwithRefResponse(
           com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getTrackingwithRefResult) {
           this.getTrackingwithRefResult = getTrackingwithRefResult;
    }


    /**
     * Gets the getTrackingwithRefResult value for this GetTrackingwithRefResponse.
     * 
     * @return getTrackingwithRefResult
     */
    public com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getGetTrackingwithRefResult() {
        return getTrackingwithRefResult;
    }


    /**
     * Sets the getTrackingwithRefResult value for this GetTrackingwithRefResponse.
     * 
     * @param getTrackingwithRefResult
     */
    public void setGetTrackingwithRefResult(com.smsaexpress.track.secom.GetTrackingwithRefResponseGetTrackingwithRefResult getTrackingwithRefResult) {
        this.getTrackingwithRefResult = getTrackingwithRefResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTrackingwithRefResponse)) return false;
        GetTrackingwithRefResponse other = (GetTrackingwithRefResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTrackingwithRefResult==null && other.getGetTrackingwithRefResult()==null) || 
             (this.getTrackingwithRefResult!=null &&
              this.getTrackingwithRefResult.equals(other.getGetTrackingwithRefResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTrackingwithRefResult() != null) {
            _hashCode += getGetTrackingwithRefResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTrackingwithRefResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getTrackingwithRefResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTrackingwithRefResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getTrackingwithRefResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getTrackingwithRefResponse>getTrackingwithRefResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

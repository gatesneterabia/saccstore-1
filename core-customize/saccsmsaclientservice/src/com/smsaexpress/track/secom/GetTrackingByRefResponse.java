/**
 * GetTrackingByRefResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.smsaexpress.track.secom;

public class GetTrackingByRefResponse  implements java.io.Serializable {
    private com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getTrackingByRefResult;

    public GetTrackingByRefResponse() {
    }

    public GetTrackingByRefResponse(
           com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getTrackingByRefResult) {
           this.getTrackingByRefResult = getTrackingByRefResult;
    }


    /**
     * Gets the getTrackingByRefResult value for this GetTrackingByRefResponse.
     * 
     * @return getTrackingByRefResult
     */
    public com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getGetTrackingByRefResult() {
        return getTrackingByRefResult;
    }


    /**
     * Sets the getTrackingByRefResult value for this GetTrackingByRefResponse.
     * 
     * @param getTrackingByRefResult
     */
    public void setGetTrackingByRefResult(com.smsaexpress.track.secom.GetTrackingByRefResponseGetTrackingByRefResult getTrackingByRefResult) {
        this.getTrackingByRefResult = getTrackingByRefResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTrackingByRefResponse)) return false;
        GetTrackingByRefResponse other = (GetTrackingByRefResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTrackingByRefResult==null && other.getGetTrackingByRefResult()==null) || 
             (this.getTrackingByRefResult!=null &&
              this.getTrackingByRefResult.equals(other.getGetTrackingByRefResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTrackingByRefResult() != null) {
            _hashCode += getGetTrackingByRefResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTrackingByRefResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">getTrackingByRefResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTrackingByRefResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", "getTrackingByRefResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://track.smsaexpress.com/secom/", ">>getTrackingByRefResponse>getTrackingByRefResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionClassificationPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;
	@Resource(name = "variantOptionFeatureListPopulator")
	private Populator<FeatureList, VariantOptionData> variantOptionFeatureListPopulator;

	@Override
	public void populate(final VariantProductModel productModel, final VariantOptionData productData)
	{
		final FeatureList featureList = classificationService.getFeatures(productModel);
		if (featureList != null && !featureList.getFeatures().isEmpty())
		{
			variantOptionFeatureListPopulator.populate(featureList, productData);
		}
	}
}

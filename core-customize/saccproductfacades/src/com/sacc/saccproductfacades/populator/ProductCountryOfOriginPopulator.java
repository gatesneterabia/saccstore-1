package com.sacc.saccproductfacades.populator;


import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ProductCountryOfOriginPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String COUNTRY_OF_ORIGIN = "SkysalesClassification/1.0/SkySalesClassifications.countryoforigin";
	private static final String COUNTRY_OF_ORIGIN_ISOCODE = "SkysalesClassification/1.0/SkySalesClassifications.countryoforiginisocode";

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source != null)
		{
			populateClassification(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateClassification(final ProductModel source, final ProductData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature countryOfOrigin = features.getFeatureByCode(COUNTRY_OF_ORIGIN);
			if (countryOfOrigin != null && countryOfOrigin.getValue() != null)
			{
				target.setCountryOfOrigin(countryOfOrigin.getValue().getValue().toString());
			}
			final Feature countryOfOriginIsocode = features.getFeatureByCode(COUNTRY_OF_ORIGIN_ISOCODE);
			if (countryOfOriginIsocode != null && countryOfOriginIsocode.getValue() != null)
			{
				target.setCountryOfOriginIsocode(countryOfOriginIsocode.getValue().getValue().toString());
			}
		}
	}
}
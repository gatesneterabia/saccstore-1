/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class VariantOptionLabelPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	private static final String PRODUCT_LABEL = "Label";

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{
		if (source != null)
		{
			populateClassification(source, target);
		}
	}

	/**
	 * @param source
	 *
	 * @param target
	 */
	private void populateClassification(final VariantProductModel source, final VariantOptionData target)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature collectionFeatureLabel = features.getFeatureByName(PRODUCT_LABEL);
			if (collectionFeatureLabel != null && collectionFeatureLabel.getValue() != null)
			{
				final String productLabel = collectionFeatureLabel.getValue().getValue().toString();

				target.setLabel(productLabel);

			}
		}
	}
}

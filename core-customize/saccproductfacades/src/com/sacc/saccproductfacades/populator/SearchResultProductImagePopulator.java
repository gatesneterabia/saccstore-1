/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

import java.util.ArrayList;
import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class SearchResultProductImagePopulator extends SearchResultVariantProductPopulator
{
	@Override
	protected List<ImageData> createImageData(final SearchResultValueData source)
	{
		final List<ImageData> result = new ArrayList<>();

		addImageData(source, "thumbnail", result);
		addImageData(source, "product", result);
		addImageData(source, "store", result);
		addImageData(source, "zoom", result);

		return result;
	}
}

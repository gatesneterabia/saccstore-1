/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.converters.populator.StockPopulator;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.stock.strategies.CommerceAvailabilityCalculationStrategy;
import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * @author amjad.shati@erabia.com
 */
public class CustomStockPopulator extends StockPopulator
{
	private CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy;
	private WarehouseSelectionStrategy warehouseSelectionStrategy;
	private StockService stockService;

	@Override
	public void populate(final ProductModel product, final StockData stockData)
	{
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		if (!isStockSystemEnabled(baseStore))
		{
			stockData.setStockLevelStatus(StockLevelStatus.INSTOCK);
			stockData.setStockLevel(Long.valueOf(0));
		}
		else
		{
			stockData.setStockLevel(getCommerceStockService().getStockLevelForProductAndBaseStore(product, baseStore));
			stockData.setStockLevelStatus(getCommerceStockService().getStockLevelStatusForProductAndBaseStore(product, baseStore));

			final Long calculateAvailability = getCommerceStockLevelCalculationStrategy().calculateAvailability(
					getStockService().getStockLevels(product, getWarehouseSelectionStrategy().getWarehousesForBaseStore(baseStore)));

			if (baseStore.isStockLevelControlEnable())
			{
				if (calculateAvailability < product.getStockDisplayAmount())
				{
					stockData.setStockLevel(calculateAvailability);
					stockData.setStockLevelStatus(
							getCommerceStockService().getStockLevelStatusForProductAndBaseStore(product, baseStore));
				}
				else
				{
					stockData.setStockLevel(Long.valueOf(product.getStockDisplayAmount()));
					stockData.setStockLevelStatus(getStockStatus(product.getStockDisplayAmount()));
				}
			}
		}
	}

	private StockLevelStatus getStockStatus(final int stockLevel)
	{
		if (stockLevel > 5)
		{
			return StockLevelStatus.INSTOCK;
		}
		else if (stockLevel >= 1)
		{
			return StockLevelStatus.LOWSTOCK;
		}
		else
		{
			return StockLevelStatus.OUTOFSTOCK;
		}
	}

	public CommerceAvailabilityCalculationStrategy getCommerceStockLevelCalculationStrategy()
	{
		return commerceStockLevelCalculationStrategy;
	}

	public void setCommerceStockLevelCalculationStrategy(
			final CommerceAvailabilityCalculationStrategy commerceStockLevelCalculationStrategy)
	{
		this.commerceStockLevelCalculationStrategy = commerceStockLevelCalculationStrategy;
	}

	public WarehouseSelectionStrategy getWarehouseSelectionStrategy()
	{
		return warehouseSelectionStrategy;
	}

	public void setWarehouseSelectionStrategy(final WarehouseSelectionStrategy warehouseSelectionStrategy)
	{
		this.warehouseSelectionStrategy = warehouseSelectionStrategy;
	}

	public StockService getStockService()
	{
		return stockService;
	}

	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}


}

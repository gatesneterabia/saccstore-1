/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccproductfacades.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CategoryImagesPopulator implements Populator<CategoryModel, CategoryData>
{
	@Resource(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final CategoryModel source, final CategoryData target)
	{
		if (source.getPlpPicture() != null)
		{
			target.setPlpPicture(imageConverter.convert(source.getPlpPicture()));
		}
		if (source.getPlpPictureResponsive() != null)
		{
			target.setPlpPictureResponsive(imageConverter.convert(source.getPlpPictureResponsive()));
		}
	}

}

package com.sacc.saccfulfillment.exception;

/**
 * @author monzer
 *
 */
public class AmbiguousSourcingBanConfigurationException extends RuntimeException
{
	public AmbiguousSourcingBanConfigurationException(final String message)
	{
		super(message);
	}
}
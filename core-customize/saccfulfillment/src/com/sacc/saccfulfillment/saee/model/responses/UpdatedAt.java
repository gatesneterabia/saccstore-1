package com.sacc.saccfulfillment.saee.model.responses;

/**
 * @author Husam Dababneh
 */
public class UpdatedAt
{
	private String date;
	private int timezone_type;
	private String timezone;

	public UpdatedAt()
	{
		super();
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(final String date)
	{
		this.date = date;
	}

	public int getTimezone_type()
	{
		return timezone_type;
	}

	public void setTimezone_type(final int timezone_type)
	{
		this.timezone_type = timezone_type;
	}

	public String getTimezone()
	{
		return timezone;
	}

	public void setTimezone(final String timezone)
	{
		this.timezone = timezone;
	}

}

package com.sacc.saccfulfillment.saee.model.responses;

import java.util.List;


/**
 * @author Husam Dababneh
 */
public class ShipmentTrakingResponse extends Response
{

	private List<Details> details;

	public ShipmentTrakingResponse()
	{
		super();
	}

	public ShipmentTrakingResponse(final List<Details> details)
	{
		super();
		this.details = details;
	}

	public List<Details> getDetails()
	{
		return details;
	}

	public void setDetails(final List<Details> details)
	{
		this.details = details;
	}

}

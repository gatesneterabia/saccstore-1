/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.saee.model;

/**
 * @author Husam Dababneh
 */
public class SaeeOrderData
{
	private String secret;
	private String ordernumber;
	private float cashondelivery;
	private String name;
	private String mobile;
	private String mobile2;
	private String streetaddress;
	private String streetaddress2;
	private String district;
	private String city;
	private String state;
	private String zipcode;
	private String custom_value;
	private String hs_code;
	private String category_id;
	private float latitude;
	private float longitude;
	private float weight;
	private int quantity;
	private String description;
	private String email;
	private String pickup_address_code;
	private String pickup_address_id;
	private String sendername;
	private String sendermail;
	private String senderphone;
	private String senderaddress;
	private String sendercity;
	private String sendercountry;
	private String sender_hub;

	/**
	 * @return the secret
	 */
	public String getSecret()
	{
		return secret;
	}

	/**
	 * @param secret
	 *           the secret to set
	 */
	public void setSecret(final String secret)
	{
		this.secret = secret;
	}

	/**
	 * @return the ordernumber
	 */
	public String getOrdernumber()
	{
		return ordernumber;
	}

	/**
	 * @param ordernumber
	 *           the ordernumber to set
	 */
	public void setOrdernumber(final String ordernumber)
	{
		this.ordernumber = ordernumber;
	}

	/**
	 * @return the cashondelivery
	 */
	public float getCashondelivery()
	{
		return cashondelivery;
	}

	/**
	 * @param cashondelivery
	 *           the cashondelivery to set
	 */
	public void setCashondelivery(final float cashondelivery)
	{
		this.cashondelivery = cashondelivery;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile()
	{
		return mobile;
	}

	/**
	 * @param mobile
	 *           the mobile to set
	 */
	public void setMobile(final String mobile)
	{
		this.mobile = mobile;
	}

	/**
	 * @return the mobile2
	 */
	public String getMobile2()
	{
		return mobile2;
	}

	/**
	 * @param mobile2
	 *           the mobile2 to set
	 */
	public void setMobile2(final String mobile2)
	{
		this.mobile2 = mobile2;
	}

	/**
	 * @return the streetaddress
	 */
	public String getStreetaddress()
	{
		return streetaddress;
	}

	/**
	 * @param streetaddress
	 *           the streetaddress to set
	 */
	public void setStreetaddress(final String streetaddress)
	{
		this.streetaddress = streetaddress;
	}

	/**
	 * @return the streetaddress2
	 */
	public String getStreetaddress2()
	{
		return streetaddress2;
	}

	/**
	 * @param streetaddress2
	 *           the streetaddress2 to set
	 */
	public void setStreetaddress2(final String streetaddress2)
	{
		this.streetaddress2 = streetaddress2;
	}

	/**
	 * @return the district
	 */
	public String getDistrict()
	{
		return district;
	}

	/**
	 * @param district
	 *           the district to set
	 */
	public void setDistrict(final String district)
	{
		this.district = district;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *           the state to set
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * @param zipcode
	 *           the zipcode to set
	 */
	public void setZipcode(final String zipcode)
	{
		this.zipcode = zipcode;
	}

	/**
	 * @return the custom_value
	 */
	public String getCustom_value()
	{
		return custom_value;
	}

	/**
	 * @param custom_value
	 *           the custom_value to set
	 */
	public void setCustom_value(final String custom_value)
	{
		this.custom_value = custom_value;
	}

	/**
	 * @return the hs_code
	 */
	public String getHs_code()
	{
		return hs_code;
	}

	/**
	 * @param hs_code
	 *           the hs_code to set
	 */
	public void setHs_code(final String hs_code)
	{
		this.hs_code = hs_code;
	}

	/**
	 * @return the category_id
	 */
	public String getCategory_id()
	{
		return category_id;
	}

	/**
	 * @param category_id
	 *           the category_id to set
	 */
	public void setCategory_id(final String category_id)
	{
		this.category_id = category_id;
	}

	/**
	 * @return the latitude
	 */
	public float getLatitude()
	{
		return latitude;
	}

	/**
	 * @param latitude
	 *           the latitude to set
	 */
	public void setLatitude(final float latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public float getLongitude()
	{
		return longitude;
	}

	/**
	 * @param longitude
	 *           the longitude to set
	 */
	public void setLongitude(final float longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * @return the weight
	 */
	public float getWeight()
	{
		return weight;
	}

	/**
	 * @param weight
	 *           the weight to set
	 */
	public void setWeight(final float weight)
	{
		this.weight = weight;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(final int quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *           the description to set
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the pickup_address_code
	 */
	public String getPickup_address_code()
	{
		return pickup_address_code;
	}

	/**
	 * @param pickup_address_code
	 *           the pickup_address_code to set
	 */
	public void setPickup_address_code(final String pickup_address_code)
	{
		this.pickup_address_code = pickup_address_code;
	}

	/**
	 * @return the pickup_address_id
	 */
	public String getPickup_address_id()
	{
		return pickup_address_id;
	}

	/**
	 * @param pickup_address_id
	 *           the pickup_address_id to set
	 */
	public void setPickup_address_id(final String pickup_address_id)
	{
		this.pickup_address_id = pickup_address_id;
	}

	/**
	 * @return the sendername
	 */
	public String getSendername()
	{
		return sendername;
	}

	/**
	 * @param sendername
	 *           the sendername to set
	 */
	public void setSendername(final String sendername)
	{
		this.sendername = sendername;
	}

	/**
	 * @return the sendermail
	 */
	public String getSendermail()
	{
		return sendermail;
	}

	/**
	 * @param sendermail
	 *           the sendermail to set
	 */
	public void setSendermail(final String sendermail)
	{
		this.sendermail = sendermail;
	}

	/**
	 * @return the senderphone
	 */
	public String getSenderphone()
	{
		return senderphone;
	}

	/**
	 * @param senderphone
	 *           the senderphone to set
	 */
	public void setSenderphone(final String senderphone)
	{
		this.senderphone = senderphone;
	}

	/**
	 * @return the senderaddress
	 */
	public String getSenderaddress()
	{
		return senderaddress;
	}

	/**
	 * @param senderaddress
	 *           the senderaddress to set
	 */
	public void setSenderaddress(final String senderaddress)
	{
		this.senderaddress = senderaddress;
	}

	/**
	 * @return the sendercity
	 */
	public String getSendercity()
	{
		return sendercity;
	}

	/**
	 * @param sendercity
	 *           the sendercity to set
	 */
	public void setSendercity(final String sendercity)
	{
		this.sendercity = sendercity;
	}

	/**
	 * @return the sendercountry
	 */
	public String getSendercountry()
	{
		return sendercountry;
	}

	/**
	 * @param sendercountry
	 *           the sendercountry to set
	 */
	public void setSendercountry(final String sendercountry)
	{
		this.sendercountry = sendercountry;
	}

	/**
	 * @return the sender_hub
	 */
	public String getSender_hub()
	{
		return sender_hub;
	}

	/**
	 * @param sender_hub
	 *           the sender_hub to set
	 */
	public void setSender_hub(final String sender_hub)
	{
		this.sender_hub = sender_hub;
	}

	@Override
	public String toString()
	{
		return "SaeeOrderData [secret=" + secret + ", ordernumber=" + ordernumber + ", cashondelivery=" + cashondelivery + ", name="
				+ name + ", mobile=" + mobile + ", mobile2=" + mobile2 + ", streetaddress=" + streetaddress + ", streetaddress2="
				+ streetaddress2 + ", district=" + district + ", city=" + city + ", state=" + state + ", zipcode=" + zipcode
				+ ", custom_value=" + custom_value + ", hs_code=" + hs_code + ", category_id=" + category_id + ", latitude="
				+ latitude + ", longitude=" + longitude + ", weight=" + weight + ", quantity=" + quantity + ", description="
				+ description + ", email=" + email + ", pickup_address_code=" + pickup_address_code + ", pickup_address_id="
				+ pickup_address_id + ", sendername=" + sendername + ", sendermail=" + sendermail + ", senderphone=" + senderphone
				+ ", senderaddress=" + senderaddress + ", sendercity=" + sendercity + ", sendercountry=" + sendercountry
				+ ", sender_hub=" + sender_hub + "]";
	}


}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.dao.impl;

import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;


/**
 * @author Husam Dababneh
 */
public class DefaultSmsaFulfillmentProviderDao extends DefaultFulfillmentProviderDao
{

	public DefaultSmsaFulfillmentProviderDao()
	{
		super(SmsaFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return SmsaFulfillmentProviderModel._TYPECODE;
	}

}

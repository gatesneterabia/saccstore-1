/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.strategy.impl;


import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.service.FulfillmentService;
import com.sacc.saccfulfillment.strategy.FulfillmentStrategy;


/**
 * @author Husam Dababneh
 */
public class DefaultSmsaFulfillmentStrategy implements FulfillmentStrategy
{
	@Resource(name = "smsaFulfillmentService")
	private FulfillmentService smsaFulfillmentService;

	/**
	 * @return the smsaFulfillmentService
	 */
	public FulfillmentService getSmsaFulfillmentService()
	{
		return smsaFulfillmentService;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSmsaFulfillmentService().createShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSmsaFulfillmentService().printAWB(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSmsaFulfillmentService().getStatus(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSmsaFulfillmentService().updateStatus(consignmentModel, fulfillmentProviderModel);
	}
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.google.common.base.Preconditions;
import com.sacc.saccfulfillment.enums.FulfillmentActionHistoryType;
import com.sacc.saccfulfillment.enums.FulfillmentProviderType;
import com.sacc.saccfulfillment.exception.FulfillmentException;
import com.sacc.saccfulfillment.exception.enums.FulfillmentExceptionType;
import com.sacc.saccfulfillment.model.FulfillmentProviderModel;
import com.sacc.saccfulfillment.model.FullfillmentHistoryEntryModel;
import com.sacc.saccfulfillment.model.SmsaFulfillmentProviderModel;
import com.sacc.saccfulfillment.service.CarrierService;
import com.sacc.saccfulfillment.service.FulfillmentService;
import com.sacc.saccfulfillment.smsa.exception.SMSAShippingException;
import com.sacc.saccfulfillment.smsa.model.SMSAShipmentData;
import com.sacc.saccfulfillment.smsa.service.SmsaService;
import com.sacc.saccpayment.enums.PaymentModeType;


/**
 * @author Husam Dababneh
 */
public class DefaultSmsaFulfillmentService implements FulfillmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultSmsaFulfillmentService.class);
	private static final String ORDER_MUST_NOT_BE_NULL = "Order Must not be null";
	private static final String DELIVERY_ADDRESS_MUST_NOT_BE_NULL = "Delivery Address must not be null";
	private static final String CITY_MUST_NOT_BE_NULL = "City must not be null";
	@Resource(name = "smsaService")
	private SmsaService smsaService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "smsaFulfillmentStatusMap")
	private Map<String, ConsignmentStatus> smsaFulfillmentStatusMap;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("creating SmsaShipment");
		final SmsaFulfillmentProviderModel smsafulfillmentProviderModel = (SmsaFulfillmentProviderModel) fulfillmentProviderModel;
		final SMSAShipmentData data = prepareSMSAShipmentData(consignmentModel, smsafulfillmentProviderModel);

		saveShipmentRequest(consignmentModel, data.toString(), Operation.CREATE);

		Optional<String> trackingID = Optional.empty();
		try
		{
			trackingID = smsaService.createShipment(data);
		}
		catch (final SMSAShippingException ex)
		{
			saveActionInHistory(consignmentModel, data.toString(), "Fail", FulfillmentActionHistoryType.CREATESHIPMENT);
			switch (ex.getType())
			{
				case MISSING_ARGUMENT:
					throw new FulfillmentException(FulfillmentExceptionType.MISSING_ARGUMENT);
				case REMOTE_ERROR:
					throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
			}
		}
		if (!trackingID.isPresent())
		{
			throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
		}
		modelService.refresh(consignmentModel);
		saveTrackingIdAndCarrier(smsafulfillmentProviderModel, consignmentModel, trackingID.get());
		saveShipmentResponse(consignmentModel, trackingID.get(), Operation.CREATE);

		saveActionInHistory(consignmentModel, data.toString(), trackingID.get(), FulfillmentActionHistoryType.CREATESHIPMENT);
		return trackingID;
	}

	private SMSAShipmentData prepareSMSAShipmentData(final ConsignmentModel consignmentModel,
			final SmsaFulfillmentProviderModel fulfillmentProviderModel)
	{

		final CustomerModel customerModel = (de.hybris.platform.core.model.user.CustomerModel) consignmentModel.getOrder()
				.getUser();

		final OrderModel orderModel = (OrderModel) consignmentModel.getOrder();

		LOG.info("Preparing SMSAShipmentData");
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

		final SMSAShipmentData data = new SMSAShipmentData();
		data.setPassKey(fulfillmentProviderModel.getPassKey());
		data.setSentDate(LocalDate.now().format(dtf));
		data.setRefNo(orderModel.getCode() + "_" + System.currentTimeMillis());
		data.setShipmentType(fulfillmentProviderModel.getSmsaDeliveryType().toString());

		final Optional<String> descs = getProductsSKUs(consignmentModel);
		data.setItemDescription(descs.isPresent() ? descs.get() : "");
		data.setShipperName(descs.isPresent() ? descs.get() : "");

		//data.setShipperName(fulfillmentProviderModel.getShipperName());
		data.setShipperContactName(fulfillmentProviderModel.getShipperContactName());
		data.setShipperCountry(fulfillmentProviderModel.getShipperCountry());
		data.setShipperCity(fulfillmentProviderModel.getShipperCity());
		data.setShipperPhone(fulfillmentProviderModel.getShipperPhone());
		data.setShipperAddress1(fulfillmentProviderModel.getShipperAddress1());
		data.setShipperAddress2(fulfillmentProviderModel.getShipperAddress1());
		data.setIdNo(consignmentModel.getCode());


		data.setCodAmount(getCodAmount(consignmentModel));
		data.setWeight(getWeightOfItems(consignmentModel));


		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(consignmentModel.getOrder().getDeliveryAddress() != null, DELIVERY_ADDRESS_MUST_NOT_BE_NULL);
		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress.getCity() != null, CITY_MUST_NOT_BE_NULL);

		data.setEmailAddress(customerModel.getContactEmail());
		data.setCustomerName(deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname());
		data.setCustomerCountryIsoCode(deliveryAddress.getCountry().getIsocode());
		data.setCustomerCityName(deliveryAddress.getCity().getName(Locale.ENGLISH));
		data.setCustomerMobileNumber(deliveryAddress.getMobile());
		data.setCustomerAddressLine1(deliveryAddress.getLine1());
		data.setCustomerAddressLine2(
				StringUtils.isEmpty(deliveryAddress.getLine2()) ? deliveryAddress.getLine1() : deliveryAddress.getLine2());
		data.setCustomerTelephoneNumber1(
				StringUtils.isEmpty(deliveryAddress.getPhone1()) ? deliveryAddress.getMobile() : deliveryAddress.getPhone1());
		data.setCustomerTelephoneNumber2(
				StringUtils.isEmpty(deliveryAddress.getPhone2()) ? deliveryAddress.getMobile() : deliveryAddress.getPhone2());
		data.setCustomerPOBox(deliveryAddress.getPobox());

		// @Note(Husam): Requested By SACC.
		data.setNumberOfItems(1);
		LOG.info("Finished preparing SMSAShipmentData");
		return data;
	}

	/**
	 *
	 */
	private String getAddress(final AddressModel deliveryAddress)
	{
		return "Street: " + deliveryAddress.getStreetname() + "\r" + "Address:" + deliveryAddress.getAddressName() + "\r"
				+ "District:" + deliveryAddress.getDistrict() + "\r" + "Mobile: " + deliveryAddress.getDistrict() + "\r";

	}

	/**
	 *
	 */
	private void saveActionInHistory(final ConsignmentModel consignmentModel, final String request, final String response,
			final FulfillmentActionHistoryType actionType)
	{
		final List<FullfillmentHistoryEntryModel> history = new ArrayList<FullfillmentHistoryEntryModel>();
		if (consignmentModel.getFulfillmentActionHistory() != null)
		{
			history.addAll(consignmentModel.getFulfillmentActionHistory());
		}

		final FullfillmentHistoryEntryModel historyEntry = modelService.create(FullfillmentHistoryEntryModel.class);
		historyEntry.setCarrier(consignmentModel.getCarrierDetails().getName());
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		historyEntry.setFulfillmentActionType(actionType);
		history.add(historyEntry);

		consignmentModel.setFulfillmentActionHistory(history);
		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);
	}

	/**
	 *
	 */
	private Optional<String> getProductsSKUs(final ConsignmentModel consignmentModel)
	{
		final StringBuilder builder = new StringBuilder();

		for (final Iterator<ConsignmentEntryModel> it = consignmentModel.getConsignmentEntries().iterator(); it.hasNext();)
		{
			final ConsignmentEntryModel consignmentEntryModel = it.next();
			if (consignmentEntryModel != null)
			{
				builder.append(consignmentEntryModel.getOrderEntry().getProduct().getCode() + (it.hasNext() ? ", " : ""));
			}
		}
		return Optional.ofNullable(builder.toString());
	}

	/**
	 *
	 */
	private String getCodAmount(final ConsignmentModel consignmentModel)
	{
		if (PaymentModeType.CARD.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType())
				|| PaymentModeType.CONTINUE.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType()))
		{
			return "0";
		}
		else if (PaymentModeType.NOCARD.equals(consignmentModel.getOrder().getPaymentMode().getPaymentModeType()))
		{
			return consignmentModel.getOrder().getTotalPrice().toString();
		}
		return "0";
	}

	/**
	 *
	 */
	private String getWeightOfItems(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getPackagingInfo() == null ? "" : consignmentModel.getPackagingInfo().getGrossWeight();
	}

	/**
	 *
	 */
	private int getNumberOfItems(final ConsignmentModel consignmentModel)
	{
		if (consignmentModel == null || consignmentModel.getConsignmentEntries() == null
				|| consignmentModel.getConsignmentEntries().isEmpty())
		{
			return 0;
		}
		int count = 0;
		for (final ConsignmentEntryModel consignmentEntryModel : consignmentModel.getConsignmentEntries())
		{
			count += consignmentEntryModel.getQuantity() == null ? 0 : consignmentEntryModel.getQuantity().intValue();
		}

		return count;
	}

	private enum Operation
	{
		CREATE, UPDATE, STATUS
	}

	private void saveTrackingIdAndCarrier(final SmsaFulfillmentProviderModel smsaFulfillmentProviderModel,
			final ConsignmentModel consignmentModel, final String trackingId)
	{
		modelService.refresh(consignmentModel);
		consignmentModel.setTrackingID(trackingId);
		consignmentModel.setCarrierDetails(carrierService.create(smsaFulfillmentProviderModel.getCode(),
				smsaFulfillmentProviderModel.getName(), FulfillmentProviderType.SMSA));
		saveConsignment(consignmentModel);
	}

	private void saveShipmentRequest(final ConsignmentModel consignment, final String request, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentRequestBody(request);
				break;
			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveShipmentResponse(final ConsignmentModel consignment, final String response, final Operation operation)
	{
		switch (operation)
		{
			case CREATE:
				consignment.setCreateShipmentResponseBody(response);
				break;
			case STATUS:
				consignment.setStatusShipmentResponseBody(response);
				break;

			default:
				return;
		}
		saveConsignment(consignment);
	}

	private void saveConsignment(final ConsignmentModel consignment)
	{
		modelService.save(consignment);
		modelService.refresh(consignment);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting PDF for SmsaShipment order");
		final SmsaFulfillmentProviderModel smsafulfillmentProviderModel = (SmsaFulfillmentProviderModel) fulfillmentProviderModel;
		try
		{
			final Optional<byte[]> pdf = smsaService.getPDF(consignmentModel.getTrackingID(),
					smsafulfillmentProviderModel.getPassKey());
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Success",
					FulfillmentActionHistoryType.PRINTAWB);
			return pdf;
		}
		catch (final Exception e)
		{
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Faild", FulfillmentActionHistoryType.PRINTAWB);
			throw new FulfillmentException(FulfillmentExceptionType.CLIENT_ERROR);
		}
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting status for SmsaShipment order");
		final SmsaFulfillmentProviderModel smsafulfillmentProviderModel = (SmsaFulfillmentProviderModel) fulfillmentProviderModel;
		final Optional<String> status = smsaService.getStatus(consignmentModel.getTrackingID(),
				smsafulfillmentProviderModel.getPassKey());
		if (status.isEmpty())
		{
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail", FulfillmentActionHistoryType.GETSTATUS);
			throw new FulfillmentException(FulfillmentExceptionType.CLIENT_ERROR);
		}
		else
		{
			consignmentModel.setFulfillmentStatus(smsaFulfillmentStatusMap.get(status.get()));
			consignmentModel.setFulfillmentStatusText(status.get());
			modelService.save(consignmentModel);
			modelService.refresh(consignmentModel);

			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), status.get(),
					FulfillmentActionHistoryType.GETSTATUS);
			return status;
		}

	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		final Optional<String> currentStatus = getStatus(consignmentModel, fulfillmentProviderModel);
		if (!currentStatus.isPresent())
		{
			LOG.error("Couldn't fetch consignment status");
			saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail",
					FulfillmentActionHistoryType.UPDATESTATUS);
			throw new FulfillmentException(FulfillmentExceptionType.COULDNT_FETCH_STATUS);
		}
		saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), currentStatus.get(),
				FulfillmentActionHistoryType.UPDATESTATUS);
		final ConsignmentStatus consignmentStatus = smsaFulfillmentStatusMap.get(currentStatus.get().trim().toUpperCase());
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(consignmentStatus))
		{
			updateConsignmentStatus(consignmentModel, consignmentStatus);
		}
		modelService.refresh(consignmentModel);
		return Optional.ofNullable(consignmentModel.getStatus());

	}

	private void updateConsignmentStatus(final ConsignmentModel consignment, final ConsignmentStatus newStatus)
	{
		modelService.refresh(consignment);
		consignment.setStatus(newStatus);
		consignment.setActualDeliveryDate(LocalDate.now().format(DateTimeFormatter.ofPattern("d/M/yyyy")));
		modelService.save(consignment);
		LOG.info("Consignment status updated for consignment: " + consignment.getCode());
	}
}
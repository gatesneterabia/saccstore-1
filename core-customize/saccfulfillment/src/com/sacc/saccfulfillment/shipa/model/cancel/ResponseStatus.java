package com.sacc.saccfulfillment.shipa.model.cancel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties
public class ResponseStatus {
	@JsonProperty("status")
	private String status;
	@JsonProperty("shipa_ref")
	private String shipaReference;
	@JsonProperty("error_message")
	private String errorMessage;

	public ResponseStatus() {

	}

	public ResponseStatus(final String status, final String shipaReference, final String errorMessage) {
		super();
		this.status = status;
		this.shipaReference = shipaReference;
		this.errorMessage = errorMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getShipaReference() {
		return shipaReference;
	}

	public void setShipaReference(final String shipaReference) {
		this.shipaReference = shipaReference;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString()
	{
		return "ResponseStatus [status=" + status + ", shipaReference=" + shipaReference + ", errorMessage=" + errorMessage + "]";
	}

}

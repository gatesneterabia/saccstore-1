/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorder.consignment.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.warehousing.daos.WarehousingConsignmentEntryQuantityDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomWarehousingConsignmentEntryQuantityDao extends AbstractItemDao
		implements WarehousingConsignmentEntryQuantityDao
{
	protected static final String shippedQuery = "SELECT SUM({consignmentEntry:quantity}) FROM {" + ConsignmentEntryModel._TYPECODE
			+ " as consignmentEntry JOIN " + ConsignmentModel._TYPECODE + " as consignment ON {consignmentEntry:" + "consignment"
			+ "}={consignment:" + "pk" + "}} WHERE {consignmentEntry." + "pk" + "}=?consignmentEntry AND ({consignment." + "status"
			+ "}=?shippedStatus OR {consignment." + "status" + "}=?pickedupStatus OR {consignment." + "status"
			+ "}=?deliveredStatus)";
	protected static final String declinedQuery = "SELECT SUM({dcee:quantity}) FROM {DeclineConsignmentEntryEvent as dcee} WHERE {dcee:consignmentEntry}=?consignmentEntry";

	@Override
	public Long getQuantityShipped(final ConsignmentEntryModel consignmentEntry)
	{
		final HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("consignmentEntry", consignmentEntry);
		params.put("shippedStatus", ConsignmentStatus.SHIPPED);
		params.put("pickedupStatus", ConsignmentStatus.PICKUP_COMPLETE);
		params.put("deliveredStatus", ConsignmentStatus.DELIVERY_COMPLETED);
		final Long quantity = processRequestWithParams(shippedQuery, params);
		return quantity;
	}

	@Override
	public Long getQuantityDeclined(final ConsignmentEntryModel consignmentEntry)
	{
		final HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("consignmentEntry", consignmentEntry);
		return processRequestWithParams(declinedQuery, params);
	}

	@Override
	public Long processRequestWithParams(final String queryString, final Map<String, Object> params)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(queryString);
		params.keySet().forEach(key -> fQuery.addQueryParameter(key, params.get(key)));
		final ArrayList<Class<Long>> resultClassList = new ArrayList<Class<Long>>();
		resultClassList.add(Long.class);
		fQuery.setResultClassList(resultClassList);
		final SearchResult<Long> result = getFlexibleSearchService().search(fQuery);
		return result.getResult().stream().filter(res -> res != null).findFirst().orElse(0L);
	}
}

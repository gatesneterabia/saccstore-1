/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorder.returns.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomConsignmentBasedReturnableCheck implements ReturnableCheck
{

	@Resource
	private ModelService modelService;
	private static final Logger LOG = Logger.getLogger(DefaultCustomConsignmentBasedReturnableCheck.class);

	@Override
	public boolean perform(final OrderModel order, final AbstractOrderEntryModel orderentry, final long returnQuantity)
	{
		if (returnQuantity < 1L || orderentry.getQuantity() < returnQuantity)
		{
			return false;
		}
		final Set<ConsignmentModel> consignments = order.getConsignments();
		if (CollectionUtils.isEmpty(consignments))
		{
			return false;
		}
		final Optional<ConsignmentEntryModel> matchingConsignmentEntry = consignments.stream()
				.filter(c -> c.getStatus().equals(ConsignmentStatus.SHIPPED)
						|| c.getStatus().equals(ConsignmentStatus.DELIVERY_COMPLETED))
				.flatMap(c -> c.getConsignmentEntries().stream()).filter(ce -> ce.getOrderEntry().equals(orderentry)).findFirst();
		if (matchingConsignmentEntry.isPresent())
		{
			final long shippedQuantity = matchingConsignmentEntry.get().getShippedQuantity() == null ? 0
					: matchingConsignmentEntry.get().getShippedQuantity().longValue();
			return shippedQuantity >= returnQuantity;
		}
		return false;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
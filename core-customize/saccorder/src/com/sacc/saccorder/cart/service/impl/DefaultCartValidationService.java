/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sacc.saccorder.cart.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sacc.saccorder.cart.exception.CartValidationException;
import com.sacc.saccorder.cart.exception.enums.CartExceptionType;
import com.sacc.saccorder.cart.service.CartValidationService;
import com.sacc.saccorder.model.CartAmountPriceRowModel;
import com.google.common.base.Preconditions;


public class DefaultCartValidationService implements CartValidationService
{
	private static final String CART_MUSTN_T_BE_NULL = "cartModel mustn't be null";
	protected static final Logger LOG = Logger.getLogger(DefaultCartValidationService.class);

	@Resource(name = "cartService")
	private CartService cartService;

	/**
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	@Override
	public boolean validateCartMinAmount(final CartModel cartModel) throws CartValidationException
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> minCartAmountPrices = baseStoreModel.getMinCartAmountPrices();
		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(minCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return true;
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();

		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}
		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}
		LOG.info("min cart Amount[" + (cartAmountPriceRowModel == null ? null : cartAmountPriceRowModel.getAmount())
				+ "] , cart total price [" + totalPrice + "] , cart code [" + cartModel.getCode() + "], includeDeliveryCost["
				+ includeDeliveryCost + "]");

		if (totalPrice < cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final String[] parm = new String[5];
			parm[0] = cartAmountPriceRowModel.getAmount().toString();
			parm[1] = cartModel.getCurrency().getSymbol();
			parm[2] = totalPrice + "";
			parm[3] = cartModel.getCode();
			parm[4] = (cartAmountPriceRowModel.getAmount().doubleValue() - totalPrice) + "";
			throw new CartValidationException("You exceed the cart min amount limit ", CartExceptionType.MIN_AMOUNT, parm);
		}
		return true;
	}

	/**
	 *
	 */
	private double getTotalPrice(final CartModel cartModel)
	{
		final double storeCreditAmount = cartModel.getStoreCreditAmount() == null ? 0
				: cartModel.getStoreCreditAmount().doubleValue();
		final double totalPrice = cartModel.getTotalPrice().doubleValue();
		return totalPrice + storeCreditAmount;
	}

	@Override
	public boolean validateCartMaxAmount(final CartModel cartModel) throws CartValidationException
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> maxCartAmountPrices = baseStoreModel.getMaxCartAmountPrices();

		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(maxCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return true;
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();
		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}

		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}

		LOG.info("max cart Amount[" + (cartAmountPriceRowModel == null ? null : cartAmountPriceRowModel.getAmount())
				+ "] , cart total price [" + totalPrice + "] , cart code [" + cartModel.getCode() + "] , includeDeliveryCost["
				+ includeDeliveryCost + "] , includePaymentCost[" + includePaymentCost + "]");

		if (totalPrice > cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final String[] parm = new String[4];
			parm[0] = cartAmountPriceRowModel.getAmount().toString();
			parm[1] = cartModel.getCurrency().getSymbol();
			parm[2] = cartModel.getTotalPrice().toString();
			parm[3] = cartModel.getCode();

			throw new CartValidationException("You exceed the cart max amount limit ", CartExceptionType.MAX_AMOUNT, parm);
		}

		return true;
	}

	protected CartAmountPriceRowModel getCartAmountPriceRowForCartCurrency(
			final List<CartAmountPriceRowModel> cartAmountPriceRowList, final CurrencyModel currency)
	{
		if (cartAmountPriceRowList == null || cartAmountPriceRowList.isEmpty() || currency == null)
		{
			return null;
		}

		for (final CartAmountPriceRowModel cartAmountPriceRowModel : cartAmountPriceRowList)
		{
			if (currency.getIsocode().equals(cartAmountPriceRowModel.getCurrency().getIsocode()))
			{
				return cartAmountPriceRowModel;
			}
		}

		return null;
	}

	@Override
	public boolean validateCartMinAmountByCurrentCart() throws CartValidationException
	{
		return getCartService().hasSessionCart() && validateCartMinAmount(getCartService().getSessionCart());
	}

	@Override
	public boolean validateCartMaxAmountByCurrentCart() throws CartValidationException
	{
		return getCartService().hasSessionCart() && validateCartMaxAmount(getCartService().getSessionCart());
	}

}

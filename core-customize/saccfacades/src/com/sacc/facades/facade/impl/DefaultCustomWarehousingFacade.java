/**
 *
 */
package com.sacc.facades.facade.impl;

import static org.springframework.util.Assert.isTrue;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.warehousing.process.impl.DefaultConsignmentProcessService;
import de.hybris.platform.warehousingfacades.order.impl.DefaultWarehousingConsignmentFacade;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;


/**
 * @author mohammad
 *
 */
public class DefaultCustomWarehousingFacade extends DefaultWarehousingConsignmentFacade
{
	private static final String PICK_CONSIGNMENT_CHOICE = "pickConsignment";
	@Override
	protected void performPickWorkflowAction(final ConsignmentModel consignmentModel, final boolean printSlip)
	{
		final WorkflowActionModel pickWorkflowAction = getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(PICKING_TEMPLATE_CODE, consignmentModel);

		// check if the consignment has already been picked. If so do not invoke the decideWorkflowAction method
		if (pickWorkflowAction != null)
		{
			if (!WorkflowActionStatus.COMPLETED.equals(pickWorkflowAction.getStatus()))
			{
				getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignmentModel, PICKING_TEMPLATE_CODE,
						PICK_CONSIGNMENT_CHOICE);
			}
			// throw an exception if the consignment has already been picked and the slip is not to be generated
			else if (!printSlip)
			{
				throw new IllegalStateException(
						String.format(Localization.getLocalizedString("warehousingfacade.consignment.pick.error.wrongstatus"),
								consignmentModel.getCode(), pickWorkflowAction.getStatus()));
			}
		}
		else if (!printSlip)
		{
			// throw an exception if the WorkflowActionModel is null and the slip is not to be generated
			throw new IllegalStateException(
					String.format(Localization.getLocalizedString("warehousingfacade.consignment.pick.error.null.workflowactionmodel"),
							consignmentModel.getCode()));
		}
	}


	@Override
	protected String packConsignment(final ConsignmentModel consignmentModel, final boolean printSlip)
	{
		isTrue(consignmentModel.getFulfillmentSystemConfig() == null,
				String.format(Localization.getLocalizedString("warehousingfacade.consignments.error.fulfillmentConfig"),
						consignmentModel.getCode()));

		// check if the consignment has already been packed. If so do not invoke the decideWorkflowAction method
		if (ConsignmentStatus.READY.equals(consignmentModel.getStatus())
				|| ConsignmentStatus.PICKPACK.equals(consignmentModel.getStatus()))
		{
			getWarehousingConsignmentWorkflowService().decideWorkflowAction(consignmentModel, PACKING_TEMPLATE_CODE,
					PACK_CONSIGNMENT_CHOICE);
		}
		// throw an exception if the consignment has already been packed and if the slip should not be generated
		else if (!printSlip)
		{
			throw new IllegalStateException(
					String.format(Localization.getLocalizedString("warehousingfacade.consignment.pack.error.wrongstatus"),
							consignmentModel.getCode(), consignmentModel.getStatus()));
		}
		String template = null;

		if (printSlip)
		{
			// generate the html template even if the consignment is already picked
			final MediaModel packListMedia = getPrintMediaService().getMediaForTemplate(PACK_SLIP_DOCUMENT_TEMPLATE,
					((DefaultConsignmentProcessService) getConsignmentBusinessProcessService())
							.getConsignmentProcess(consignmentModel));

			template = getPrintMediaService().generateHtmlMediaTemplate(packListMedia);
		}
		return template;
	}
}

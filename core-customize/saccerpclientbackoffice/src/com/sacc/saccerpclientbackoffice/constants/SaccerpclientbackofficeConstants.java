/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.sacc.saccerpclientbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SaccerpclientbackofficeConstants extends GeneratedSaccerpclientbackofficeConstants
{
	public static final String EXTENSIONNAME = "saccerpclientbackoffice";

	private SaccerpclientbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
